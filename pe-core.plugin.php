<?php 
/*
Plugin Name: PE Core
Plugin URI: https://gitlab.com/wp37/pe-core
Description: Base ProtopiaEcosystem plugin for Wordpress CMS
Version: 0.0.1
Author: Genagl@list.ru
Author URI: https://protopiahome-public.gitlab.io/protopia-ecosystem/cra-template-pe/
License: GPL2
Text Domain:   pe-core
Domain Path:   /lang/
*/
/*  Copyright 2022  Genagl  (email: genag1@list.ru)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/  
error_reporting(E_ERROR);
ini_set('session.use_cookies', '1');

//библиотека переводов
function init_textdomain_pe_core() 
{ 
	if (function_exists('load_plugin_textdomain')) 
	{
		load_plugin_textdomain("pe-core", false , dirname( plugin_basename( __FILE__ ) ) .'/lang/');     
	} 
}
add_action('plugins_loaded', 'init_textdomain_pe_core');
//Paths
define('PE_CORE_URLPATH', plugins_url() . '/pe-core/');
define('PE_CORE_REAL_PATH', WP_PLUGIN_DIR.'/'.plugin_basename(dirname(__FILE__)).'/');
define('_REAL_PATH', PE_CORE_REAL_PATH);
define('PE_CORE', 'pe-core');
define('PE_MAIL_TYPE', 'pe-mail');
define('PE_COMMENT_TYPE', 'pe-comment');
define('PE_MESSAGE', 'pe-message');
define('PE_LAND_GROUP', 'pe-land-group');

define('PE_POST_TITLE', 'title');
define('PE_POST_CONTENT', 'post_content');
define('PE_POST_DATE', 'post_date');
define('PE_POST_STATUS', 'post_status');
define('PE_POST_NAME', 'post_name');
define('PE_POST_AUTHOR', 'post_author');
define('PE_POST_TYPE', 'post_type');

define('PE_CORE_MEDIA_TAXONOMY_TYPE', 'media-category');
define('PE_CORE_EMPTY_IMG', PE_CORE_URLPATH . "assets/img/empty.png");

define("PE_CORE_DEFAULT_ORDER", 0);
define("PE_CORE_FREE_TEST_ORDER", 1);
define("PE_CORE_INTRAMURAL_TEST_ORDER", 2);
define("PE_CORE_ALPHABETICAL_ORDER", 3);

require_once(PE_CORE_REAL_PATH."vendor/autoload.php");
require_once(PE_CORE_REAL_PATH.'class/PE_Assistants.php');
require_once(PE_CORE_REAL_PATH.'class/graphql/pe-graphql.php');

require_once(PE_CORE_REAL_PATH.'class/PECore.class.php');
require_once(PE_CORE_REAL_PATH.'class/PE_Post.php');
require_once(PE_CORE_REAL_PATH.'class/PE_Taxonomy.class.php');
require_once(PE_CORE_REAL_PATH.'class/PEPost_Tag.class.php');
require_once(PE_CORE_REAL_PATH.'class/PE_Object_Type.php');
require_once(PE_CORE_REAL_PATH.'class/PEConglomerath.class.php');
require_once(PE_CORE_REAL_PATH.'class/PEAjax.class.php');
require_once(PE_CORE_REAL_PATH.'class/PEUser.class.php');
require_once(PE_CORE_REAL_PATH.'class/PEMail.class.php');
require_once(PE_CORE_REAL_PATH.'class/PEComment.class.php');
require_once(PE_CORE_REAL_PATH.'class/PEMessage.class.php');
require_once(PE_CORE_REAL_PATH.'class/PEJWT.class.php');
require_once(PE_CORE_REAL_PATH.'class/PECoreGraphQL.class.php'); 
require_once(PE_CORE_REAL_PATH.'class/PELandGroup.class.php');
require_once(PE_CORE_REAL_PATH.'class/graphql/JsonParser.php');
require_once(PE_CORE_REAL_PATH.'tpl/ObsceneCensorRus.php');

if (function_exists('register_activation_hook'))
{
	require_once(PE_CORE_REAL_PATH.'class/PEActivation.class.php');
	register_activation_hook( __FILE__, [ "PECore", 'activate' ] ); 
	register_activation_hook( __FILE__, [ "PEActivation", 'get_instance' ] ); 
}
if (function_exists('register_deactivation_hook'))
{
	 register_deactivation_hook(__FILE__, array("PECore", 'deactivate'));
}
function insertLog($args)
{
	
} 
		
function get_full_roles()
{
	$arr = apply_filters("pe_core_rull_roles", []); 
	array_unshift($arr, [
		"administrator", 	
		__("Administrator", BIO),
		
	]);
	array_push($arr, 	[
		"contributor", 	
		__("Contributor", BIO)
	]);
	return $arr;
}
	
add_action("init", function()
{
	PE_Object_Type::get_instance();
	PECore::get_instance();
	PEAjax::get_instance();
	PE_Assistants::init();  
	PEUser::init();
	PEMail::init();
	PEMessage::init();
	PEComment::init(); 	
	PECoreGraphQL::init();
	PELandGroup::init();
}, 2);

add_action("init", function()
{ 
	
}, 2); 

 