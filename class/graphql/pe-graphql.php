<?php
  
 
use GraphQL\GraphQL;
use GraphQL\Type\Schema;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\EnumType;
use GraphQL\Type\Definition\CustomScalarType;
use GraphQL\Type\Definition\UnionType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\SchemaConfig;
use GraphQL\Utils\Utils;
use GraphQL\Error\Debug;
use GraphQL\Utils\SchemaPrinter; 



add_action('parse_request', ["PEGraphql", 'graphql_url_handler']);

add_action("init", function()
{
	
}, 1);


class PEGraphql 
{
	
    static $types = [
        "Query" 		=> [],
        "Mutation" 		=> [],
        "object_types"	=> [],
        "union_types" 	=> [],
        "json_types" 	=> [],
        "input_types" 	=> [],
		"enum_types" 	=> [],
		"scalar_types" 	=> []
    ];

    static function object_type($name) {
        if (!isset(self::$types["object_types"][$name]))
        {
            self::$types["object_types"][$name] = new ObjectType([
                "name" => "pegraphql_temp_{$name}",
                "fields" => ['id' => Type::string()],
            ]);
        }
        return self::$types["object_types"][$name];
    }

    static function input_type( $name) {
        if (!isset(self::$types["input_types"][$name]))
        {
            self::$types["input_types"][$name] = new InputObjectType([
                "name" 	=> "pegraphql_temp_{$name}",
                "fields" => ['id' => Type::string()],
            ]);
        }
        return self::$types["input_types"][$name];
    }
	
    static function enum_type($name) {
        if (!isset(self::$types["enum_types"][$name]))
        {
            self::$types["enum_types"][$name] = new EnumType([
                "name" => "pegraphql_temp_{$name}",
            ]);
        }
        return self::$types["enum_types"][$name];
    }
	
    static function scalar_type($name) {
        if (!isset(self::$types["scalar_types"][$name]))
        {
            self::$types["scalar_types"][$name] = new CustomScalarType([
                "name" => "pegraphql_temp_{$name}", 
            ]);
        }
        return self::$types["scalar_types"][$name];
    }
	
	
    static function json_type($name) {
        if (!isset(self::$types["json_types"][$name]))
        {
            self::$types["json_types"][$name] = new JsonParser([
                "name" => "pegraphql_temp_{$name}", 
            ]);
        }
        return self::$types["json_types"][$name];
    }	
	/**/
	
    static function union_type($name ) {
        if (!isset(self::$types["union_types"][$name]))
        {
            self::$types["union_types"][$name] = new UnionType([
                "name" => "pegraphql_temp_{$name}",
                "types" => [
					Type::string(), 
					Type::listOf( Type::string() ) 
				],
            ]);
        }
        return self::$types["union_types"][$name];
    }
	

    static function add_query($name, $query) {
        self::$types["Query"][$name] = $query;
    }

    static function add_mutation($name, $mutation) {
        self::$types["Mutation"][$name] = $mutation;
    }
    
    static function add_object_type($data) {
        if (is_array( $data["fields"]) &&  !count($data["fields"]))
        {
            wp_die($data["name"]." hasn't fields");
        }
        if (isset(self::$types["object_types"][$data["name"]]))
        {
            self::$types["object_types"][$data["name"]]->__construct($data);
            return self::$types["object_types"][$data["name"]];
        }
		$data = apply_filters("pe_graphql_object_type_" . $data["name"], $data);
        self::$types["object_types"][$data["name"]] = new ObjectType($data);
    }
	

    static function add_input_type($data) {
        if ( is_array( $data["fields"]) && count( $data["fields"] ) == 0)
        {
            wp_die($data["name"]." hasn't fields");
        }
        foreach ($data["fields"] as &$field)
        {
            if (is_array($field))
            {
                unset($field["resolve"]);
            }
        }
        if (isset(self::$types["input_types"][$data["name"]]))
        {
            self::$types["input_types"][$data["name"]]->__construct($data);
            return self::$types["input_types"][$data["name"]];
        }
		$data = apply_filters("pe_graphql_input_type_" . $data["name"], $data);
        self::$types["input_types"][$data["name"]] = new InputObjectType($data);
    }
	static function add_enum_type($data) {
		if (!count($data["values"]))
        {
            wp_die($data["name"]." hasn't values");
        } 
		if (isset(self::$types["enum_types"][$data["name"]]))
        {
            self::$types["enum_types"][$data["name"]]->__construct($data);
            return self::$types["enum_types"][$data["name"]];
        }
		$data = apply_filters("pe_graphql_enum_type_" . $data["name"], $data);
        self::$types["enum_types"][$data["name"]] = new EnumType($data);
	}
	static function add_scalar_type($data) { 
		if (isset(self::$types["scalar_types"][$data["name"]]))
        {
            self::$types["scalar_types"][$data["name"]]->__construct($data);
            return self::$types["scalar_types"][$data["name"]];
        }
		$data = apply_filters("pe_graphql_enum_type_" . $data["name"], $data);
        self::$types["scalar_types"][$data["name"]] = new CustomScalarType($data);
	}
	static function add_union_type($data) 
	{ 
		if (isset(self::$types["union_types"][$data["name"]]))
        {
            self::$types["union_types"][$data["name"]]->__construct($data);
            return self::$types["union_types"][$data["name"]];
        }
		$data = apply_filters("pe_graphql_union_type_" . $data["name"], $data);
        self::$types["union_types"][$data["name"]] = new UnionType($data);
	}
	
	static function add_json_type($data) 
	{ 
		/**/
		if (isset(self::$types["json_types"][$data["name"]]))
        {
            self::$types["json_types"][$data["name"]]->__construct($data);
            return self::$types["json_types"][$data["name"]];
        }
		$data = apply_filters("pe_graphql_json_type_" . $data["name"], $data);
        self::$types["json_types"][$data["name"]] = new JsonParser($data);
		
	}
	

    static function graphql_url_handler() 
	{
        if($_SERVER["REQUEST_URI"] == '/graphql') 
		{
			global $jwt_token;
			// wp_die( PECore::$options );
			// http_response_code(200);
			$code = "";
            try 
			{
				$headers = getallheaders();
				// wp_die( $headers["Authorization"] );
				if (isset($headers["Authorization"]) || isset($headers["authorization"]))
				{
					$Authorization = preg_replace("/^Bearer /", "", $headers["Authorization"]);
					$Authorization = $Authorization 
						? 
						$Authorization 
						: 
						preg_replace("/^Bearer /", "", $headers["authorization"]);
					$jwt_token = PEJWT::verifyJWS( $Authorization, "acces token" ); 
					if ($jwt_token) 
					{
						if( $jwt_token['headers']['exp'] < time() )
						{ 
							// http_response_code(401);
							header("Access-Control-Allow-Origin: *");
							header("Access-Control-Allow-Headers: *");		
							$code = "UNAUTHENTICATED";
							throw new \PEGraphQLUnloggedException(__("Your token has expired", PE_CORE) ); 
						} 
						wp_set_current_user( $jwt_token["payload"]["sub"] );
					}
				}

				do_action("pe_graphql_make_schema");
				
				//
				header("Access-Control-Allow-Origin: *");
				header("Access-Control-Allow-Headers: *");
				
				$schema_array = [];
				$config = SchemaConfig::create();
				if (self::$types["Query"]) {
					$queryType = new ObjectType([
						'name' => 'Query',
						'fields' => self::$types["Query"]
					]);
					$config->setQuery($queryType);
				}
				if (self::$types["Mutation"]) {
					$mutationType = new ObjectType([
						'name' => 'Mutation',
						'fields' => self::$types["Mutation"]
					]);
					$config->setMutation($mutationType);
				}

				$config->setTypes(
					array_merge(
						self::$types["object_types"], 
						self::$types["input_types"], 
						self::$types["enum_types"],
						// self::$types["json_types"],
						self::$types["union_types"],
						self::$types["scalar_types"],
					)
				); 

                $schema = new Schema($config);
				// echo SchemaPrinter::doPrint($schema);
                
                $rawInput = file_get_contents('php://input');
                if (isset($_POST["operations"])) {
                    $rawInput = stripslashes_deep($_POST["operations"]);
                }
                $input = json_decode($rawInput, true);
                $query = $input['query'];
                $variableValues = isset($input['variables']) ? $input['variables'] : null; 
                $rootValue = ['prefix' => 'You said: '];
                $result = GraphQL::executeQuery($schema, $query, $rootValue, null, $variableValues);				
				
				do_action("pe_graphql_cookies"); 
                $debug = \GraphQL\Error\Debug::INCLUDE_DEBUG_MESSAGE || \GraphQL\Error\Debug::RETHROW_UNSAFE_EXCEPTIONS;
                $schema->assertValid();
                $output = $result->toArray($debug);
				  
				//
            } 
			catch (\Exception $e) 
			{
                $output = [
                    'errors' => [
                        [
                            'message' 		=> $e->getMessage(),
							"extensions" 	=> [
								"code"	=> $code,
							],
							"code"			=> $e->getCode(),
							"data"			=> ""
                        ]
                    ]
                ];
            }
            header('Content-Type: application/json');
            echo json_encode($output);  
			
			exit();
			//wp_die(  );
        }
    }
}
require_once(__DIR__ . "/auth.php");
require_once(__DIR__ . "/settings.php");
 