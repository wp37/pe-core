<?php

declare(strict_types=1);

// namespace GraphQL\Type\Definition;

use GraphQL\Language\AST\BooleanValueNode;
use GraphQL\Language\AST\FloatValueNode;
use GraphQL\Language\AST\IntValueNode;
use GraphQL\Language\AST\ListValueNode;
use GraphQL\Language\AST\Node;
use GraphQL\Language\AST\ObjectValueNode;
use GraphQL\Language\AST\StringValueNode;

use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ScalarType;
use GraphQL\Language\AST\ScalarTypeDefinitionNode;
use GraphQL\Language\AST\ScalarTypeExtensionNode;
use GraphQL\Utils\Utils;
use function is_string;

class JsonParser extends Type
{
    /** @var ScalarTypeDefinitionNode|null */
    public $astNode;
	
	public $config;
    /** @var ScalarTypeExtensionNode[] */
    public $extensionASTNodes;
    public $name = 'JsonParser';
    public $description =
        'The `JSON` scalar type represents JSON values as specified by
        [ECMA-404](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf).';

    public function __construct( $config = [] )
    { 
        $this->name              = $config['name'];
		$this->description       = $config['description'];
        $this->astNode           = $config['astNode'];
        $this->extensionASTNodes = $config['extensionASTNodes'];
        $this->config            = $config;

        // Utils::invariant(is_string($this->name), 'Must provide name.');
    }

		/*
    public function parseValue($value)
    {
        return $this->identity($value);
    }

    public function serialize($value)
    {
        return $this->identity($value);
    }

    public function parseLiteral($valueNode)
    {
        switch ($valueNode) {
            case ($valueNode instanceof StringValueNode):
            case ($valueNode instanceof BooleanValueNode):
                return $valueNode->value;
            case ($valueNode instanceof IntValueNode):
            case ($valueNode instanceof FloatValueNode):
                return floatval($valueNode->value);
            case ($valueNode instanceof ObjectValueNode): {
                $value = [];
                foreach ($valueNode->fields as $field) {
                    $value[$field->name->value] = $this->parseLiteral($field->value);
                }
                return $value;
            }
            case ($valueNode instanceof ListValueNode):
                return array_map([$this, 'parseLiteral'], $valueNode->values);
            default:
                return null;
        }

    }

    private function identity($value)
    {
        return $value;
    }

		*/
}