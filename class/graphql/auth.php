<?php

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Error\ClientAware;

class PE_GraphQL_Exception extends \Exception implements ClientAware
{
	public function isClientSafe()
    {
        return true;
    }
    public function getCategory()
    {
        return 'No access';
    }
}
class PE_GraphQL_Attention_Exception extends \Exception implements ClientAware
{
	public function isClientSafe()
    {
        return true;
    }
    public function getCategory()
    {
        return 'ATTENTION';
    }
}
class PEGraphQLUnloggedException extends \Exception implements ClientAware
{ 
	/**/
	//$graphQLCode;
	function __construct($message = "UNAUTHENTICATED")
	{
		$this->graphQLCode = 'UNAUTHENTICATED';
		parent::__construct($message);
	}
	//function getGQCode()
	//{
	//	return 'UNAUTHENTICATED';
	//}
	public function isClientSafe()
    {
        return true;
    }
    public function getCategory()
    {
        return 'UNAUTHENTICATED';
    }
}
class PEGraphQLNotAccess extends \Exception implements ClientAware
{
	public function isClientSafe()
    {
        return true;
    }
    public function getCategory()
    {
        return 'not access caps';
    }
}
class PEGraphQLExeption extends \Exception implements ClientAware
{
	//$graphQLCode;
	function __construct($message = "not access")
	{
		$this->graphQLCode = 'not access';
		parent::__construct($message);
	}
	//function getGQCode()
	//{
	//	return 'not access';
	//}
	public function isClientSafe()
    {
        return true;
    }
    public function getCategory()
    {
        return 'not access';
    }
}
class GQLUser {
	
	static function insertUser( $args) {			
		//wp_die(wp_roles());
		$user_id =  wp_insert_user([ 
			"user_login"	=> $args[ 'user_login' ], 
			"first_name"	=> $args[ 'first_name' ], 
			"display_name"	=> $args[ 'display_name' ], 
			"last_name"		=> $args[ 'last_name' ], 
			"user_description"	=> $args[ 'user_descr' ], 
			"description"	=> $args[ 'description' ], 
			"post_content"	=> $args[ 'description' ], 
			"user_pass"		=> $args[ 'password' ], 
			"user_email"	=> $args[ 'user_email' ],
			"phone"			=> $args[ 'phone' ] 
		]);					
		if ( is_wp_error( $user_id ) ) 
		{
			PECore::addLog( $args, $user_id, "error_register_user");
			throw new PE_GraphQL_Exception( $user_id->get_error_message() );
			//wp_die( $user_id->get_error_message() );
		}
		else
		{
			PECore::addLog( $args, $user_id, "success_register_user");
			$user_id = wp_update_user(apply_filters(
				"pe_graphql_change_user",
				[
					"ID"			=> $user_id, 
					"description"	=> $args[ 'description' ]
				], 
				$user_id
			)); 
			$user_id = apply_filters("pe_graphql_change_meta_user", $user_id, $args);
			update_user_meta($user->ID, "post_content", $args[ 'user_descr' ]);
			if( $args[ 'avatar' ] ) {
				PE_Assistants::change_user_avatar( $args['avatar'], $user_id );	
			}
			
			
			if( isset( $args['roles'] ) )
			{ 
				$user = get_user_by( "id", $user_id ); 
				foreach( $user->roles as $role )
				{
					$user->remove_role( $role );
				}
				foreach( $args['roles'] as $role )
				{
					$user->add_role( $role ); 
				}
			}
			else
			{
				$user = get_user_by( "id", $user_id );		
				$user->add_role("contributor");	
			}					
			//wp_die( isset( $args['roles'] ) ); 
		}
		$user = get_user_by( "id", $user_id ); 
		$userData =  apply_filters("pe_graphql_get_user", $user, true);
	}
	static function changeUser($id, $args) { 
		if( !current_user_can( 'edit_users' ) && get_current_user_id() !== $id )
		{
			throw new PE_GraphQL_Exception ("you not rights");
		}
		$args['ID']	= $id;
		if($args["password"]) {
			if(is_super_admin() || get_current_user_id() === $id) {
				$args["user_pass"] = $args["password"];
			}
			else {
				PECore::addLog( $args, $user_id, "illegal_change_user_password");
				throw new PE_GraphQL_Exception ("you not rights");
			}
		}
		$user_id = wp_update_user(apply_filters(
			"pe_graphql_change_user",
			$args, 
			$id
		));
		if( is_wp_error( $user_id ) )
		{
			PECore::addLog( $args, $user_id, "error_insert_user");
			throw new PE_GraphQL_Exception ($user_id->get_error_message());
		}
			 			
		$user_id = PEUser::update($args, $user_id ); 
		if( is_wp_error( $user_id ) )
		{
			PECore::addLog( $args, $user_id, "error_edit_user");
			throw new PE_GraphQL_Exception ("unknown error in edit proccess...");
		}
		
		PECore::addLog( $args, $user_id, "success_edit_user");
		update_user_meta($user->ID, "post_content", $args[ 'user_descr' ]);
		update_user_meta($user->ID, "phone", $args[ 'phone' ]);
		
		if( $args[ 'avatar' ] ) {
			PE_Assistants::change_user_avatar( $args['avatar'], $user_id );	
		}
		if( isset( $args['roles'] ) )
		{
			$user = get_user_by( "id", $id );
			foreach( $user->roles as $role )
			{
				$user->remove_role( $role );
			}
			foreach( $args['roles'] as $role )
			{
				$user->add_role( $role );
			}
		}
		$user = get_user_by( "id", $user_id ); 
		$userData =  apply_filters("pe_graphql_get_user", $user, true);
		return $userData;
	}
	
}
add_filter("pegq_get_tocken", function($user_id, $jwt_token)
{
	$is_blocked = get_user_meta($user_id, "is_blocked", true);
	if($is_blocked)
	{
		//throw new PE_GraphQL_Exception ("...");
		//return null;
	}
	return $user_id;
});

add_action("pe_graphql_make_schema", function() 
{
    PEGraphql::add_object_type([
        'name' => 'User',
        'fields' => apply_filters(
			"pe_graphql_user_fields", 
			[
				'id' => Type::string(),
				'user_login' 	=> Type::string(),
				'user_email' 	=> Type::string(),
				'email' 		=> Type::string(),
				"post_content" 	=> Type::string(),
				'display_name' 	=> Type::string(),
				'first_name' 	=> Type::string(),
				'phone' 		=> Type::string(),
				'last_name' 	=> Type::string(),
				'user_descr' 	=> Type::string(),
				'externalId' 	=> Type::string(),
				'external' 		=> Type::string(),
				'avatar' 		=> Type::string(),
				'avatar_id' 	=> Type::string(),
				'avatar_name' 	=> Type::string(),
				'is_blocked' 	=> Type::boolean(),
				'has_vk_token' 	=> Type::boolean(),
				'password' 		=> Type::string(),
				'restored_code' => Type::string(),
				'roles' 		=> [
					'type' => Type::listOf(Type::string()),
				]
			],
			false
		),
        
    ]);

    PEGraphql::add_query(
		"getUser",  
		[
			'type' => PEGraphql::object_type("User"),
			'args' => [
				'id' => Type::string(),
				"land_id" 	=> Type::id(),
			],
			'resolve' => function ($root, $args) {
				if( $args["land_id"] ) {
					switch_to_blog( $args["land_id"] );
				}
				$user = get_user_by("id", $args["id"]); 
				if ($user->ID > 0) 
				{
					$user->id = $user->ID; 
					return apply_filters("pe_graphql_get_user", $user);
				}
				throw new PE_GraphQL_Exception ("No user!");
			}
		]
	);
    PEGraphql::add_query(
		"getUserCount",  
		[
			'type' => Type::int(),
			'args' => [ 
				"paging" 	=> [ "type" => PEGraphql::input_type("UserPaging") ],
				"land_id" 	=> Type::id(),
				
			],
			'resolve' => function ($root, $args) {
				if( $args["land_id"] ) {
					switch_to_blog( $args["land_id"] );
				}
				return count( apply_filters("pe_graphql_get_users", PEUser::get_all2( $args["paging"], $args["land_id"] )) );
				//$users  = get_users(['blog_id' => $GLOBALS['blog_id']]);
				//return count( $users );
			}
		]
	);

    PEGraphql::add_query(
		"getAllRoles",  
		[
			'type' => Type::listOf(Type::string()),
			'args' => [  
				"land_id" 	=> Type::id(),
				
			],
			'resolve' => function ($root, $args) {
				global $wp_roles;
				if( $args["land_id"] ) {
					switch_to_blog( $args["land_id"] );
				}
				add_role( 
					"School_guardian", __("School guardian", FRMRU) , 
					array(
						'edit_published_posts'		=> 1,
						'upload_files'				=> 1,
						'publish_posts'				=> 1,
						'delete_published_posts'	=> 1,
						'edit_posts'				=> 1,
						'delete_posts'				=> 1,
						'read'						=> 1
					)
				); 
				return PEUser::get_editable_roles();
			}
		]
	);
    PEGraphql::add_query(
		"getUsers",  
		[
			'type' => Type::listOf(PEGraphql::object_type("User")),
			'args' => [ 
				"paging" 	=> [ "type" => PEGraphql::input_type("UserPaging") ],
				"land_id" 	=> Type::id(),
				
			],
			'resolve' => function ($root, $args) {
				if( $args["land_id"] && $args["land_id"] > 0 ) {
					switch_to_blog( $args["land_id"] );
				}
				return apply_filters("pe_graphql_get_users", PEUser::get_all2( $args["paging"], $args["land_id"] ));
				//return  apply_filters("pe_graphql_get_users", $users);
			}
		]
	);

    PEGraphql::add_input_type([
        'name' => 'UserInput',
        'fields' =>  apply_filters(
			"pe_graphql_user_fields", 
			[
				'id' => Type::string(),
				'user_login' => Type::string(),
				'user_email' => Type::string(),
				'email' => Type::string(),
				"post_content" => Type::string(),
				'display_name' => Type::string(),
				'first_name' => Type::string(),
				'last_name' => Type::string(),
				'user_descr' => Type::string(),
				'avatar' => Type::string(),
				'avatar_id' => Type::string(),
				'avatar_name' => Type::string(), 
				'phone' => Type::string(),
				'password' => Type::string(),
				'has_vk_token' 	=> Type::boolean(),
				'roles' => [
					'type' => Type::listOf(Type::string()),
				]
			], 
			true
		),
    ]);
	
	PEGraphql::add_mutation( 
		"changeUser",
		[
			'description' 	=> __( "Change User", PE_CORE ),
			'type' 			=> PEGraphql::object_type("User"),
			'args'         	=> [
				"id"	=> [
					'type' => Type::string(),
					'description' => __( 'User unique identificator', PE_CORE ),
				],
				"land_id" => [ 'type' => Type::id() ],
				'input' => [
					'type' => PEGraphql::input_type('UserInput'),
					'description' => __( "User's new params", PE_CORE ),
				]
			],
			'resolve' => function( $root, $args, $context, $info )
			{
				require_once ABSPATH . 'wp-admin/includes/user.php';
				
				
				if( $args["land_id"] ) {
					switch_to_blog( $args["land_id"] );
				}
				
				if( !isset($args[ 'id' ]) || $args[ 'id' ] == "-1" )
				{ 
					PECore::addLog( $args, [ ], "try_register_user");
					if(!$args['input']["user_login"]) {
						$args['input']["user_login"] = $args['input']["email"];
					}
					return GQLUser::insertUser( $args['input'] );
				} 
				else
				{	 
					PECore::addLog( $args, [ ], "try_edit_user");
					return GQLUser::changeUser($args["id"], $args['input']);
				}
				/**/
			}
		]
	);
	PEGraphql::add_mutation( 
		"changeCurrentUser",
		[
			'description' 	=> __( "Change Current User", PE_CORE ),
			'type' 			=> PEGraphql::object_type('User'),
			'args'         	=> [
				'input' => [
					'type' => PEGraphql::input_type('UserInput'),
					'description' => __( "User's new params", PE_CORE ),
				],
				"land_id" => Type::id()
			],
			'resolve' => function( $root, $args, $context, $info )
			{
				require_once ABSPATH . 'wp-admin/includes/user.php';
				if( $args["land_id"] ) {
					switch_to_blog( $args["land_id"] );
				}
				if( get_current_user_id() < 1 )
				{
					throw new PE_GraphQL_Exception ("you not rights");
					//wp_die( "you not rights" );
				}
				$userData = [ 
					'ID'       		=> get_current_user_id(), 
					'user_pass' 	=> $args['input']["password"] 	? $args['input']["password"] 	: null, 
					'nickname' 		=> $args['input']["nickname"] 	? $args['input']["nickname"] 	: null, 
					'first_name' 	=> $args['input']["first_name"] ? $args['input']["first_name"] 	: null, 
					'last_name' 	=> $args['input']["last_name"] 	? $args['input']["last_name"] 	: null, 
					'phone' 		=> $args['input']["phone"] 		? $args['input']["phone"] 		: null, 
					'user_description' 	=> $args['input']["user_descr"] ? $args['input']["user_descr"] 	: null, 
					'display_name'	=> $args['input']["first_name"] && $args['input']["last_name"]
						?
						$args['input']["first_name"] . " " .  $args['input']["last_name"]
						:
						null
				];
				if( $args['input']['avatar'] )
				{
					if( substr($args['input']['avatar'],0, 4) != "http" )
					{ 
						PE_Assistants::change_user_avatar( $args['input']['avatar'], get_current_user_id() );
					}
				}
				$userData["user_email"] = $args[ 'input' ][ "user_email" ];
				$user_id = wp_update_user( $userData ); 
				
				update_user_meta( get_current_user_id(), 'post_content', 	$userData['user_description'] );
				/*
				if(
					isset( $args[ 'input' ][ "user_email" ] ) 
					&&	   $args[ 'input' ][ "user_email" ] != wp_get_current_user()->user_email
				)
				{
					// verify email field engine start
					$code 		= md5(time());
					update_user_meta( get_current_user_id(), 'new_email_code', $code);
					update_user_meta( get_current_user_id(), "new_user_email", $args[ 'input' ][ "user_email" ] );
					wp_mail(
						wp_get_current_user()->user_email,//"genglaz@gmail.com",
						sprintf( __("How to restore email registration on %s", PE_CORE),  get_bloginfo("title") ),
						sprintf( 
							__("You can change your registered email address. To do this, go to <a href='%s'>this link</a>", PE_CORE),  
							PECore::$options['web_client_url'] . "/changeemail/$user_id/$code" 
						),
						[ 'content-type: text/html' ]
					);
					return 2;
				}
				
				do_action("pe_graphql_change_current_user", $args);
				*/ 
				
				if( is_wp_error( $user_id ) )
				{
					PECore::addLog( $args, $user_id, "error_edit_current_user");
					throw new PE_GraphQL_Exception ( $user_id->get_error_message() );
				}
				PECore::addLog( $args, [ ], "edit_current_user");
				return apply_filters( "pe_graphql_get_user", wp_get_current_user() );
			}
		]
	); 	
		
	PEGraphql::add_input_type( 
		[
			"name"		=> 'UserPaging',
			'description' => __( "Pagination of Users collection", PE_CORE ),
			'fields' 		=> [
				'count' 	=> [
					'type' 	=> Type::int(),
					'description' => __( 'Count of elements in page. Default - 10', PE_CORE ),
					"defaultValue" => 10000
				],
				'offset' 	=> [
					'type' 	=> Type::int(),
					'description' => __( 'Current page. Default is 1', PE_CORE ),
					"defaultValue" => 0
				],
				"meta_relation" => [
					'type' 	=> Type::string(),
					'description' => __( 'Comare of different meta filters', PE_CORE ),
					"defaultValue" => "OR"
				],
				"order" => [
					'type' 	=> Type::string(),
					'description' => __( 'Current page. Default is 1', PE_CORE ),
					"defaultValue" => 'date'
				],
				'order_by_meta'	=> [
					'type' 	=> Type::string(),
					'description' => __( 'Current page. Default is 1', PE_CORE ),
					"defaultValue" => ''
				],
				'relation'		=> [
					'type' 	=> Type::string(),
					'description' => __( 'Current page. Default is 1', PE_CORE ),
				],
				'role__in'		=> [
					'type' 	=> Type::listOf( Type::string() ),
					'description' => __( 'List of need roles', PE_CORE ),
				],
				'metas'	=> [
					'type' 	=> Type::listOf(PEGraphql::input_type("MetaFilter")),//,
					'description' => __( ' ', PE_CORE ),
					"defaultValue" => []
				],
				'is_admin'	=> [
					'type' 	=> Type::boolean(),
					'description' => __( 'For admin panel', PE_CORE ),
					"defaultValue" => false
				],
				"search" => [
					'type' 	=> Type::string(),
					'description' => __( 'Search user substring', PE_CORE ),
					"defaultValue" => ""
				]
			],
		]
	);
	
	//register
	PEGraphql::add_mutation( 
		'verifyEmailUser', 
		[
			'description' 	=> __( "Verified User after create account", PE_CORE ),
			'type' 			=> Type::string(),
			'args'         	=> [
				'id' 	=> [
					'type' 	=> Type::string(),
					'description' => __( 'User unique identificator', PE_CORE )
				],
				'code' => [
					'type' => Type::string(),
					'description' => __( "Unique cache that user have in registration e-mail.", PE_CORE ),
				]
			],
			'resolve' => function( $root, $args, $context, $info )
			{
				$user_id = get_current_user_id();
				if($id !== $args['id']) {
					throw new PE_GraphQL_Exception ( "No verified user and no change email" );
				}
				$code				= $args['code'];
				$new_email_code	= get_user_meta((int)$args['id'], 'new_email_code', $true);
				$new_user_email	= get_user_meta((int)$args['id'], 'new_user_email', $true);
				if($new_email_code[0] == $code)
				{
					$user_id = wp_update_user( [ 
						'ID'       		=> (int)$args['id'], 
						'user_email' 	=> $new_user_email[0]
					] );
					delete_user_meta((int)$args['id'], 'new_email_code' );
					PECore::addLog( $args, [], "verify_user");
					return $new_user_email[0];
				}
				else
				{
					PECore::addLog( $args, [], "error_verify_user");
					throw new PE_GraphQL_Exception ( "No verified user and no change email" );
				}
				
			}
		]
	);
	
	
	
	PEGraphql::add_mutation( 
		"deleteUser",
		[
			'description' 	=> __( "Delete User account", PE_CORE ),
			'type' 			=> Type::boolean(),
			'args'         	=> [
				"id"	=> [
					'type' => Type::string(),
					'description' => __( 'Unique identificator of User', PE_CORE ),
				]
			],
			'resolve' => function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
			{		
				require_once ABSPATH . 'wp-admin/includes/user.php';
				if( !current_user_can('delete_users' ) )
				{
					PECore::addLog( $args, $is_delete, "illegal_delete_user");
					throw new PE_GraphQL_Exception ("you not rights");
					//wp_die( "you not rights");
				}
				
				$is_delete = wp_delete_user( $args['id'] );
				PECore::addLog( $args, $is_delete, "delete_user");
				return $is_delete;
			}
		]
	);
	
	PEGraphql::add_mutation( 
		"deleteBulkUser",
		[
			'description' 	=> __( "Delete User account", PE_CORE ),
			'type' 			=> Type::listOf(PEGraphql::object_type($class_name)),
			'args'         	=> [
				"id"	=> [
					'type' => Type::listOf(Type::id()),
					'description' => __( 'Unique identificators of User', PE_CORE ),
				],
				"land_id"	=> ['type' => Type::id()]
			],
			'resolve' => function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
			{		
				require_once ABSPATH . 'wp-admin/includes/user.php';
				if( !current_user_can('delete_users' ) )
				{
					PECore::addLog( $args, [], "illegal_delete_bulk_user");
					throw new PE_GraphQL_Exception ("you not rights");
					//wp_die( "you not rights");
				}
				if( $args["land_id"] ) { 
					switch_to_blog( $args["land_id"] );
				}
				if(is_array($args['id'])) {
					
					$users = [];
					foreach($args['id'] as $id) {
						$users[] = apply_filters("pe_graphql_get_user", get_user_by("id", $id) ) ;
						wp_delete_user( $id ); 
					}
					PECore::addLog( $args, $users, "delete_bulk_user");
					return $users;
				}
				return []; 
			}
		]
	);
	
    PEGraphql::add_input_type([
        'name' => 'TokenInput',
        'fields' => [
            'grant_type' => [
                'type' => Type::string(),
            ],
            'login' => [
                'type' => Type::string(),
            ],
            'password' => [
                'type' => Type::string(),
            ],
        ]
    ]);
	
    PEGraphql::add_input_type([
        'name' => 'RefreshInput',
        'fields' => [ 
            'refresh_token' => [
                'type' => Type::string(),
            ],
        ]
    ]);

    PEGraphql::add_object_type([
        'name' => 'UserToken',
        'fields' => [
            'access_token' => [
                'type' => Type::string(),
            ],
            'token_type' => [
                'type' => Type::string(),
            ],
            'expires_in' => [
                'type' => Type::string(),
            ],
            'refresh_token' => [
                'type' => Type::string(),
            ],
			'user_id' => [
				'type' => Type::string(),
			]
        ]
    ]);

    PEGraphql::add_query("userInfo",  [
        'type' => PEGraphql::object_type("User"),
        'args' => [ 
			"land_id" => Type::id(),
            //'message' => Type::nonNull(PEGraphql::input_type("UserInput")),
        ],
        'resolve' => function ($root, $args) { 
			global $jwt_token;
			if( $args["land_id"] ) {
				if( $args["land_id"] === "-1") {
					switch_to_blog( get_main_site_id() );
				}
				else {
					switch_to_blog( $args["land_id"] );
					if( is_multisite() ) {
						$user = wp_get_current_user();
						if( !PEUser::existsInCurrentBlog($user->ID, $args["land_id"] ) )
						{
							PECore::addLog( $args, $users, "add_user_to_blog");
							add_user_to_blog( (int)$args["land_id"], (int)$user->ID, "contributor" );
						}
					}
				}
			}
			$user = wp_get_current_user();  
            if ($user->ID > 0) {
                $user->id = $user->ID;
				$user = apply_filters("pe_graphql_get_user", $user, true);  
				return $user;
            }
			return null;
        }
    ]);

    PEGraphql::add_mutation("token",  [
        'type' => PEGraphql::object_type("UserToken"),
        'args' => [
            'input' => PEGraphql::input_type("TokenInput"),
        ],
        'resolve' => function ($root, $args) { 
			/* 
			*	wp auth user
			*/
			$user = wp_authenticate( $args["input"]["login"], $args["input"]["password"] );
			// Проверка ошибок
			if ( is_wp_error( $user ) ) {
				$error_string = $user->get_error_message();
				PECore::addLog( $args, $users, "error_token");
				throw new PE_GraphQL_Exception( __($error_string, PE_CORE) );
			}
			else {
				// юзер с указанным логином и паролем существует!
				// авторизуем его 
				wp_set_auth_cookie( $user->ID );
			}
	
			/* */ 
			//blocked user
			$is_blocked			= get_user_meta($user->ID, "is_blocked", true);
			if($is_blocked)
			{ 
				PECore::addLog( $args, $users, "token_is_blocked");
				throw new PE_GraphQL_Exception (__("This account has been blocked. Contact the portal administrator. E-mail address:", PE_CORE) . get_bloginfo("admin_email") );
			}// проверка на подтвержденный по почте аккаунт
			if(PECore::$options["user_verify_account"])
			{
				$account_activated = get_user_meta($user->ID, "account_activated", true);
				if(!$account_activated)
				{
					PECore::addLog( $args, $users, "token_as_not_verify");
					throw new PE_GraphQL_Exception (__("It is necessary to activate your account according to the instructions that you received at your postal address.", PE_CORE));
				}				
			}
			
			$jwt_token = PEJWT::generateJWSTokens( $user );
			$tokens = [ 
				"access_token" 		=> $jwt_token['access_token'],
				"expires_in"		=> $jwt_token[ 'expires_in' ],
				"refresh_token" 	=> $jwt_token['refresh_token'], 
				"token_type" 		=> $jwt_token['token_type'] 
			];
			PECore::addLog($args, $tokens, "token");
            return $tokens;
        }
    ]);

    PEGraphql::add_mutation("refresh",  [
        'type' => PEGraphql::object_type("UserToken"),
        'args' => [
            'input' => PEGraphql::input_type("RefreshInput"),
        ],
        'resolve' => function ($root, $args) {
			//wp_die( $args );
            /*
            $user = get_user_by('email', $args["input"]["login"]);
			if ( !wp_check_password( $args["input"]["password"], $user->user_pass ) ) 
			{
                throw new PE_GraphQL_Exception( __("Wrong password", PE_CORE) );
            }
			*/
			// check refresh
			if( $args[ "input" ] )
			{
				$jwt_token		= PEJWT::verifyJWS( $args[ "input" ]['refresh_token'], "refresh token" );
				wp_set_current_user( $jwt_token["payload"]["sub"]);
				$user = wp_get_current_user();
			}
			else{
				throw new PEGraphQLUnloggedException(__("Refresh: You are unlogged",PE_CORE) . ":: " . $args[ "input" ]['refresh_token']);
			}
			
			//blocked user
			$is_blocked			= get_user_meta($user->ID, "is_blocked", true);
			if($is_blocked)
			{
				throw new PE_GraphQL_Exception (__("This account has been blocked. Contact the portal administrator. E-mail address:", PE_CORE) . get_bloginfo("admin_email") );
			}
			// проверка на подтвержденный по почте аккаунт
			if(PECore::$options["user_verify_account"])
			{
				$account_activated = get_user_meta($user->ID, "account_activated", true);
				if(!$account_activated)
				{
					throw new PE_GraphQL_Exception (__("It is necessary to activate your account according to the instructions that you received at your postal address.", PE_CORE));
				}				
			}			
			$jwt_token2 = PEJWT::generateJWSTokens( $user );			
            return [ 
				"access_token" 		=> $jwt_token2['access_token'],
				"expires_in"		=> $jwt_token2[ 'expires_in' ],
				"refresh_token" 	=> $jwt_token2['refresh_token'],
				"user_id"			=> $user->ID
			];
        }
    ]);
	
	/* EXTERNAL TOKEN */
	
	
    PEGraphql::add_input_type([
        'name' => 'ExternalUserDataInput',
        'fields' => [ 
            'id' => [
                'type' => Type::string(),
            ],
            'display_name' => [
                'type' => Type::string(),
            ],
            'externalId' => [
                'type' => Type::string(),
            ],
            'external' => [
                'type' => Type::string(),
            ],
            'email' => [
                'type' => Type::string(),
            ],
            'phone' => [
                'type' => Type::string(),
            ],
            'avatar' => [
                'type' => Type::string(),
            ],
            'phone' => [
                'type' => Type::string(),
            ],
            'roles' => [
                'type' => Type::listOf(Type::string()),
            ],
        ]
    ]);
    PEGraphql::add_input_type([
        'name' => 'ExternalTokenInput',
        'fields' => [
            'grant_type' => [
                'type' => Type::string(),
            ],
            'user' => [
                'type' => PEGraphql::input_type("ExternalUserDataInput"),
            ],
            'external' => [
                'type' => Type::string(),
            ], 
        ]
    ]);
	
	PEGraphql::add_mutation("external_token",  [
        'type' => PEGraphql::object_type("UserToken"),
        'args' => [
            'input' => PEGraphql::input_type("ExternalTokenInput"),
        ],
        'resolve' => function ($root, $args) {
            $user = get_user_by('login', $args["input"]["user"]["email"]); 
			//wp_die($user);
			if(!$user) {
				PECore::addLog( $args, $user, "external_create");				
				$names = explode(" ", ($args["input"]["user"]["display_name"]));
				$data = [
					"display_name" 	=> ($args["input"]["user"]["display_name"]),
					"nickname" 		=> ($args["input"]["user"]["display_name"]),
					"first_name"	=> ($names[0]),
					"last_name"		=> ($names[1]),
					"user_email"	=> ($args["input"]["user"]["email"]),
					"user_login" 	=> ($args["input"]["user"]["email"]),
					"user_pass" 	=> MD5( ($args["input"]["user"]["externalId"]). GRAPHQL_JWT_AUTH_SECRET_KEY ),
				];
				$user_id = wp_insert_user($data); 
				if(is_wp_error( $user_id )) { 
					PECore::addLog( $args, $user_id, "external_login");
					throw new PE_GraphQL_Exception( $user_id->get_error_message() );
				}
				else {
					PECore::addLog( $args, $user_id, "success_external_create");
				}
				update_user_meta( $user_id, 'external', 	$args["input"]["external"] );
				update_user_meta( $user_id, 'externalId', 	$args["input"]["user"]["externalId"] );
				update_user_meta( $user_id, 'external', 	$args["input"]["external"] );
				update_user_meta( $user_id, 'avatar', 		$args["input"]["user"]["avatar"] ); 
				update_user_meta( $user_id, 'phone', 		$args["input"]["user"]["phone"] ); 
				update_user_meta( $user_id, 'is_blocked', 	false );
				if( $args["input"]["user"]["externalId"] === "vk" ) {
					update_user_meta( $user_id, 'has_vk_token', true ); 	
				}
				
				/* avatar copy */
				
				if( $args["input"]["user"]["avatar"] ) { 
					$image = PE_Assistants::convertImage(
						$args["input"]["user"]["avatar"], 
						ABSPATH . "/avatars/avatar".$user_id.".jpg",
						95
					);
				}
				$user = get_user_by('email', $args["input"]["user"]["email"]);
			}
			else {
				PECore::addLog( $args, $user_id, "success_external_token");
				//update_user_meta( $user_id, 'externalId', 	$args["input"]["user"]["externalId"] );
				//update_user_meta( $user_id, 'external', 	$args["input"]["external"] );
			}
            if ( !wp_check_password( 
				MD5( $args["input"]["user"]["externalId"]. GRAPHQL_JWT_AUTH_SECRET_KEY ),  
				$user->user_pass ) 
			) {
				PECore::addLog($args, $user->user_pass, "wrong_password");
                //throw new PE_GraphQL_Exception( __("Wrong password", PE_CORE) );
            }
			//blocked user
			$is_blocked			= get_user_meta($user->ID, "is_blocked", true);
			if($is_blocked)
			{
				PECore::addLog($args, [], "is_blocked");
				throw new PE_GraphQL_Exception (__("This account has been blocked. Contact the portal administrator. E-mail address:", PE_CORE) . get_bloginfo("admin_email") );
			}
						
			$jwt_token = PEJWT::generateJWSTokens( $user );
			
			
            return [ 
				"access_token" 		=> $jwt_token[ 'access_token' ],
				"expires_in"		=> $jwt_token[ 'expires_in' ],
				"refresh_token" 	=> $jwt_token[ 'refresh_token' ], 
				"token_type" 		=> $jwt_token[ 'token_type' ] 
			];
        }
    ]);
	
		
	PEGraphql::add_object_type( 
		[
			"name"		=> 'SuperAdmins',
			'description' => __( "list of Super Administration users", PE_CORE ),
			'fields' 		=> [
				'access_token' => [
					'type' => Type::string(),
				],
			],
		]
	);	
	
	PEGraphql::add_input_type( 
		[
			"name"		=> 'SetSuperAdminInput',
			'description' => __( "list of Super Administration users", PE_CORE ),
			'fields' 		=> [
				"user_id"	=> Type::id(),
				"is_add"	=> [
					"type"			=> Type::boolean(),
					"description"	=> "True to add super admin privileges. False to revoke as exists."
				]
			],
		]
	);
	
	PEGraphql::add_query( "getAllSuperAdmins",  [
        'type' => Type::listOf( PEGraphql::object_type("User") ),
        'args' => [],
        'resolve' => function ($root, $args) { 
			if( !current_user_can("manage_sites") ) {
				PECore::addLog($args, [], "getAllSuperAdmins_not_rights");
				throw new PEGraphQLNotAccess("you not rights");
			}
			$logins 		= get_super_admins();
			$users 			= [];
			foreach( $logins as $login ) {
				$users[] 	= PEUser::get_user( $login, "login" );
			}
			return $users;
		}
	]);
	
	PEGraphql::add_mutation("setSuperAdmin",  [
        'type' => Type::boolean(),
        'args' => [
            'input' => PEGraphql::input_type("SetSuperAdminInput")
        ],
        'resolve' => function ($root, $args) {
			if( !current_user_can("manage_sites") ) {
				PECore::addLog($args, [], "setSuperAdmin_not_rights");
				throw new PEGraphQLNotAccess("you not rights");
			}
			if($args["input"]["is_add"]) {
				PECore::addLog($args, [], "set_super_admin");
				return grant_super_admin(  (int)$args["input"]["user_id"] );
			}
			else {
				PECore::addLog($args, [], "revoke_super_admin");
				return revoke_super_admin( (int)$args["input"]["user_id"] );
			} 
		}
	]);
	
	PEGraphql::add_mutation("logout",  [
        'type' => Type::boolean(),
        'args' => [ ],
        'resolve' => function ($root, $args) {
			return true;
		}
	]);
	
});

add_action("pe_graphql_cookies", function()
{
	header( 'Content-mine: ' . json_encode( $_COOKIE ) );
});