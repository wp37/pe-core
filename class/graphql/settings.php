<?php

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Error\ClientAware;

add_action( 'show_user_profile',		'pegq_extra_user_profile_fields', 11);		
add_action( 'pe_graphql_user_fields',	'pe_graphql_user_fields', 11);		
add_action( 'edit_user_profile',		'pegq_extra_user_profile_fields', 11);
add_action( 'edit_user_profile_update', 'pegq_edit_user_profile_update' , 11);
add_filter('manage_users_columns' , 	'pegq_add_extra_user_column', 13);	
add_action('manage_users_custom_column','pegq_user_column_content', 99, 3);

add_action("pe_graphql_make_schema", function() 
{
    
});

function pe_graphql_user_fields( $arr )
{
	
	return $arr;
}
function pegq_extra_user_profile_fields(  $user )
{
	if(!current_user_can("manage_options")) return;
	
	//blocked user
	$is_blocked = get_user_meta($user->ID, "is_blocked", true);
	echo '
			<table class="form-table clear_table1">
				<tr>
					<th>
						<label>'. __("Is User blocked?").'</label>
					</th>
					<td>
						<input 
							type="checkbox" class="checkbox" id="$is_blocked'.$user->ID.'" name="$is_blocked" value="1" '.checked($is_blocked, 1, 0). '/>
						<label for="$is_blocked'.$user->ID.'" />
					</td>
				</tr>
			</table>
		';
}
function pegq_edit_user_profile_update($user_id)
{
	update_user_meta($user_id, "is_blocked", $_POST['$is_blocked']);
}
function pegq_add_extra_user_column( $columns )
{
	$columns['is_blocked'] = __("Is User blocked?");
	return $columns;
}
function pegq_user_column_content($value, $column_name, $user_id)
{
	$is_blocked = get_user_meta($user_id, "is_blocked", true);	
	switch($column_name )
	{
		case "is_blocked":
			$value = $is_blocked 
				? 
				"<div class='text-danger'>заблокирован</div>" 
				: 
				"<div class='text-success'>не заблокирован</div>";
			break;
	}
	return $value;
}