<?php

  
use GraphQL\Error\ClientAware;
use GraphQL\Utils\BuildSchema;
use GraphQL\Utils\SchemaPrinter;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ScalarType;

/* mime large url date author */
function getMediaData ($id , $fields = [ "url" ] ) { 
	$mediaURL = in_array("url", $fields  )
		? 
		wp_get_attachment_image_src($id, "full")[0]
		:
		"";
	return [
		"id" 	=> $id,
		"url" 	=> $mediaURL,
	];
}

class GQLMedia {
	static function init()
	{ 
		add_action("pe_graphql_make_schema", [__CLASS__, "init_media"], 48.76);
	}
	
	static function init_media () {
		PEGraphql::add_object_type( 
			[
				"name"			=> "Media",
				"description"	=> __( "Media file from media-library", PE_CORE ),
				"fields"		=> apply_filters( "bio_gq_media", [ 
					"id" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'Uniq identificator', PE_CORE )
					],
					"author"		=> [ 
						'type' => PEGraphql::object_type("User"), 			
						'description' 	=> __( 'authorize author of media-file', PE_CORE ),
						"resolve"	=> function( $root, $args, $context, $info )
						{
							// wp_die($root);
							if( isset($root["user_id"]) &&  $root["user_id"]> 0) 
							{
								$user = PEUser::get_user( $root['user_id'] );
							}
							else
							{
								$user = apply_filters(
									"bio_get_user",
									[
										"id"				=> -1,
										"ID"				=> -1,
										"display_name"		=> $root['comment_author'],
										"first_name"		=> $root['comment_author'],
										"last_name"			=> $root['comment_author'],
										"roles"				=> [],
										"caps"				=> [],
										"caps1"				=> [],			
										"user_email"		=> $root['comment_author_email'],
										"account_activated"	=> false,
										"is_blocked"		=> true,
										"__typename" 		=> "User"
									],
									new StdClass
								);
							}
							return $user;
						}
						
					], 
					PE_POST_TITLE		=> [
						'type' => Type::string()
					],
					"post_date"		=> [
						'type' => Type::string()
					],
					"date" 		=> [ 
						'type' => Type::int(), 		
						'description' 	=> __( 'UNIXtime int. Date of creation.', PE_CORE )
					], 
					"url" 		=> [
						'type' => Type::string(), 		
						'description' 	=> __( 'URL of thumbnail', PE_CORE )
					], 
					"large" 		=> [
						'type' => Type::string(), 		
						'description' 	=> __( 'URL of full file', PE_CORE )
					], 
					"mime"		=> [ 
						'type' => Type::string(), 		
						'description' 	=> __( 'mime-type', PE_CORE ) 						
					],
					"ancestors" => [
						'type' => Type::listOf(PEGraphql::object_type("Post")), 		
						'description' 	=> __( 'Achestors of media', PE_CORE ) 
					]
				] )
			]
		);
		PEGraphql::add_input_type( 
			[
				"name"		=> 'MediaInput',
				'description' => __( "Media file from media-library", PE_CORE ),
				'fields' 		=> apply_filters( "bio_gq_media_input", [ 
					"id" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'Uniq identificator', PE_CORE ), 
						'name' => 'id' 
					], 
					PE_POST_TITLE		=> [
						'type' => Type::nonNull(Type::string())
					],
					"source"		=> [
						'type' => Type::string()
					],
					"author" 		=> [ 
						'type' => Type::int(), 	
						'description' 	=> __( "Author", PE_CORE ), 
						'name' => 'author' 
					],
					"post_date"		=> [ 
						'type' => Type::int(), 		
						'description' 	=> __( 'Date of creation.', PE_CORE ) 
						
					], 
					"date"		=> [ 
						'type' => Type::int(), 		
						'description' 	=> __( 'UNIXtime int. Date of creation.', PE_CORE ) 
						
					], 
					"url"		=> [ 
						'type' => Type::string(), 		
						'description' 	=> __( 'URL', PE_CORE ) 						
					], 
					"mime"		=> [ 
						'type' => Type::string(), 		
						'description' 	=> __( 'mime-type', PE_CORE ) 						
					],
					
				]),
			]
		);
		PEGraphql::add_query( 
			'getMedia', 
			[
				'description' => __( 'Get media files', PE_CORE ),
				'type' 		=> PEGraphql::object_type("PublicOptions"),
				'args'     	=> [
					"id"		=> [ "type" => Type::string() ], 
					"land_id"	=> [ "type" => Type::id() ]	
				],
				'resolve' 	=> function( $root, $args, $context, $info )
				{	
					if( $args["land_id"] ) { 
						switch_to_blog( $args["land_id"] );
					}
					$media = get_post( $args[ "input" ]["id"] );
					$arr = [
						"id"			=> $media->ID,
						PE_POST_TITLE 	=> $media->post_title,
						"author"		=> $media->post_author,
						"post_date"		=> $media->post_date,
						"date"			=> strtotime($media->post_date),
						"url"			=> wp_get_attachment_url( $media->ID ),
						"mime"			=> get_post_mime_type( $media->ID ),
						"ancestors"		=> []
					];
					$ancestors = get_post_ancestors( $args[ "input" ]["id"] );
					if ( ! empty( $ancestors ) ) {
					  foreach ( $ancestors as $ancestor ) {
						$ancestor_post = get_post( $ancestor ); // Получаем объект предка
						$ancestor_title = $ancestor_post->post_title; // Получаем заголовок предка
						// echo '<p>' . $ancestor_title . '</p>'; // Выводим заголовок предка
						$arr[ "ancestors" ][] = [
							"id"			=> $ancestor,
							PE_POST_TITLE 	=> $ancestor_post->post_title,
							"post_content" 	=> $ancestor_post->post_content,
							"post_type"		=> $ancestor_post->post_type,
							"status"		=> strtoupper($ancestor_post->post_status)
						];
					  }
					}
					return $arr;
				}
			] 
		);
		
		PEGraphql::add_query( 
			'getMediaCount', 
			[
				'description' 	=> __( 'Get media files count', PE_CORE ),
				'type' 			=> Type::int(),
				'args'     			=> [  
					'paging'	=> [
						"type" 	=> PEGraphql::input_type("Paging") 
					], 
					"land_id"	=> Type::id()
				], 
				'resolve' 	=> function( $root, $args, $context, $info )
				{		
					if( $args["land_id"] ) { 
						switch_to_blog( $args["land_id"] );
					}
					$medias = get_posts([
						"post_type"		=> "attachment"
					]); 			
					return count($medias);
				}
			] 
		);
		PEGraphql::add_query( 
			'getMedias', 
			[
				'description' 	=> __( 'Get media files', PE_CORE ),
				'type' 			=> Type::listOf( PEGraphql::object_type("Media") ),
				'args'     			=> [  
					'paging'	=> [
						"type" 	=> PEGraphql::input_type("Paging") 
					], 
					"land_id"	=> Type::id()
				], 
				'resolve' 	=> function( $root, $args, $context, $info )
				{		
					if( $args["land_id"] ) { 
						switch_to_blog( $args["land_id"] );
					}
					$medias = get_posts([
						"post_type"		=> "attachment",
						//"post_status"	=> "publish",
						"numberposts"	=> $args["paging"]["count"],
						"offset"		=> $args["paging"]["offset"]
					]);
					//wp_die( $args["paging"] );
					$arr = [];
					foreach($medias as $media)
					{
						$arr_1 = [
							"id"		=> $media->ID,
							PE_POST_TITLE 	=> $media->post_title,
							"author"	=> $media->post_author,
							"post_date"	=> $media->post_date,
							"date"		=> strtotime($media->post_date),
							"url"		=> wp_get_attachment_thumb_url( $media->ID ),
							"large"		=> wp_get_attachment_url( $media->ID ),
							"mime"		=> get_post_mime_type( $media->ID )
						];
						$ancestors = get_post_ancestors( $media->ID );
						if ( !empty( $ancestors ) ) {
						  foreach ( $ancestors as $ancestor ) {
							$ancestor_post = get_post( $ancestor ); // Получаем объект предка 
							$ancestor_title = $ancestor_post->post_title;
							$arr_1[ "ancestors" ][] = [
								"id"			=> $ancestor,
								PE_POST_TITLE 	=> $ancestor_post->post_title,
								"post_content" 	=> $ancestor_post->post_content,
								"post_type"		=> $ancestor_post->post_type,
								"status"		=> strtoupper($ancestor_post->post_status),
							];
						  }
						}
						$arr[] = $arr_1;
					}					
					return $arr;
				}
			] 
		);
	
	
		PEGraphql::add_mutation ( 
			'changeMedia', 
			[
				'description' 	=> __( "Change single media", PE_CORE ),
				'type' 			=> PEGraphql::object_type("Media"),
				'args'         	=> [
					"id"	=> [
						"type" => Type::string(),
					],
					'input' => [
						'type' => PEGraphql::input_type("MediaInput"),
					], 
					"land_id"	=> Type::id()
				],
				'resolve' => function( $root, $args, $context, $info )
				{		
					if( $args["land_id"] ) { 
						switch_to_blog( $args["land_id"] );
					}
					if(!isset($args['id']))
					{
						if( !$args['input'][PE_POST_TITLE]) {
							throw new PE_GraphQL_Exception( "Title not exists" );
						}
						if( !$args['input']['source'] && !$args['input']['url']) {
							throw new PE_GraphQL_Exception( "Source not exists" );
						}
						
						if( $args['input']['url'] && substr($args['input']['url'],0, 4) == "http" ) {
							$media = PE_Assistants::set_media_from_url( $args['input']['url'] );
							wp_set_object_terms( 
								$media["id"], 
								(int)PECore::$options['icon_media_term'], 
								PE_CORE_MEDIA_TAXONOMY_TYPE 
							);
						}
						else if( substr($args['input']['source'],0, 11) == "data:image/"  && $args['input']['source'] )
						{
							$media = PE_Assistants::insert_media(
								[
									"data" => $args['input']['source'],
									"media_name"=> $args['input'][PE_POST_TITLE]
								],
								1
							); 
							wp_set_object_terms( 
								$media['id'], 
								(int)PECore::$options['icon_media_term'], 
								PE_CORE_MEDIA_TAXONOMY_TYPE 
							);
						}
						else {
							throw new PE_GraphQL_Exception( "Unknown source format" ); 
						}
						return $media;
					}
					else
					{
					}
					return false;
				}
			] 
		);  
		
		PEGraphql::add_mutation( 
			'deleteMedia', 
			[
				'description' 	=> __( "Delete single media", PE_CORE ),
				'type' 			=> Type::boolean(),
				'args'         	=> [
					"id"	=> [
						'type' => Type::string(),
						'description' => __( 'Unique identificator', PE_CORE ),
					], 
					"land_id"	=> Type::id()
				],
				'resolve' => function( $root, $args, $context, $info )
				{
					if( $args["land_id"] ) { 
						switch_to_blog( $args["land_id"] );
					}
					//return wp_delete_comment( $args['id'], true );
				}
			] 
		);
	}
}