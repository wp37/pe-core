<?php 
	/*
		это надкласс, расширяющий возможности страндартных WP_Post
	*/
	class PE_Taxonomy
	{
		public $id;
		public $term_id;
		public $body;
		public $meta;
		static $instances;
		static $all_ids;
		static $all_posts;
		static $args;
		function __construct($id)
		{
			if(isset($id->ID))
			{
				$this->id		= $this->ID	= $id->ID;
				$this->body		= $id;
			}
			else
			{
				$this->id		= $id;
				$this->body		= get_term( (int)$id, static::get_type() );
				//wp_die([$id, static::get_type(), $this->body]);
				//wp_die( get_term( $id, static::get_type() ) );
			}
		}
		function get($field)
		{
			require_once( PE_CORE_REAL_PATH."class/PE_Object_type.php" );
			$PE_Object_type	= PE_Object_Type::get_instance();	
			$obj	= $PE_Object_type->object[ static::get_type() ];
			
			switch($field)
			{
				case "post_title":
					return $this->body->name;
				case "post_content":
					return $this->body->description;
				default:
					//return $this->get_meta($field);
					foreach($obj as $key => $value)
					{
						if($key == 't' ||$key == 'class' ) continue;
						switch( $obj[$key]['type'] )
						{
							case "boolean":
								return (bool)$element->get_meta($key);
							case "date":
							case "number":
								$num 		= (int)$element->get_meta($key);
								if(!$num) $num = 0;
								return $num; 
							case "period":
								return $element->get_meta($key);  
							case "picto":
							case "media":
								$inc_id		= get_post_meta($p->ID, $key, true);
								$inc  		= wp_get_attachment_url( $inc_id );
								return $inc;
							case "id":
								return $element->get_meta($key); 
							case "string":
							default:
								return (string)$element->get_meta($key);
						}
					}
			}
		}
		static function get_type()
		{
			return "category";
		}
		function update_meta($field, $value)
		{
			require_once( PE_CORE_REAL_PATH."class/PE_Object_type.php" );
			$PE_Object_type	= PE_Object_Type::get_instance();		
			//wp_die($PE_Object_type->object[$field]['type']);
			switch($PE_Object_type->object[$field]['type'])
			{
				case "picto":
				case "media":
					{
						$post_id = $this->id;
						if( substr($value,0, 4) != "http"  && $value )
						{
							$media = PE_Assistants::insert_media(
								[
									"data" => $value,
									"media_name"=> "vagavaga.jpg"//$data[ $field . '_name']
								], 
								$post_id
							);
							wp_set_object_terms( $media['id'], (int)PECore::$options['icon_media_term'], PE_CORE_MEDIA_TAXONOMY_TYPE );
							update_term_meta( $post_id, $field, $media  ? $media['id'] : "" );
						}
					}
					break;
				default:
					update_term_meta($this->id, $field, $value);
			}
			
		}
		function get_meta($field )
		{
			return get_term_meta($this->id, $field, true);
		}
		static function get_title_name()
		{
			return "name";
		}
		static function get_instance($id)
		{
			$obj				= is_numeric($id) ?	$id :	$id->term_id;
			if(!static::$instances)	static::$instances = array();
			if(!isset(static::$instances[$obj]))
				static::$instances[$obj] = new static($obj);
			return static::$instances[$obj];
		}
		static function init()
		{
			$typee	= static::get_type();
			
			
		}
		
		static function get_all( $pars = [ ] )
		{
			$args = [
				'taxonomy'      => static::get_type(),
				'orderby'       => isset($pars['orderby']) ? $pars['orderby'] : 'meta_value_num', 
				'order'         => isset($pars['order'])  ? $pars['order'] : 'ASC',
				'hide_empty'    => isset($pars['hide_empty']) ? $pars['hide_empty']  : false,
				'hierarchical'  => $pars['hierarchical'] ? $pars['hierarchical'] : false, 
				'order_by_meta'	=> $pars['order_by_meta'] ? $pars['order_by_meta'] : "order", 
				//'child_of'    => 0, 
				//'pad_counts'  => $pars['pad_counts'], 
				'offset'        => isset($pars['offset']) ? $pars['offset'] : 0, 
				'number'        => 1000, 
				//'search'      => '', 
				//'childless'   => isset($pars['childless']) ? $pars['childless']: false
			];
			if( 
					isset( $pars['orderby'] ) 
				&& ($pars['orderby'] == "meta_value" || $pars['orderby'] == "meta_value_num" ) 
				|| isset( $pars['order_by_meta'] )
			)
			{
				$args['orderby']	= $pars['orderby'] ? $pars['orderby'] : "meta_value_num";
				$args['meta_key']	= $pars['order_by_meta'];
			}
			if( isset( $pars['parent'] ) )
			{
				$args['parent']	= $pars['parent'];
			}
			if( isset( $pars['post_author'] ) && $pars['post_author'] > 0 )
			{
				/**/
				$pars['metas']['post_author'] = [
					'key' 			=> 'post_author',
					'value_numeric'	=> (int)$pars['post_author']
				];
				unset ($pars['post_author']);
				
			}
			if(isset($pars["taxonomies"]))
			{
				foreach($pars["taxonomies"] as $tax)
				{ 
					$pars[ 'metas' ][ $tax[ 'tax_name' ] ] = [
						'key' 		=> $tax['tax_name'],
						'value'		=> $tax['term_ids'], 
						"compare"	=> "IN"
					];
				}
			}
			if( isset( $pars['metas'] ) )
			{
				$args['meta_query'] = [
					"relation" => isset($pars["meta_relation"]) ? $pars["meta_relation"] : "OR"
				];
				foreach($pars['metas'] as $meta)
				{
					//wp_die( $meta );
					$f =  [
						"key"		=> $meta['key'],
						'value'		=>  $meta['value'],
						"compare"	=> $meta['compare'] ? $meta['compare'] : "="
					];
					if( !in_array( $meta['compare'], [ "EXISTS", "NOT EXISTS", "IN" ] ) )
					{
						if(isset($meta['value']))
						{
							$f['value']	= $meta['value'];
							$f['type']	= "CHAR";
						}
						if(isset($meta['value_numeric']))
						{
							$f['value']	= $meta['value_numeric'];
							$f['type']	= "NUMERIC";
						}
					}
					$args['meta_query'][] = $f;
				}
			}
			$args = apply_filters( "get_all_taxonomies_args", $args, $pars );
			//wp_die( $args );
			$terms = apply_filters( "get_all_taxonomies", get_terms( $args ), $args, $pars );
			// $terms = get_terms( $args );
			//wp_die($terms);
			return $terms;
		}
		
		function is_enabled()
		{
			return isset($this->body->term_id);
		}
		
		
		function get_single_matrix( $params = -1)
		{
			$term = $this->body;
			if(is_wp_error($this->body))
			{
				return;
			}
			
			$matrix = [];
			$matrix['ID']			= $term->term_id;
			$matrix['id']			= $term->term_id;
			$matrix['post_title']	= $term->name;
			$matrix['post_content']	= $term->description;
			$matrix['post_author']	= get_term_meta($term->term_id, "post_author", true);
			$matrix['icon']			= get_term_meta($term->term_id, "thumbnail", true);
			$matrix['icon']			= is_wp_error($matrix['icon']) ? $matrix['icon']->get_error_message() : $matrix['icon'];
			$matrix['thumbnail_id']	= $matrix['icon'];
			$matrix['thumbnail']	= wp_get_attachment_url($matrix['icon']);
			$matrix['thumbnail_name'] = basename( $matrix['thumbnail'] );
			
			$matrix['parent'] = $term->parent;
			$children = [];
			if(is_array($params) && $params['children'])
			{
				/* */
				$childs = static::get_all(["parent" => $term->term_id]);
				foreach($childs as $child)
				{
					$children[] = static::get_single_matrix();
				}
				
			}
			$matrix['children']		= $children;
			return apply_filters("get_single_matrix", $matrix, $term, static::get_type());
		}
		
		static function get_all_matrixes( $params=-1 )
		{
			require_once(PE_CORE_REAL_PATH."class/PE_Object_type.php");
			$obj 	= PE_Object_type::get_instance()->get( static::get_type() );
			
			if(!is_array( $params ) )
			{
				$params = [
					"parent" => 0,
					'count' => 10000,
					'orderby' => "id",
					'order' => "DESC",
					"order_by_meta" => null,
					'hide_empty' => false,
					'offset' => 0
				];
				if( isset( $obj['class']['order'] ) )
				{
					$params['order'] = $obj['class']['order'];
				}
				if( isset( $obj['class']['order_by_meta'] ) )
				{
					$params['order_by_meta'] = $obj['class']['order_by_meta'];
					$params['orderby'] = "meta_value_num";
				}
			}
			//wp_die($params);
			
			$get_all = static::get_all($params);
			$matrixes = [];
			$i = 0;
			$count = 0;
			$offset = 0;
			if(isset($params['count']))
			{
				$count = $params['count'];
			}
			if(isset($params['offset']))
			{
				$offset	= $params['offset'];
				$count 	= $count + $offset;
			}
			foreach($get_all as $term)
			{
				if(is_wp_error($term))
				{
					wp_die($term);
				}
				if( $i < $offset)
				{
					$i++;
					continue;
				}
				if( $i >= $count)
					break;
				$elem  = static::get_instance( $term->term_id );
				
				$matrixes[] = $elem->get_single_matrix( $params );
				$i++;
			}
			return apply_filters("get_all_matrixes", $matrixes, static::get_type());
		}
		static function insert( $data )
		{
			$ins = wp_insert_term( 
				$data['post_title'], 
				static::get_type(), 
				[
					'description' => $data['post_content'],
					'parent'      => (int)$data['parent'],
					'slug'        => $data['post_name'],
				]
			);
			if(is_wp_error($ins))
			{
				return $ins;
			}
			else
			{
				$term_id = $ins['term_id'];
				update_term_meta( $term_id, "post_author", 	$data['post_author'] );
				update_term_meta( $term_id, "post_date", 	$data['post_date'] );
				if(  $data['thumbnail'] )
				{
					$media = Bio_Assistants::insert_media(
						[
							"data" => $data['thumbnail'],
							"media_name"=> $data['thumbnail_name']
						], 
						$term_id
					);
					wp_set_object_terms( $media['id'], (int)PECore::$options['icon_media_term'], PE_CORE_MEDIA_TAXONOMY_TYPE );
					update_term_meta( $term_id, "thumbnail", $media  ? $media['id'] : "" );
				}
				return $ins;
			}
		}
		static function delete($id)
		{
			return wp_delete_term( $id, static::get_type() );
		}
		
		static function update($data, $post_id)
		{ 
			$data = apply_filters( "before_update_smc_taxonomy", $data, $post_id, static::get_type() );
			$term =  wp_update_term(
				(int)$post_id,
				static::get_type(),
				[
					"name" => $data['post_title'],
					"slug" => $data['post_name'],
					"description" => $data['post_content'],					
				]
			);
			update_term_meta( $post_id, "post_author", $data['post_author'] );
			if( substr($data['thumbnail'],0, 4) != "http"  && $data['thumbnail'] )
			{
				$media = PE_Assistants::insert_media(
					[
						"data" => $data['thumbnail'],
						"media_name"=> $data['thumbnail_name']
					], 
					$post_id
				);
				wp_set_object_terms( $media['id'], (int)PECore::$options['icon_media_term'], PE_CORE_MEDIA_TAXONOMY_TYPE );
				update_term_meta( $post_id, "thumbnail", $media  ? $media['id'] : "" );
			}
			else if($data["thumbnail_id"])
			{
				update_term_meta( $post_id, "thumbnail", $data[ "thumbnail_id"]);
			}
			$post_id = apply_filters("smc_edited_term", $post_id, $data, static::get_type());
			if(!is_wp_error( $term ))
			{
				return $term["term_id"];
			}
			else
			{
				return $post_id;
			}
		}
		
		function update_thumbnail($data)
		{
			$post_id = $this->id;
			if( substr($data['thumbnail'],0, 4) != "http"  && $data['thumbnail'] )
			{
				$media = PE_Assistants::insert_media(
					[
						"data" => $data['thumbnail'],
						"media_name"=> $data['thumbnail_name']
					], 
					$post_id
				);
				wp_set_object_terms( $media['id'], (int)PECore::$options['icon_media_term'], PE_CORE_MEDIA_TAXONOMY_TYPE );
				update_term_meta( $post_id, "thumbnail", $media  ? $media['id'] : "" );
			}
		}
		
		function doubled()
		{
			// Check permissions
			if (!current_user_can('manage_categories')) {
				wp_die( __( 'You do not have permission to duplicate terms.', PE_CORE ) );
			}
			
			$term_id = $this->id; 
			$taxonomy = static::get_type();
			
			// Get original term
			$term = get_term($term_id, $taxonomy);
			if (!$term || is_wp_error($term)) {
				wp_die(esc_html__( 'Term not found.', PE_CORE ));
			}
			
			// Prepare new term name
			$new_term_name = sprintf(
				 
				__('%s (Copy)', PE_CORE),
				$term->name
			);
			$counter = 1;
			
			// Make sure the new term name is unique
			while (term_exists($new_term_name, $taxonomy)) {
				$new_term_name = sprintf(
					 
					__('%1$s (Copy %2$d)', PE_CORE),
					$term->name,
					$counter
				);
				$counter++;
			}
			// Create the duplicate term
			$new_term = wp_insert_term(
				$new_term_name,
				$taxonomy,
				array(
					'description' => $term->description,
					'slug'        => sanitize_title($new_term_name),
					'parent'      => $term->parent
				)
			);
			if (is_wp_error($new_term)) {
				wp_die(sprintf( 
					esc_html__('Error creating duplicate term: %s', PE_CORE),
					esc_html($new_term->get_error_message())
				));
			}
			
			// Copy term meta
			$term_meta = get_term_meta($term_id);
			if ($term_meta) {
				foreach ($term_meta as $meta_key => $meta_values) {
					foreach ($meta_values as $meta_value) {
						add_term_meta( $new_term['term_id'], $meta_key, maybe_unserialize($meta_value) );
					}
				}
			}
			
			/* */
		}
	}