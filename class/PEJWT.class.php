<?php

class PEJWT
{
	static function init()
	{
		
	}
	static function generateJWTToken( $payload, $expireSecs)
	{
		$alg = 'HS256';
		$token = new \Gamegos\JWT\Token();
		$token->setClaim ('sub', 'someone@example.com' );
		$token->setClaim( 'exp', time() + $expireSecs );
		
	}
	static function generateJWSTokens( $user )
    {
        $accessToken 	= static::generateJWS( $user, PECore::$options['jwt']['pe-user-acces-token'],   24 * 60 * 60 ); 
        $refreshToken 	= static::generateJWS( $user, PECore::$options['jwt']['pe-user-refresh-token'], 30 * 24 * 60 * 60 );
        return [
            "refresh_token"	=> $refreshToken,
            "access_token"	=> $accessToken,
			"expires_in"	=> time() + 2 * 60,
			"token_type"	=> "JWT"
        ];
    }
	static function generateJWS( $user, $key, $expireSecs )
	{		 
		global $jwsString;
		global $allExpireSincs;
		$time 		= time();
		$allExpireSincs = $time + $expireSecs;
		$av = (string)get_user_meta($user->ID, "avatar", true);
		$payload 	= [
			'sub' 			=> $user->ID,
			'login' 		=> $user->user_login, 
			'display_name' 	=> $user->display_name,
			"rules"			=> $user->roles,
			"avatar"		=> $av ? get_bloginfo("url") . $av : "",
			'iat' 			=> $time
		];
		$headers 	= [
			'alg' 		=> 'HS256', //alg is required
			'typ' 		=> 'JWT',
			'iss'		=> get_bloginfo("url"),
			'cty'		=> 'application/json',
			'exp'		=> $allExpireSincs,
			'b64'		=> false,
			//'crit'	=> ['exp', 'b64']
		];  
		$jws 		= new \Gamegos\JWS\JWS();
		$jwsString 	= $jws->encode( $headers, $payload, $key );
		if( $key === PECore::$options['jwt']['pe-user-refresh-token'] )
		{		
			add_action(
				"pe_graphql_cookies", 
				function()
				{
					global $allExpireSincs;
					global $jwsString;
					setcookie(
						"refresh", 
						$jwsString, 
						$allExpireSincs, 
						"/", 
						$_SERVER['HTTP_ORIGIN'], 
						false, 
						false
					); 
				}
			);
		}
		return $jwsString;
	}
	
	static function verifyJWS($headers, $typ = "refresh token")
	{
		
		$key = $typ === "refresh token"
			? 
			PECore::$options['jwt']['pe-user-refresh-token']
			:
			PECore::$options['jwt']['pe-user-acces-token'];
		$jws = new \Gamegos\JWS\JWS();
		$jwt_token = $jws->verify($headers, $key );		
		return $jwt_token;
	}
}