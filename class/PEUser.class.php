<?php
/* 
require_once(PE_CORE_REAL_PATH . "vendor/webonyx/autoload.php");
*/
require_once( PE_CORE_REAL_PATH . "vendor/webonyx/graphql-php/src/GraphQL.php"); 
require_once( PE_CORE_REAL_PATH . "vendor/webonyx/graphql-php/src/Error/ClientAware.php");
require_once( PE_CORE_REAL_PATH . "vendor/webonyx/graphql-php/src/Utils/BuildSchema.php");
require_once( PE_CORE_REAL_PATH . "vendor/webonyx/graphql-php/src/Utils/SchemaPrinter.php"); 
require_once( PE_CORE_REAL_PATH . "vendor/webonyx/graphql-php/src/Type/Definition/Type.php");

use GraphQL\Error\ClientAware;
use GraphQL\Utils\BuildSchema;
use GraphQL\Utils\SchemaPrinter; 
use GraphQL\Type\Definition\Type;
/**/

class PEUser
{
	static function init()
	{	
		add_filter( 'pe_graphql_get_user', 			[__CLASS__, 'pe_graphql_get_user' ],9, 2);		
		add_filter( 'pe_graphql_change_user', 		[__CLASS__, 'pe_graphql_change_user' ],10, 2);		
		add_action( 'show_user_profile', 			[__CLASS__, 'extra_user_profile_fields' ],9);		
		add_action( 'edit_user_profile', 			[__CLASS__, 'extra_user_profile_fields' ],9);
		add_action( 'edit_user_profile_update', 	[__CLASS__, 'edit_user_profile_update' ],9, 2); 
		add_filter( 'manage_users_columns' , 		[__CLASS__, 'add_extra_user_column'], 2);	
		add_action( 'manage_users_custom_column',	[__CLASS__, 'user_column_content'], 99, 3);
		add_action( 'pe_graphql_change_meta_user',	[__CLASS__, 'pe_graphql_change_meta_user'], 99, 2);
		add_action( "pe_graphql_make_schema", 		[__CLASS__, "exec_graphql"], 8); 
	}
	static function exec_graphql()
	{
		try
		{
			static::register_gq( );
		}
		catch(PE_GraphQL_Exception $ew)
		{
			
		}
		catch(PEGraphQLNotLogged $ew)
		{
			
		}
		catch(PEGraphQLNotAdmin $ew)
		{
			
		}
		catch(PE_GraphQL_Exception $ew)
		{
			
		}
	}
	static function register_gq()
	{
		PEGraphql::add_query( 
			'getFullUserBlockedCount', 
			[
				'description' 		=> __( "Get count of all blocked users", PE_CORE ),
				'type' 				=> Type::int(),
				'args'     			=> [ ],
				'resolve' 			=> function( $root, $args, $context, $info )
				{
					global $wpdb;	 
					$users = get_users([
						"fields"	=> "ID",
						'meta_query' => [
							'relation' => 'OR',
							[
								'key' => 'is_blocked',
								'value' => '1'
							] 
						]
					]);
					return count($users);
				}
			] 
		);
	}
	static function pe_graphql_change_user( $user_data, $id )
	{
		return $user_data;
	}
	static function pe_graphql_get_user( $user, $isMe = false )
	{ 
		$homeUrl = is_multisite() ? network_home_url() : get_home_url();
		$user->id = $user->ID; 
		$user->user_descr = get_user_meta($user->ID, "post_content", true); 
		$user->avatar 	= file_exists( ABSPATH . "avatars/avatar" . $user->ID . ".jpg" ) 
			?
			$homeUrl . "/avatars/avatar".$user->ID . ".jpg"
			:
			$homeUrl . "/avatars/user_ava.png";
				
		$user->user_login	= $user->user_login;
		$user->is_blocked	= (bool)get_user_meta($user->ID, "is_blocked", true);
		$user->has_vk_token	= get_user_meta($user->ID, "has_vk_token", true);
		$user->external		= get_user_meta($user->ID, "external", true);
		$user->externalId	= get_user_meta($user->ID, "externalId", true);
		$user->restored_code = get_user_meta($user->ID, "restored_code", true);
		if(!$user->roles) {
			$user->roles	= [];
		}
		if(is_super_admin( $user->ID )) {
			$user->roles = array_merge( ( is_array($user->roles) ? $user->roles : [] ), [ "SuperAdmin" ] );
		}
		$user->email 		= $user->user_email;
		unset( $user->caps);
		unset( $user->allcaps);
		if($isMe) {
			$user->first_name 	= $user->user_firstname;  
			$user->last_name 	= $user->user_lastname; 
		}
		return $user;
	}
	static function extra_user_profile_fields( $user )
	{
		$account_activated = get_user_meta($user->ID, "account_activated", true) ? 1 : 0;
		echo '
			<table class="form-table clear_table1">
				<tr>
					<th>
						<label>'. __("Is verified e-mail", BIO).'</label> 
					</th>
					<td>
						<input 
							type="checkbox" 
							class="checkbox" 
							id="$account_activated'.$user->ID.'" 
							name="$account_activated" 
							value="1" 
							role_user_id="'.$user->ID.'" 
							 '.checked($account_activated, 1, 0). '
						/>
						<label for="$account_activated'.$user->ID.'" />
					</td>
				</tr>
				<tr>
					<th>
						<label>'. __("Roles", BIO).'</label>
					</th>
					<td>'.
						static::role_group_iface( $user->ID ) .
					'</td>
				</tr>
			</table>
		';
		//$user->display_name;
	}
	static function edit_user_profile_update($user_id )
	{
		update_user_meta($user_id, "account_activated", $_POST['account_activated']);
		//update_user_meta($user_id, "user_descr", $data['user_descr']);
	}
	static function get_full_count($params= 1)
	{
		if(!is_array($params)) $params = [];
		
		$args = [
			'orderby'   => 'display_name',
			'order'      => 'ASC',
		];
		if($params['class'])
		{
			$args['meta_query'] = [];
			$args['meta_query'][] = ['key' => "studentclass", "value" => $params['class'], "operator" => "OR"];
		}
		if($params['role__in'])
		{
			$args['role__in'] = $params['role__in'];
		}
		if($params['include'])
		{
			$args['include'] = $params['include'];
		}
		if($params['search'])
		{
			$args['search'] = "*".$params['search']."*";
		}
		$users = get_users( $args );
		return count($users);
	}
	static function get_all2($params= 1, $site_id="-1", $only_args = false)
	{
		// FmRU::update_activation_1();
		$homeUrl = is_multisite() ? network_home_url() : get_home_url();
		if(!is_array($params)) $params = [];
		if(!isset($params["count"]))
			$params["count"] = -1;
		if(!isset($params["offset"]))
			$params["offset"] = 0;
		
		$args = [
			"offset" 	=> $params["offset"],
			"number"	=> $params["count"],
			'orderby'   => 'display_name',
			'order'      => 'ASC',
		];
		if($params['class'])
		{
			$args['meta_query'] = [];
			$args['meta_query'][] = ['key' => "studentclass", "value" => $params['class'], "operator" => "OR"];
		}
		if($params['role__in'])
		{
			$args['role__in'] = $params['role__in'];
		}
		if($params['include'])
		{
			$args['include'] = $params['include'];
		}
		if($params['search'])
		{
			$args['search'] = "*".$params['search']."*";
		}
		if($params["metas"]) {
			$meta_query = [ ];
			if($params['relation']) {
				$meta_query['relation'] = $params['relation'];
			}
			foreach($params["metas"] as $metas) {
				if($metas["key"] === "external") {
					$value = explode(",", $metas["value"]);
					$compare = "IN";
				}
				else {
					$value = $metas["value"];
					$compare = "LIKE";
				}
				$meta_query[] = [
					'key' 	=> $metas["key"],
					'value' => $value,
					'compare'=> $compare
				];
			}
			$args['meta_query'] = $meta_query;
		}
		
		$args['fields'] = $params['fields'] ? [$params['fields']] : 'all'; 
		
		$users = apply_filters(
			"bio_get_users", 
			is_multisite() && $site_id == "-1" 
				? 
				static::get_all_from_multisite( $params ) 
				: 
				array_unique( get_users( $args ), SORT_REGULAR ), 
			$params
		);
		$result	= [];
		foreach($users as $re)
		{
			//$av = (string)get_user_meta($re->ID, "avatar", true);
			$user = apply_filters( "pe_graphql_get_user", $re );
			if(!$user->roles || !count($user->roles ) ) {
				$user->roles = $re->roles;
			}
			$result[] = $user;
			/*[
				"ID"			=> $re->ID,
				"id"			=> $re->ID,
				"display_name" 	=> $re->display_name,
				"user_descr" 	=> get_user_meta($re->ID, "post_content" , true), 
				"external" 		=> get_user_meta($re->ID, "external" , true), 
				"externalId" 	=> get_user_meta($re->ID, "externalId" , true), 
				"user_registered" => date_i18n( 'j M Y, H:m', strtotime( $re->user_registered)),
				"user_email"	=> $re->user_email,
				"email"	=> $re->user_email,
				"rules"			=> $re->roles,
				"roles"			=> $re->roles,
				"avatar"		=> file_exists( ABSPATH. "avatars/avatar".$re->ID.".jpg" ) 
					?
					$homeUrl . "/avatars/avatar".$re->ID.".jpg"
					:
					$homeUrl . "/avatars/user_ava.png"
			];*/
		}
		return $only_args ? $args : $result;
	}

	static function get_all($params= 1)
	{
		if(!is_array($params)) $params = [];
		if(!isset($params["numberposts"]))
			$params["numberposts"] = 10;
		if(!isset($params["offset"]))
			$params["offset"] = 0;
		global $wpdb;
		$query = "SELECT u.ID, u.display_name, u.user_email, um.meta_value AS rules FROM " . $wpdb->prefix . "users AS u
		LEFT JOIN ".$wpdb->prefix . "usermeta AS um ON um.user_id=u.ID 
		WHERE um.meta_key='" . $wpdb->prefix . "capabilities'
		LIMIT " . ($params["offset"] * $params["numberposts"]) . ", " . $params["numberposts"] . "; ";
		$res = $wpdb->get_results($query);
		$result	= [];
		foreach($res as $re)
		{
			$rss = [];
			foreach($re as $key=>$val)
			{
				$rss[$key] = $key == "rules" ? array_keys( unserialize( $val ) ) : $val;			
			}
			$result[] = $rss;
		}
		return $result;
	}
	static function get_all_from_multisite ($params = -1) {
		global $wpdb;
		$where = "";
		if( $search = $params["search"] ) {
			$where .= " WHERE display_name LIKE '%$search%' OR user_login LIKE '%$search%' ";
		}
		$query = "SELECT * FROM ".$wpdb->base_prefix."users $where"; 
		$results = $wpdb->get_results($query);
		// wp_die( $search );
		return $results;
	}
	static function add_extra_user_column($columns)
	{
		unset($columns['role']);
		$columns['roles'] = __("Roles", PE_CORE);
		$columns['params'] = __("Parameters", PE_CORE);
		return $columns;
	}
	 
    static function parse_rules( $cls )
    {
        $rules = [];
        $all = get_full_bio_roles();
        foreach( $cls as $r)
        {
             $rules[] = $all[$r][0];
        }
        return $rules;
    }

    

    function user_column_content($value, $column_name, $user_id)
	{
		switch($column_name )
		{
			case "params":
				
				break;
			case "roles":
				return static::role_group_iface( $user_id );
		}
		return $value;
	}

    static function delete( $post_id )
    {
        $post_id = (int)$post_id;
        return $post_id;
    }

	static function role_group_iface( $user_id )
	{
		$html = " ";
		foreach(get_full_roles() as $r)
		{
			$html .= "<div class='row '>
				<input 
					type='checkbox' 
					class=checkbox 
					id='is_".$r[0]."_$user_id' 
					value='1' " . 
					checked(1, (int)static::is_user_role($r[0] , $user_id), 0) . 
					" 
					role_user_id='$user_id' 
					role='".$r[0]."'
				/>		
				<label for='is_".$r[0]."_$user_id'>".
					__( $r[1], BIO ).
				"</label>
			</div>";
		}
		return $html;
	}
    static function pe_graphql_change_meta_user( $id, $data )
	{
		return static::update( $data, $id, false );
	}
    static function update( $data, $id, $is_admin=false )
    { 
        $user_id 	= (int)$id;
		$user		= get_userdata( $user_id );
		/*
        if($data['psw'] && $is_admin==false)
        {
            wp_update_user([
                "ID"		=> $user_id,
                "user_login"=> $data["email"],
                "user_email"=> $data["email"],
                "display_name"=> $data['fname']." ".$data['sname'],
                "first_name"=> $data['fname'],
                "last_name" => $data['sname'],
                "user_pass" => $data['psw']
            ]);
		}*/
		//wp_die( $data );
		$u 		= ["ID" => $id];
		if($data["email"]) {
			$u["user_email"] =	$data["email"];
		}
		if($data["first_name"]) {
			$u["first_name"] = $data["first_name"];
		}
		if($data["last_name"]) {
			$u["last_name"]	= $data["last_name"];
		}
		if($data["first_name"] && $data["last_name"]) {
			$u["display_name"] = $data["first_name"] . " " . $data["last_name"];
		}
		if($data["display_name"]) {
			$u["display_name"] = $data["display_name"];
		}
		if($data["password"]) {
			$u["password"] = $data["password"];
		}
		wp_update_user( $u );
		

		if($data['avatar_name'])
		{
			if(!function_exists('wp_handle_upload')){
				require_once ABSPATH . 'wp-admin/includes/image.php';
				require_once ABSPATH . 'wp-admin/includes/file.php';
				require_once ABSPATH . 'wp-admin/includes/media.php';
			}
			// get rid of everything up to and including the last comma
			$imgData1 	= $data['avatar'];
			$imgData1 	= substr($imgData1, 1 + strrpos($imgData1, ','));
			// write the decoded file
			$ext 		= substr($data['avatar_name'], strrpos($data['avatar_name'], ".")+1);
			$nm 		= $user_id . "." . $ext;
			$filePath	= ABSPATH. '/avatars/' . $nm ;
			$fileURL	= get_bloginfo("url")  . '/avatars/' . $nm;
			file_put_contents($filePath, base64_decode($imgData1));
			update_user_meta($user_id, "avatar", '/avatars/' . $nm );
		}
        update_user_meta($user_id, "mname", $data['mname']);
        update_user_meta($user_id, "post_content", $data['user_descr']);
        update_user_meta($user_id, "birthday", $data['birthday']);
        update_user_meta($user_id, "phonenumber", $data['phonenumber']);
        update_user_meta($user_id, "anover", $data['anover']);
        update_user_meta($user_id, "graduationyear", $data['graduationyear']);
        update_user_meta($user_id, "parentphonename1", $data['parentphonename1']);
        update_user_meta($user_id, "parentphonename2", $data['parentphonename2']);
        update_user_meta($user_id, "parentphonenumber2", $data['parentphonenumber2']);
        update_user_meta($user_id, "parentphonenumber1", $data['parentphonenumber1']);
        update_user_meta($user_id, "usergroup", $data['usergroup']);
        update_user_meta($user_id, "school", $data['school']);
        update_user_meta($user_id, "studentclass", $data['studentclass']);
        update_user_meta($user_id, "town", $data['town']);
        update_user_meta($user_id, "gender", $data['gender']);
		if($data['usergroup'] == "teacher")
		{
			$user->add_role("Teacher");
		}
		else
		{
			$user->remove_role("Teacher");
		}
		if($data['usergroup'] == "anover")
		{
			$user->add_role("contributor");
		}
		else
		{
			$user->remove_role("contributor");
		}
		if($data['usergroup'] == "pupil")
		{
			$user->add_role("Pupil");
		}
		else
		{
			$user->remove_role("Pupil");
		}
        return $id;
	}
	
	static function createGUID() { 
    
		// Create a token
		$token      = $_SERVER['HTTP_HOST'];
		$token     .= $_SERVER['REQUEST_URI'];
		$token     .= uniqid(rand(), true);
		
		// GUID is 128-bit hex
		$hash        = strtoupper(md5($token));
		
		// Create formatted GUID
		$guid        = '';
		
		// GUID format is XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX for readability    
		$guid .= substr($hash,  0,  8) . 
			 '-' .
			 substr($hash,  8,  4) .
			 '-' .
			 substr($hash, 12,  4) .
			 '-' .
			 substr($hash, 16,  4) .
			 '-' .
			 substr($hash, 20, 12);
				
		return $guid;
	}	

    static function insert( $data )
    {
        $post_id = null;
        $answ = wp_insert_user([
            "user_login"	=> static::createGUID(),
            "user_pass"		=> $data["password"],
            "user_email"	=> $data["user_email"],
            "first_name"	=> $data["first_name"],
            "last_name"		=> $data["last_name"],
			"role"			=> "contributor"
        ]);
		if( is_wp_error( $answ) )
		{
			throw new PE_GraphQL_Exception( "User not registered. " . $answ->get_error_message() );			
		}
        return $answ;
    }
	static function get_caps($user_id =-1)
	{
		$len = 30;
		$roles = static::get_roles($user_id);
		$caps = [0,0,0,0,0,0,0,0];
		$num = (int)( $capability / $len );
		foreach($roles as $role)
		{
			$caps[0] |= PECore::$options['caps'][$role][0];			
			$caps[1] |= PECore::$options['caps'][$role][1];			
			$caps[2] |= PECore::$options['caps'][$role][2];			
			$caps[3] |= PECore::$options['caps'][$role][3];			
			$caps[4] |= PECore::$options['caps'][$role][4];			
			$caps[5] |= PECore::$options['caps'][$role][5];			
			$caps[6] |= PECore::$options['caps'][$role][6];			
			$caps[7] |= PECore::$options['caps'][$role][7];			
		}
		return $caps;
	}
	
	static function access_caps_gq($capability, $msg)
	{
		if( ! static::is_user_cap($capability) )
			throw new \PEGraphQLNotAccess( $msg );
	}
	static function is_user_cap($capability, $user_id =-1)
	{
		$user_id = $user_id > 0 ? $user_id : get_current_user_id();
		$roles = static::get_roles($user_id);
		$caps = false;
		$caps2 = [];
		foreach($roles as $role)
		{
			$caps  = $caps || static::is_cap($capability, $role);	 
			$caps2[]  = static::is_cap($capability, $role);	 
		}
		return $caps;
	}
	static function is_cap($capability, $role)
	{
		if( static::is_user_roles(['administrator']) ) return true;
		$len = 30;
		$num = (int)( $capability / $len );
		//return [$num, PECore::$options['caps'][$role][$num], pow( 2, $capability % $len ) ];
		return ( PECore::$options['caps'][$role][$num] & pow( 2, $capability % $len ) ) > 0;
	}
	static function is_user_role( $role, $user_id = null ) 
	{
		$user = is_numeric( $user_id ) ? get_userdata( $user_id ) : wp_get_current_user();
		if( ! $user )
			return false;
		return in_array( $role, (array) $user->roles );
	}
	static function is_user_roles( $array_roles, $user_id = null ) 
	{
		if(!is_array($array_roles)) $array_roles = [$array_roles];
		$user = is_numeric( $user_id ) ? get_userdata( $user_id ) : wp_get_current_user();
		if( ! $user )
			return false;
		return array_intersect( $array_roles, (array) $user->roles );
	}
	static function get_roles( $user_id = null )
	{
		$user = is_numeric( $user_id ) ? get_userdata( $user_id ) : wp_get_current_user();
		$roles = array();
		if($user && is_array($user->roles))
		{
			foreach($user->roles as $role)
				$roles[] = $role;
		}
		return $roles;
	} 
	/*
		@userId - id of user
		@roles -- array for add to User's roles
	*/
	static function add_role( $user_id = null, $roles=[]) {
		if( !count( $roles ) ) return false;
		$user = is_numeric( $user_id ) ? get_userdata( $user_id ) : wp_get_current_user();
		foreach($roles as $role) {
			$user->add_role($role);
		}
		return true;
	}
	
	static function find_user_email($search="34567890")
	{
		global $wpdb;
		$query = "SELECT ID, user_email, display_name FROM " . $wpdb->prefix . "users WHERE user_email LIKE '%$search%';";
		return $wpdb->get_results($query);
	}
	
	static function existsInCurrentBlog($userId, $site_id)
	{
		$blogs = get_blogs_of_user( $userId, true);
		$user_exists_in_blogs	= array_keys( $blogs ); 
		return in_array($site_id, $user_exists_in_blogs);
	}
	
	static function addAutoUserToBlog($user, $festId) {
		$fmru_users 	= [];
		$blogs 			= get_blogs_of_user( $user->ID );
		foreach($blogs as $blog) {
			$fmru_users = get_blog_option($blog->userblog_id, "fmru_users") ;
			foreach($fmru_users as $fuser) {
				if( $fuser["login"] === $user->user_email ) {
					$autoUserData = $fuser;
					break(2);
				}
			}
		} 
		if($autoUserData) {
			$fmru_users2 	= get_blog_option($festId, "fmru_users");
			
			foreach($fmru_users2 as $fuser) {
				if($fuser["login"] === $autoUserData["login"])
					return;
			}
			$fmru_users2[] = $autoUserData;
			update_blog_option($festId, "fmru_users", $fmru_users2 );
		}
	}
	
	static function get_user($user_id, $by = "id")
	{
		$user = get_user_by( $by, $user_id );
		$homeUrl = is_multisite() ? network_home_url() : get_home_url();
		return apply_filters(
			"bio_get_user",
			[
				"id"				=> $user->ID,
				"ID"				=> $user->ID,
				"display_name"		=> $user->display_name,
				"first_name"		=> $user->first_name,
				"avatar"			=> file_exists( ABSPATH . "avatars/avatar" . $user->ID . ".jpg" ) 
					?
					$homeUrl . "/avatars/avatar".$user->ID . ".jpg"
					:
					$homeUrl . "/avatars/user_ava.png",
				"user_descr"		=> get_user_meta($user->ID, 'post_content', true), 
				"last_name"			=> $user->last_name,
				"roles"				=> $user->roles,
				"caps"				=> static::get_caps( $user_id ),
				//"caps1"				=> decbin(static::get_caps( $user_id )),			
				"user_email"		=> $user->user_email,
				"email"				=> $user->user_email,
				"account_activated"	=> get_user_meta($user->ID, 'account_activated', true),
				"has_vk_token"		=> get_user_meta($user->ID, 'has_vk_token', true),
				"external"			=> get_user_meta($user->ID, 'external', true),
				"externalId"		=> get_user_meta($user->ID, 'externalId', true),
				"is_blocked"		=> get_user_meta($user->ID, 'is_blocked', true),
				"__typename" 		=> "User"
			],
			$user
		); 
	}
	
	/**/
	
	static function get_editable_roles() {
		global $wp_roles;

		$all_roles = $wp_roles->roles;
		$editable_roles = apply_filters('editable_roles', $all_roles); 
		$roles = [];
		foreach($editable_roles as $key=>$value) {
			$roles[] = $key;
		}
		return $roles;
	}
}
