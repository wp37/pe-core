<?php 
use GraphQL\GraphQL;
use GraphQL\Type\Schema;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\SchemaConfig;
use GraphQL\Utils\Utils;
use GraphQL\Error\Debug;
use GraphQL\Utils\SchemaPrinter;

class PECore 
{
	static $options;
	static $instance;
	static function get_instance()
	{
		if(!static::$instance)
			static::$instance = new static;
		return static::$instance;
	}
	function __construct()
	{
		static::$options = get_option(PE_CORE); 
		
		add_action( 'admin_enqueue_scripts',	[__CLASS__, 'add_admin_js_script'], 99 );
		add_action( 'admin_menu',				[__CLASS__, 'admin_page_handler'], 5);	
		
		// GraphQL publicOptions  
		add_filter( "pe_add_option",			[ __CLASS__, "pe_add_option"], 2 );
		add_filter( "bio_gq_options",			[ __CLASS__, "bio_gq_options"] );
		add_filter( "bio_get_gq_options",		[ __CLASS__, "bio_get_gq_options"] );
		add_filter( "pe_get_public_gq_options", [ __CLASS__, "pe_get_public_gq_options"], 20, 2 );
	}
	static function pe_add_option( $options ) {
		$options["vk_client_id"] = [
			'type' => 'string',  
			"name" => __("Register VK application (https://vk.com/apps?act=manage) and put this key 'Application ID'.", PE_CORE), 
			"hidden" => false,
			"tab"		=> "Public options" 
		];
		$options["vk_client_secret"] = [
			'type' => 'string',  
			"name" => __("Register VK application (https://vk.com/apps?act=manage) and put this key 'Service key'.", PE_CORE), 
			"hidden" => true,
			"tab"		=> "Public options" 
		];
		$options["yandex_client_id"] = [
			'type' => 'string',  
			"name" => __("...", PE_CORE), 
			"hidden" => false,
			"tab"		=> "Public options" 
		];
		$options["yandex_client_secret"] = [
			'type' => 'string',  
			"name" => __("...", PE_CORE), 
			"hidden" => true,
			"tab"		=> "Public options" 
		];
		$options["yandex_client_token"] = [
			'type' => 'string',  
			"name" => __("...", PE_CORE), 
			"hidden" => true,
			"tab"		=> "Public options" 
		];
		$options["telegramm_client_id"] = [
			'type' => 'string',  
			"name" => __("...", PE_CORE), 
			"hidden" => false,
			"tab"		=> "Public options" 
		];
		$options["icon_media_term"] = [
			'type' => 'string',  
			"name" => __("...", PE_CORE), 
			"hidden" => false,
			"tab"		=> "Public options" 
		];
		$options["user_media_term"] = [
			'type' => 'string',  
			"name" => __("...", PE_CORE), 
			"hidden" => false,
			"tab"		=> "Public options" 
		];
		$options["test_media_term"] = [
			'type' => 'string',  
			"name" => __("...", PE_CORE), 
			"hidden" => false,
			"tab"		=> "Public options" 
		];
		$options["jwt"] = [
			'type' => 'string',  
			"name" => __("...", PE_CORE), 
			"hidden" => false,
			"tab"		=> "Public options" 
		];
		return $options;
	}
	static function bio_gq_options( $matrix )
	{
		$PE_Object_Type 	= PE_Object_Type::get_instance();		
		foreach( $PE_Object_Type->options as $option => $value )
		{
			$type = Type::string(); 
			switch( $value["type"] )
			{
				case "boolean":
					$type = Type::boolean();
					break;
				case "media":
					$type = Type::string();// PEGraphql::object_type("Media") ;					
					break;
				case "email":
				case "url":
				case "string":
				default:
					$type = Type::string();
					break;
			}
			
			$matrix[$option] = $type;
		}
		return $matrix;
	}
	
	static function bio_get_gq_options( $matrix )
	{ 
		$PE_Object_Type 	= PE_Object_Type::get_instance(); 		
		foreach( $PE_Object_Type->options as $option => $value )
		{
			$matrix[$option] = static::$options[$option];
		}
		return $matrix;
	}
	
	static function pe_get_public_gq_options( $matrix, $Special=[] )
	{ 
		$PE_Object_type 	= PE_Object_Type::get_instance();
		foreach( $PE_Object_type->options as $option => $value )
		{
			if( !$Special['is_load_menus'] && $option == "name" ) continue;
			if( !$Special['is_load_menus'] && $option == "description" ) continue;
			if( !$value['hidden'] )
			{
				$matrix[$option] = static::$options[$option];
			}
		} 
		return $matrix;
	}
	/**/
	static function add_admin_js_script()
	{
		wp_deregister_script("bootstrap");
		wp_register_style(PE_CORE, PE_CORE_URLPATH . 'assets/css/pe-core.css' );
		wp_enqueue_style( PE_CORE );
		
		/**/
		wp_register_style("bootstrap", "https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css");
		wp_enqueue_style( "bootstrap" );
		wp_register_script("bootstrap-js", "https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js");
		wp_enqueue_script("bootstrap-js");
		
		wp_register_style("fontawesome", 'https://use.fontawesome.com/releases/v5.5.0/css/all.css', []);
		wp_enqueue_style( "fontawesome" );
		wp_register_style("formhelpers", PE_CORE_URLPATH . 'assets/css/bootstrap-formhelpers.min.css', array());
		wp_enqueue_style( "formhelpers");		
		wp_register_style("periodpicker", PE_CORE_URLPATH . 'assets/css/jquery.periodpicker.min.css', array());
		wp_enqueue_style( "periodpicker");		
		
		wp_register_script("datetimepicker", PE_CORE_URLPATH . 'assets/js/jquery.datetimepicker.js' , array());
		wp_enqueue_script("datetimepicker");
		wp_register_script("formhelpers", plugins_url( '../js/bootstrap-formhelpers.min.js', __FILE__ ), array());
		wp_enqueue_script("formhelpers");
		wp_register_script(PE_CORE,  PE_CORE_URLPATH . 'assets/js/pe-core.js', array());
		wp_enqueue_script( PE_CORE);
		
		// load media library scripts
		wp_enqueue_media();
		wp_enqueue_style( 'editor-buttons' );
		//ajax
		wp_localize_script( 
			'jquery', 
			'peajax', 
			array(
				'url' => admin_url('admin-ajax.php'),
				'nonce' => wp_create_nonce('peajax-nonce')
			)
		); 	
		wp_localize_script( 
			'jquery', 
			'dict', 
			apply_filters("pe_core_dictionary", [ ])
		); 	
	}
	static function activate()
	{		
		
	}
	static function deactivate()
	{
		
	} 
	
	static function admin_page_handler()
	{
		add_menu_page( 
			__('Protopia Ecosystem', PE_CORE), 
			__('Protopia Ecosystem', PE_CORE),
			'manage_options', 
			'pe_core_page', 
			[__CLASS__, 'get_admin'], 
			PE_CORE_URLPATH . "assets/img/pe_logo.svg",
			'2.1'
		);		
	}
	
	static function get_admin()
	{
		require_once PE_CORE_REAL_PATH . "tpl/admin.php" ;
	}
	
	static function addLog( $data, $error, $cahpter = "log") {
		global $blog_id;
		if (!file_exists(ABSPATH . '/log')) 
		{
			mkdir(ABSPATH . '/log', 0777, true);
		}
		$user = PEUser::get_user( get_current_user_id() );
		$log = "\r\n\r\n" . 
			date('d m Y h:i:s a', time()) . "\r\n" . 
			json_encode( $error, JSON_UNESCAPED_UNICODE ) . "\r\n" . 
			json_encode( $data, JSON_UNESCAPED_UNICODE ) . "\r\n" . 
			json_encode( $user, JSON_UNESCAPED_UNICODE ) . "\r\n" ;
		
		file_put_contents(ABSPATH  . '/log/' . $blog_id. ".". $cahpter . "." . date("j-n-Y") .'.log', $log, FILE_APPEND);
	}
}