<?php

class PEMessage extends PE_Post
{
	public $categories; 
	static function init()
	{	
		add_filter( "smc_add_post_types",	[ __CLASS__, "init_obj"], 12.7688);
		parent::init();
	}
	
	static function get_type()
	{
		return PE_MESSAGE;
	}
	
	static function init_obj($init_object) // 
	{
		$p					= [];
		$p['t']				= ['type' => 'post'];	
		$p['class']			= [
			'type' 		=> 'PEMessage',
			"title"		=> __("User's message", PE_CORE) 
		]; 
		$p[ "resource_id" ] 	= [ 'type' => "number", "name"	=> 	__("Resource ID", FRMRU),	"thread" => true ]; 
		$p[ "resource_type" ] 	= [ 'type' => "string", "name"	=> 	__("Resource type", FRMRU), "thread" => true ]; 
		$p[ "resource_title" ] 	= [ 'type' => "string", "name"	=> 	__("Resource title", FRMRU), "thread" => true ]; 
		$p[ "is_viewed" ] 		= [ 'type' => "boolean", "name"	=> 	__("Is viewed", FRMRU), "thread" => true ]; 
		
		$init_object[ static::get_type() ]	= $p; 
		return $init_object;
	}
}