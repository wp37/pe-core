<?php
use GraphQL\GraphQL;
use GraphQL\Type\Schema;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\SchemaConfig;
use GraphQL\Utils\Utils;
use GraphQL\Error\Debug;
use GraphQL\Utils\SchemaPrinter;
class PEActivation
{
	static $options;
	static $instance;
	static function get_instance()
	{
		if(!static::$instance)
			static::$instance = new static;
		return static::$instance;
	}
	function __construct()
	{
		static::$options = get_option(PE_CORE); 
		$this->acttivateMediaTerms();
		$this->activateSchema();
		$this->activateCommentsDiscussionType();
	}
	function activateSchema()
	{
		global $wpdb;		
		$query = "CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."pe_refresh_token` (
	`ID` int(255) NOT NULL AUTO_INCREMENT,
	`user_id` int(255) NOT NULL,
	`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`ID`),
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1;";
		$wpdb->query($query);
	}
	function acttivateMediaTerms()
	{ 
		global $wpdb;		 
		
		/*
		*	
		*/
		if( !isset ( static::$options['vk_client_id'] )) {
			static::$options['vk_client_id'] = "";
		}
		if( !isset ( static::$options['vk_client_secret'] )) {
			static::$options['vk_client_secret'] = "";
		}
		if( !isset ( static::$options['yandex_client_id'] )) { 
			static::$options['yandex_client_id'] = "";
		}
		if( !isset ( static::$options['yandex_client_secret'] )) {
			static::$options['yandex_client_secret'] = "";
		}
		if( !isset ( static::$options['yandex_client_token'] )) {
			static::$options['yandex_client_token'] = "";
		}
		if( !isset ( static::$options['telegramm_client_id'] )) {
			static::$options['telegramm_client_id'] = "";
		}
		if( !isset ( static::$options['odnoklassniki_client_id'] )) {
			static::$options['odnoklassniki_client_id'] = "";
		}
		if((int)static::$options['icon_media_term'] < 1)
		{
			$icon_term = wp_insert_term( "icons", PE_CORE_MEDIA_TAXONOMY_TYPE, [
				'description' => '',
				'parent'      => 0,
				'slug'        => '',
			] );
			static::$options['icon_media_term'] = $icon_term->term_id;
		}
		if((int)static::$options['test_media_term'] < 1)
		{
			$test_term = wp_insert_term( "tests", PE_CORE_MEDIA_TAXONOMY_TYPE, [
				'description' => '',
				'parent'      => 0,
				'slug'        => '',
			] );
			static::$options['test_media_term'] = $test_term->term_id;
		}
		if((int)static::$options['test_media_free'] < 1)
		{
			$free_term = wp_insert_term( "User upload", PE_CORE_MEDIA_TAXONOMY_TYPE, [
				'description' => '',
				'parent'      => 0,
				'slug'        => '',
			] );
			static::$options['test_media_free'] = $free_term->term_id;
		}	
		// JWT
		if(!static::$options['jwt'])
		{
			static::$options['jwt'] = [
				'pe-user-acces-token' 		=> substr( MD5("pe-user-access-token" .  time() . rand(0, mt_getrandmax())), 0, 128 ),
				'pe-user-refresh-token' 	=> substr( MD5("pe-user-refresh-token" . time() . rand(0, mt_getrandmax())), 0, 128 ) 
			];
		} 
		update_option(PE_CORE, static::$options);
	}
	function activateCommentsDiscussionType()
	{
		
		global $wpdb;			
		$query = "ALTER TABLE `".$wpdb->prefix."comments` ADD `discussion_type` VARCHAR( 10 ) NOT NULL DEFAULT 'post' AFTER `comment_post_ID` ;";
		$wpdb->query($query);
		
	}
}