<?php
/*
	Это класс необходимых утилит и автонастроек
*/


class PE_Assistants
{
	
	static function init()
	{
		add_filter( 'upload_mimes', 				[__CLASS__, 'upload_allow_types'] );
		//add_action( 'user_register', 				[__CLASS__, 'my_registration'], 10, 2 );
		add_action( 'before_delete_post', 			[__CLASS__, 'before_delete_post'], 10, 2 );
		add_action( 'delete_post ', 				[__CLASS__, 'delete_post '], 10, 2 );
		
		//cron log		
		add_filter( 'pre_schedule_event', 			[__CLASS__, 'pre_schedule_event'], 10, 3 );
		add_filter( 'schedule_event', 				[__CLASS__, 'schedule_event'] );
		
		if (!file_exists(ABSPATH . '/temp')) 
		{
			mkdir(ABSPATH . '/temp', 0777, true);
		}
	}
	/*
	 * @param null|bool|WP_Error $result   The value to return instead. Default null to continue adding the event.
	 * @param object             $event    {
	 *     An object containing an event's data.
	 *
	 *     @type string       $hook      Action hook to execute when the event is run.
	 *     @type int          $timestamp Unix timestamp (UTC) for when to next run the event.
	 *     @type string|false $schedule  How often the event should subsequently recur.
	 *     @type array        $args      Array containing each separate argument to pass to the hook's callback function.
	 *     @type int          $interval  Optional. The interval time in seconds for the schedule. Only present for recurring events.
	 * }
	 * @param bool               $wp_error Whether to return a WP_Error on failure.
	 */
	static function pre_schedule_event ( $null, $event, $wp_error ) {
		PECore::addLog( [], $event, "_CRON_pre_schedule_event");
		return $null;
	}
	static function schedule_event( $event ) {
		PECore::addLog( [], $event, "_CRON_schedule_event");
		return $event;
	}
	static function before_delete_post( $postId, $post )
	{ 
		static::delete_attachments($postId);
	}
	static function delete_post ( $postId, $post )
	{ 
		static::delete_attachments($postId);
	}
	static function delete_attachments($postId)
	{ 
		$childrens = get_children( [
			'post_parent' => $postId,
			'post_type'   => 'attachment',
			'numberposts' => -1,
			'post_status' => 'any'
		] );

		if( $childrens )
		{
			foreach( $childrens as $children )
			{
				// var_dump($children);
				// wp_die();
				$attach_id    = $children->ID;
				$meta         = wp_get_attachment_metadata( $attach_id );
				$backup_sizes = get_post_meta( $attach_id, '_wp_attachment_backup_sizes', true );
				$file         = get_attached_file( $attach_id );

				wp_delete_attachment_files( $attach_id, $meta, $backup_sizes, $file );
				wp_delete_attachment( $children->ID, true );
			}
		}
		delete_post_thumbnail( $postId );		
	}
	static  function upload_allow_types( $mimes ) 
	{
		// разрешаем новые типы
		$mimes['svg']  = 'image/svg+xml';
		$mimes['doc']  = 'application/msword'; 
		// отключаем имеющиеся
		unset( $mimes['mp4a'] );
		return $mimes;
	}
	
	static function my_registration( $user_id ) 
	{
		// get user data
		$user_info = get_userdata($user_id);
		// create md5 code to verify later
		$code = md5(time());
		// make it into a code to send it to user via email
		$string = array('id'=>$user_id, 'code'=>$code);
		// create the activation code and activation status
		update_user_meta($user_id, 'account_activated', 0);
		update_user_meta($user_id, 'activation_code', $code);
		// create the url
		$url = get_site_url(). '/my-account/?act=' .base64_encode( serialize($string));
		// basically we will edit here to make this nicer
		$html = 'Please click the following links <br/><br/> <a href="'.$url.'">'.$url.'</a>';
		// send an email out to user
		wp_mail( $user_info->user_email, __('Email Subject', PE_CORE) , $html);
	}

	/*
	*
	*	Utilities
	*
	*/
	
	static function insert_media( $data, $post_id=0 )
	{
		require_once(ABSPATH . '/wp-admin/includes/file.php');
		require_once(ABSPATH . '/wp-admin/includes/media.php');
		require_once(ABSPATH . '/wp-admin/includes/image.php');
		
		// get rid of everything up to and including the last comma
		$imgData1 = $data['data'];
		$imgData1 = substr($imgData1, 1 + strrpos($imgData1, ','));
		$ext 		= explode( ".", $data['media_name'] );
		$ext		= $ext[ count($ext) - 1 ];
		$mediaName = substr($data['media_name'], -20 );
		
		
		// write the decoded file
		$filePath	= ABSPATH . 'temp/' . $mediaName;
		$fileURL	= ABSPATH . 'temp/' . $mediaName;
		$bin		= base64_decode($imgData1); 
		
		//extention
		$size = getImageSizeFromString( $bin );
		
		$ext = "jpg";
		if ( !( empty($size['mime']) || strpos($size['mime'], 'image/') !== 0 ) )
		{
			$ext = substr($size['mime'], 6);
		}
		$filePath 	= $filePath;// . "." . $ext;
		$fileURL	= $fileURL;// . "." . $ext;
		
		//
		if(!file_put_contents( $filePath, $bin )) {
			throw new Error( "Temp directory not open or not exists" );
		}
		//$id = media_sideload_image( $fileURL, 0, $mediaName, "id" ); 
		$file_array = [];
		
		// корректируем имя файла в строках запроса.
		preg_match('/[^?]+\.(jpg|jpe|jpeg|gif|png|svg|webp)/i', $fileURL, $matches);
		$file_array['name'] = basename($matches[0]);
		$file_array['tmp_name'] = $filePath; 
		
		$id = media_handle_sideload( $file_array, $post_id, $mediaName );
			
		// удалим временный файл
		@unlink( $file_array['tmp_name'] );
		@unlink( $filePath );		
		if(is_wp_error($id))
		{
			//throw new Error( "error upload: ".$id->get_error_message() . ":  $mediaName" ); 
			return [ "url" => $url, "id" => "-1" ];
		}
		$url = wp_get_attachment_url( $id );
		return ["url" => $url, "id" => $id];
	}
	
	static function change_user_avatar( $data, $userId )
	{
		require_once(ABSPATH . '/wp-admin/includes/file.php');
		require_once(ABSPATH . '/wp-admin/includes/media.php');
		require_once(ABSPATH . '/wp-admin/includes/image.php');
		// get rid of everything up to and including the last comma
		$imgData1 = $data;
		$imgData1 = substr($imgData1, 1 + strrpos($imgData1, ','));
		// write the decoded file
		$filePath	= ABSPATH. 'avatars/avatar' . $userId . '.jpg'; 
		$bin		= base64_decode($imgData1); 
		// wp_die( $data );
		file_put_contents(
			$filePath, 
			$bin
		);
	}
	
	static function set_media_from_url( $url ) { 
		require_once ABSPATH . 'wp-admin/includes/image.php'; 
		$filename	= explode("/", $url);
		$filename	= $filename[count($filename) - 1]; 
		$uploaddir 	= wp_upload_dir();
		$uploadfile = $uploaddir['path'] . '/' . $filename; 
		$contents = file_get_contents($url);
		
		if(!$contents) { 
			return [ "url" => "", "id" => "" ];
		}
		$savefile = fopen($uploadfile, 'w');
		fwrite($savefile, $contents);
		fclose($savefile);
		
		$wp_filetype = wp_check_filetype(basename($filename), null );
		if(!$wp_filetype['type']) {
			return [ "url" => "", "id" => "" ];
		}
		$attachment = array(
			'post_mime_type' 	=> $wp_filetype['type'],
			'post_title' 		=> $filename,
			'post_content' 		=> '',
			'post_status' 		=> 'inherit'
		);

		$attach_id		= wp_insert_attachment( $attachment, $uploadfile );
		 
		$imagenew 		= get_post( $attach_id );
		$fullsizepath 	= get_attached_file( $imagenew->ID );
		if(!$fullsizepath) {
			return [ "url" => "", "id" => "" ];
		}
		$attach_data 	= wp_generate_attachment_metadata( $attach_id, $fullsizepath );
		wp_update_attachment_metadata( $attach_id, $attach_data );
		
		return [ "url" => get_attachment_link( $attach_id ), "id" => $attach_id ];  
	}
	
	static function extract_img($content, $title, $post_id=0)
	{
		return $content;
		mb_internal_encoding("UTF-8");
		$doc = new DOMDocument();
		$doc->strictErrorChecking = false;
		@$doc->loadHTML($content);
		
		$tags = $doc->getElementsByTagName('img');
		$d = [];
		foreach ($tags as $tag) 
		{
				$prefix = substr($tag->getAttribute('src'), 0,5);
				if($prefix == "data:")
				{
					//$d[] = $tag->getAttribute('src');
					$data = [ "data" => $tag->getAttribute('src'), "media_name" => $title.".jpg"];
					$img = static::insert_media( $data, $post_id );
					wp_set_object_terms( $img['id'], (int)PECore::$options['test_media_free'], PE_CORE_MEDIA_TAXONOMY_TYPE );
					$tag->setAttribute("src", $img['url']);
				}				
		}
		$html =  $doc->saveHTML( );
		$html = str_replace('<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"', '', $html);
		$html = str_replace('"http://www.w3.org/TR/REC-html40/loose.dtd">', '', $html);
		$html = str_replace('<html><body>', '', $html);
		$html = str_replace('</body></html>', '', $html);
		return $html;
	}	
	
	static function admin_form($type, $meta, $meta_name, $data = [])
	{
		switch($type)
		{
			case "smc_object":
				return PE_Object_Type::dropdown([
					"selected"	=> $meta,
					"name"		=> $meta_name,
					"class"		=> "form-control ".$data["class"].""
				]);
			case "geo":
				$meta = is_array($meta) ? $meta : [];
				return "<div class='row'>
					<div class='col-md-4 text-right align-middle'>".
						__("Latitude", PE_CORE).
					"</div>
					<div class='col-md-8'>
						<input type='number' step='0.00001' name='".$meta_name."[]' id='lat_$meta_name' value='$meta[0]' class='form-control'/>
					</div>
				</div>
				<div class='row'>
					<div class='col-md-4 text-right align-middle'>".
						__("Longitude", PE_CORE).
					"</div>
					<div class='col-md-8'>
						<input type='number' step='0.00001' name='".$meta_name."[]' id='lon_$meta_name' value='$meta[1]' class='form-control'/>
					</div>
				</div>
				<div class='row'>
					<div class='col-md-4 text-right align-middle'>".
						__("Zoom", PE_CORE).
					"</div>
					<div class='col-md-8'>
						<input type='number' name='".$meta_name."[]' id='z_$meta_name' value='$meta[2]' class='form-control'/>
					</div>
				</div>";
				break;
			case "email":
				return "<div class='input-group '>
					<span class='input-group-text' id='basic-addon1'>@</span>
					<input 
						type='' 
						class='form-control ".$data["class"]."' 
						placeholder='Username@email.com' 
						aria-label='Username' 
						aria-describedby='basic-addon1'
						name='$meta_name' 
						value='$meta'
					>
				</div>";
			case "url":
				return "<div class='input-group '>
					<span class='input-group-text' id='basic-addon1'><span class='web-svg'></span></span>
					<input 
						type='' 
						class='form-control ".$data["class"]."' 
						placeholder='https://...' 
						aria-label='URL' 
						aria-describedby='basic-addon1'
						name='$meta_name' 
						value='$meta'
					>
				</div>";
			case "number":
			case "string":
			case "radio":
				return "<div class='input-group '>
					<span class='input-group-text' id='basic-addon1'><span class='pen-svg'></span></span>
					<input 
						type='' 
						class='form-control ".$data["class"]."' 
						placeholder='https://...' 
						aria-label='URL' 
						aria-describedby='basic-addon1'
						name='$meta_name' 
						value='$meta'
					>
				</div>";
				break;
			case "____date":
				return $meta ? date("d.m.Y   H:i", $meta) : "";
				break;
			case "boolean":
				return "<div class='form-check '>
					<input 
						type='checkbox' 
						class='checkbox ".$data["class"]."' 
						name='$meta_name' 
						id='$meta_name' 
						value='1' 
						".checked(1,$meta,0)."
					>
					<label class='form-check-label ' for='$meta_name'></label>
				</div>";
			
			case "picto":
			case "media":
				$media_title = __("Load");
				if($meta)
				{
					$media_title = wp_basename(wp_get_attachment_url( $meta ));
				}
				return "<div ajax_com='".$data["class"]."' ajax_attrs='$meta_name' >".
					// get_input_file_form2("default_img", $meta, $meta_name,"") .
				"</div>";
			case "array":
				return implode(", ", $meta);
				break;
			case "date":
				return date("d.m.Y H:i", $meta);
				break;
			case "period":
				return implode("<BR>", $meta); 
			case "post":
				if($meta)
				{
					$p = get_post($meta);
					$post_title = $p ? $p->post_title : "";
					$color = "";
					return $meta>0 ? "
						<strong>$post_title</strong>
						<br><div class='IDs'><span style='background-color:$color;'>ID</span>$meta</div>"
						:
						"--";
				}
				break;							
			case "taxonomy":
				if($term)
				{
					$term = get_term_by("term_id", $meta, $elem);
					return $term ? "<h6>".$term->name ."</h6> <div class='IDs'><span>ID</span>".$meta. "</div>
						<div style='background-color:#$color; width:15px;height:15px;'></div>" : $meta;
				}
				break;
			case "user":
				$user = get_user_by("id", $meta);
				return $user ? "<h6>".($user->display_name)."</h6> <div class='IDs'><span>ID</span>".$meta. "</div>
						<div style='background-color:#$color; width:15px;height:15px;'></div>" : $meta;
				break;
			case "post_status":
				$post_status = $p->get("post_status");
				$is = $post_status == "publish" ? 1 : 0;
				return "
				<div class='row'>
					<div class='col-12'>
						<input type='checkbox' class='checkbox' id='is_publish_$post_id' value='$is' ".checked(1, (int)$is, 0) . " publish_id='$post_id'/>		
						<label for='is_publish_$post_id'>".__("Publish", BIO)."</label>
					</div>
				</div>";
				break;
			case "id":
			default:
				/*
				$elem = $PE_Object_Type->get_object($meta, $obj[$column_name]["object"] );
				switch( $obj[$column_name]["object"])
				{
					case "user":
						if($meta)
						{
							$user = get_user_by("id", $meta);
							$display_name = $user ? $user->display_name : "==";
							return  $display_name."<br><div class='IDs'><span>ID</span>".$meta. "</div>
								<div style='background-color:#$color; width:15px;height:15px;'></div>";
						}
						break;
					case "post":
						if($meta)
						{
							$p = get_post($meta);
							$post_title = $p->post_title;
							return $meta>0 
								? 
								"
								<strong>$post_title</strong>
								<br>
								<div class='IDs'><span>ID</span>".$meta. "</div>
								<div style='background-color:#$color; width:15px;height:15px;'></div>" 
								: 
							"";
						}
						break;
					case "taxonomy":
						$term = get_term_by("term_id", $meta, $elem);
						return $term ? "<h6>".$term->name ."</h6> <div class='IDs'><span>ID</span>".$meta. "</div>
							<div style='background-color:#$color; width:15px;height:15px;'></div>" : $meta;
						break;
					default:
						return apply_filters(
							"smc_post_fill_views_column",
							$meta, 
							$obj[$column_name]['type'],
							$obj,
							$column_name,
							$post_id
						);
				}
				break;
				*/
				return "default";
		}
	}
	static function convertImage( $originalImage, $outputImage, $quality = 75 ) {
		// jpg, png, gif or bmp?
		$exploded = explode('.',$originalImage);
		$ext = $exploded[count($exploded) - 1];
		if (preg_match('/jpg|jpeg/i',$ext))
			$imageTmp = imagecreatefromjpeg($originalImage);
		else if (preg_match('/png/i',$ext))
			$imageTmp = imagecreatefrompng($originalImage);
		else if (preg_match('/gif/i',$ext))
			$imageTmp = imagecreatefromgif($originalImage);
		else if (preg_match('/bmp/i',$ext))
			$imageTmp = imagecreatefrombmp($originalImage);
		else
			return 0;
		// quality is a value from 0 (worst) to 100 (best)
		
		header('Content-Type: image/jpeg');
		$jpg = imagejpeg($imageTmp, $outputImage, $quality);
		imagedestroy($imageTmp);
		return $jpg;
	}
}