<?php

class PELandGroup extends PE_Post {
	static function init()
	{	
		add_filter( "smc_add_post_types",	[ __CLASS__, "init_obj"], 12.8883);
		parent::init();
	}
	
	static function get_type()
	{
		return PE_LAND_GROUP;
	}
	
	static function init_obj($init_object) // 
	{
		$p					= [];
		$p['t']				= ['type' => 'post'];	
		$p['class']			= [
			'type' 		=> 'PELandGroup',
			"title"		=> __("User's message", PE_CORE) 
		]; 
		
		$init_object[ static::get_type() ]	= $p; 
		return $init_object;
	}
}