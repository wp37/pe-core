<?php 
	/*
		это надкласс, расширяющий возможности страндартных WP_Post
	*/
	
	class PE_Post
	{
		public $id;
		public $ID;
		public $body;
		public $meta;
		static $instances;
		static $all_ids;
		static $all_posts;
		static $args;
		function __construct($id)
		{
			if(isset($id->ID))
			{
				$this->id		= $this->ID	= $id->ID;
				$this->body		= $id;
			}
			else
			{
				$this->id		= $id;
				$body 			= get_post( $id ); 
				$this->body		= $body;
			} 
		}
		function is_enabled()
		{
			return isset($this->body->ID);
		}
		static function get_title_name()
		{
			return PE_POST_TITLE;
		}
		public static function clear_instance($id)
		{
			if(!static::$instances)	static::$instances = array();
			$i = 0;
			foreach(static::$instances as $instance)
			{
				if($instance->id == $id)
					array_splice(static::$instances, $i, 1);
				$i++;
			}
		}
		public static function get_instance($id)
		{
			// wp_die("PE POST");
			$obj	= is_numeric($id) ?	$id :	$id->ID;
			if(!static::$instances)	
				static::$instances = array();
			if(!isset(static::$instances[$obj]))
				static::$instances[$obj] = new static($obj);
			return static::$instances[$obj];
		}
		static function insert($data)
		{ 
			$id = wp_insert_post([
				"post_type"		=> $data[PE_POST_TYPE] ? wp_strip_all_tags($data[PE_POST_TYPE]) : static::get_type(),
				'post_name'    	=> $data[PE_POST_NAME],
				'post_title'    => ObsceneCensorRus::check(
					$data[PE_POST_TITLE] 
						? 
						$data[PE_POST_TITLE] 
						: 
						$data["post_title"]
				),
				'post_content'  => ObsceneCensorRus::check(
					$data[PE_POST_CONTENT] 
						? 
						$data[PE_POST_CONTENT] 
						: 
						""
				),
				'post_status'   => 'publish',
				"post_author"	=> $data[PE_POST_AUTHOR] ? $data[PE_POST_AUTHOR] : get_current_user_id()
			], true);
			if(is_wp_error($id))
			{
				return $id->get_error_message();
			}
			$post	= static::get_instance($id); 
			$post->update_metas($data);
			$post->update_thumbnail($data);	
			return $post;
		}
		
		static function update($data, $id)
		{
			$cd = [];
			foreach($data as $key => $val)
			{
				if( in_array( 
					$key, 
					["post_type", 'post_name', 'post_title', 'post_content', 'post_status', "post_author", "thumbnail"] 
				) )
					$cd[$key]	= $val;
				if( $data[ PE_POST_TITLE ] ) {
					$cd[ "post_title" ] = $data[ PE_POST_TITLE ];
				}
				
			}
			$cd['ID']	= $id;
			$id		= wp_update_post( $cd );
			$post	= static::get_instance($id);
			$post->update_thumbnail($data);			
			$post->update_metas($data);
			return $post;
		}
		function doubled()
		{
			$metas		= [
				'post_title' 	=> $this->body->post_title . ' (Copy)', 
				'post_content'  => $this->body->post_content
			];
			require_once(PE_CORE_REAL_PATH."class/PE_Object_Type.php");
			$PE_Object_Type	= new PE_Object_Type();
			$object				= $PE_Object_Type->object;
			foreach($object[static::get_type()] as $key=>$val)
			{
				if($key 		== "t") continue;
				$metas[$key]	= $this->get_meta($key);
			}
			$metas = apply_filters("smc_before_doubled_post", $metas, $this);
			$metas['post_author'] = $this->get("post_author");
			$post =  static::insert($metas);	
			do_action("smc_after_doubled_post", $post, $this);
			return  $post;
		}
		static function delete($id)
		{
			if(is_numeric($id))
			{ 
				return wp_delete_post($id);
			}
			else
			{ 
				return wp_delete_post($id->ID);
			}
		}
		
		
		function update_thumbnail ( $data )
		{ 
			if(!isset($data['thumbnail']) || !$data['thumbnail']) return false;
			try {
				if( substr($data['thumbnail'],0, 4) != "http" && $data['thumbnail'] )
				{
					$media = PE_Assistants::insert_media(
						[
							"data" => $data['thumbnail'],
							"media_name"=> $data['thumbnail_name']
						], 
						$this->id
					); 
					wp_set_object_terms( $media['id'], (int)PECore::$options['icon_media_term'], PE_CORE_MEDIA_TAXONOMY_TYPE );
					set_post_thumbnail( $this->id, $media['id'] );
					return true;
				}
				else
				{
					set_post_thumbnail( $this->id, $data['thumbnail_id'] );
					return true;
				} 
			}
			catch( \Exception $e ) {
				
			}
		}
		
		function update_metas($meta_array)
		{
			/* terms */
			$taxonomy_names = get_object_taxonomies( $this->body->post_type );
			
			/*
			foreach($taxonomy_names as $tax)
			{
				$taxField = PE_Object_Type::fieldToName($meta_array[$tax], PEFestProject::get_type());
				wp_die( $meta_array );
				if(isset( $taxField ) )
				{
					wp_set_object_terms( $this->id, $meta_array[$tax], $tax );
					delete( $meta_array[$tax]);
				}	
			}
			*/
			
			$data	= array();
			foreach($meta_array as $meta => $val)
			{
				if( $meta	== PE_POST_TITLE || $meta	== 'post_content' )
				{
					$data[$meta] = $val;
					continue;
				}
				if( $meta	== 'title' || $meta	== 'name' || $meta == 'obj_type' )
				{
					continue;
				} 
				
				// add term
				$taxField = PE_Object_Type::fieldToName( $meta, PEFestProject::get_type());
				if( $taxField && in_array($taxField, $taxonomy_names )) {
					wp_set_object_terms( $this->id, $meta_array[ $meta ], $taxField );
					unset( $meta_array[ $meta ] );
				}
				
				//var_dump( [$meta, $val] );
				$this->update_meta($meta, $val);
			}
			if(count($data))
			{
				$data['ID'] = $this->id;
				$id			= wp_update_post($data);
				//insertLog("update_metas", $id);
			}
		}
		public function get_meta($name)
		{
			$meta = get_post_meta($this->id, $name, true);
			if(!$meta)
			{
				$obj	= $PE_Object_Type->object[ static::get_type() ]; 
				$field 	= $obj[$name];
				if($field['default'])
				{
					$meta = $field['default'];
				}
			}
			return $meta;
		}
		public function update_meta($name, $value)
		{ 
			update_post_meta( $this->id, $name, $value );
			return $value;
		}
		public function get($field)
		{ 
			return $this->body ?  $this->body->$field : "";
		}
		function set($field)
		{
			$this->body->$field	= $field;
			wp_update_post($this->body);
		}
		public function get_the_author()
		{
			global $authordata;
			$autor_id		= $this->body->post_author;
			$authordata		= get_userdata($autor_id);
			$author			= apply_filters("the_author", $authordata->display_name);
			return $author;
		}
		
		static function get_all_matrixes($params, $sort="")
		{ 
			$all = static::get_all_with_params(
				[
					isset($params['metas']) 			
						? 
						$params['metas'] 		
						: 
						-1,	// []
					isset($params['count'])			
						? 
						$params['count'] 	
						: 
						-1,	// -1
					isset($params['offset'])			
						? 
						$params['offset']		
						: 
						0,	// 0
					isset( $params['order_by_meta'] ) && $params['order_by_meta'] == "title" 		
						? 
						$params['order_by_meta']	
						: 
						"title",	// 'id'
					isset( $params['order'] )	 && $params['order']		
						? 
						$params['order']		
						: 
						'DESC',// 'DESC'
					isset( $params['order_by_meta'] ) && $params['order_by_meta'] != "title"	
						? 
						$params['order_by_meta']
						: 
						"",	// ""
					"all",		// $pars['fields'],
					isset($params['relation'])		
						? 
						$params['relation']	
						: 
						"AND",// "AND",
					isset($params['post_author'])		
						? 
						$params['post_author']	
						: 
						-1,	// -1,
					isset($params['taxonomies'])		
						? 
						$params['taxonomies']	
						: 
						-1,	//
					"OR", 
					isset($params['post_status'])		
						? 
						$params['post_status']
						: 
						"publish", //,
					isset($params['search'])
						? 
						$params['search']
						:
						-1 
				], 
				$params, 
				$sort="" 
			); 
			// wp_die([$params, count($all)]);
			// wp_die(count($all));
			// wp_die($all);
			$matrixes = [];
			foreach($all as $single)
			{
				$matrixes[] = static::get_single_matrix($single); 
			}
			return $matrixes;
		}
		static function get_fake_matrix()
		{
			$obj	= $PE_Object_Type->object[ static::get_type() ];	
			$matrix = [];
			foreach($obj as $key => $value)
			{
				$v = $value["default"] ? $value["default"] : null;
				$matrix[$key ] = $v;
			}
			return $matrix;
		}
		static function get_single_matrix($p)
		{ ;
			if($p == -1 || !$p ) return static::get_fake_matrix();
			
			$post = static::get_post($p);
			
			return $post->ID 
				? 
				["id" => $post->ID] 
				: 
				$post; 
			//static::get_fake_matrix();
		}
		static function get_post($p)
		{
			if(is_numeric($p))
			{
				$p = get_post($p);
			}
			$element = static::get_instance($p); 
			
			if(!$element->id) {
				return null;
			}
			$thumbnail			= get_the_post_thumbnail_url( $element->id, "full" );
            $thumbnail			= $thumbnail ? $thumbnail : PE_CORE_EMPTY_IMG;
            $thumbnail_id		= (int)get_post_thumbnail_id( $p->ID ) ;
			
			$cats				= [];		
			/*
            $categs	= get_the_terms($p->ID, "category");			
            if(count($categs) && $categs && !is_wp_error($categs))
            {
                foreach($categs as $categ)
                {
                    $cats[]		= PE_Taxonomy::get_single_matrix( $categ );
                }
            }
			*/ 
			$arg = [
				"id"			=> $p->ID,
				"ID"			=> $p->ID,
				PE_POST_TITLE	=> $p->post_title,
				PE_POST_STATUS	=> $p->post_status,
				PE_POST_CONTENT	=> $p->post_content, 
				PE_POST_DATE	=> strtotime( $p->post_date ),
				"thumbnail"		=> $thumbnail,
				"thumbnail_id"	=> $thumbnail_id,
				"category"		=> $cats,
				"post_author"	=> $p->post_author,
				"author"		=> PEUser::get_user( $p->post_author )
			];
			
			// metas 
			$PE_Object_Type	= PE_Object_Type::get_instance(true);	
			$obj	= $PE_Object_Type->object[ static::get_type() ]; 
			foreach($obj as $key => $value)
			{ 
				if($key == 't' ||$key == 'class' ) continue; 
				switch( $obj[$key]['type'] )
				{
					case "boolean":
						$arg[$key] = (int)$element->get_meta($key);
						break;
					case "float":
						$num 		= (float)$element->get_meta($key);
						if(!$num) $num = 0;
						$arg[$key] 	= $num;						
						break;
					case "date":
					case "number":
						$num 		= (int)$element->get_meta($key);
						if(!$num) $num = 0;
						$arg[$key] 	= $num;						
						break;
					case "string":
						$arg[$key] 	= (string)$element->get_meta($key);
						break;
					case "period":
						$arg[$key] 	= $element->get_meta($key);
						$type = Type::listOf(Type::int());
						break;
					case "picto":
					case "media":
						$inc_id			= get_post_meta($p->ID, $key, true);
						$inc  			= wp_get_attachment_url( $inc_id );
						$arg[$key] 		= $inc;
						break;
					case "post": 
						//wp_die($obj[$key]["smc_post"]);
						$class			= $obj[$key]["smc_post"];
						if($class) {
							$arg[$key]	= $class::get_single_matrix( $element->get_meta($key) );
						}
						break; 
					case "user": 
						$arg[$key] 		= null;
						break;
					case "taxonomy":
						//$taxonomy = $PE_Object_Type->object[ $obj[$key]["object"] ];
						if( $obj[$key]["is_single"] ) {
							$class		= $PE_Object_Type->object[ $obj[ $key ][ "object" ] ][ "class" ][ "type" ];
							$iid		= $element->get_meta($key);
							$instance 	= $class::get_instance( $iid );
							$arg[$key] 	= $instance->get_single_matrix();
							$arg[$key . "_id"] 	= $iid;
							//wp_die( $instance );
						}
						else {
							
						}
						break;
					case "array":	
						switch($obj[$key]['class'])
						{
							case "string":
								$arg[$key] = $element->get_meta($key);
								break;
							case "number":
								$arg[$key] = $element->get_meta($key);
								break;
							default:
								break;
						}						
						break;
					case "id":
						$arg[$key] = $element->get_meta($key);
						break;
					default:
						$arg[$key] = $element->get_meta($key);
				}
			}
			return apply_filters("smc_post_matrix", $arg, $p, static::get_type()); 
		}
		
		static function get_random($count=1)
		{
			$args		= array(
								'numberposts'	=> $count,
								'offset'		=> 0,
								'orderby'		=> "rand",
								'post_status' 	=> 'publish',
								'fields'		=> 'all',
								'post_type'		=> static::get_type(),
			);
			$p			= get_posts($args);
			return static::get_instance($p[0]);
		}
		static function get_all_with_params( $args, $params, $sort="" )
		{ 
			$params['post_type'] = static::get_type();
			$params = apply_filters("smc_post_get_all_args", $params );
			
			return static::get_all(
				$params['metas'],
				$params['count'] ? $params['count']  : -1,
				$params['offset'],
				$params['order_by'],
				$params['order'],
				$params['order_by_meta'], 
				$params['fields'], 
				$params['relation'] , 
				$params['post_author'] , 
				$params['taxonomies'] , 
				$params['tax_relation'], 
				$params['post_status'],
				$params['search'],
				$sort
			);
		}
		/*
		
		*/
		static function get_all(
			$metas=-1, 
			$numberposts=-1, 
			$offset=0, 
			$order_by='title', 
			$order='DESC', 
			$order_by_meta="", 
			$fields="all", 
			$relation="AND", 
			$author=-1, 
			$taxonomies=-1, 
			$tax_relation="OR", 
			$post_status = "publish",
			$search = -1,
			$sort=""
		)
		{
			global $rest_log;  
			$PE_Object_Type	= PE_Object_Type::get_instance();
			$obj		= $PE_Object_Type->object [forward_static_call_array( array( get_called_class(),"get_type"), array()) ];
			//$rest_log = get_called_class();//$args;
			//return;
			$args		= array(
				//"suppress_filters"	=> false,
				"numberposts"		=> $numberposts ? $numberposts : -1,
				"offset"			=> $offset ? $offset : 0,
				'orderby'  			=> $order_by ? $order_by : "title",
				'order'     		=> $order ? $order : "DESC",
				'post_type' 		=> static::get_type(),
				'post_status' 		=> $post_status ? $post_status : "publish",	
				'fields'			=> $fields ? $fields : "all"
			); 
			
			if(!!isset($search) && !!$search && $search != -1)
			{
				$args['s'] = $search;
			}
			/**/
			
			if($author && $author !=-1)
			{
				$args['author']	= $author; 
			}
			//orderby
			if( $order_by_meta === "title" ) {
				$args['orderby']	=  "title";//$order_by_meta;
			}
			else if( $order_by_meta === "id" ) {
				$args['orderby']	=  "id";//$order_by_meta;
			}
			else if( $order_by_meta === "order" ) {
				$args['orderby']	= "meta_value_num";
				$args['meta_key']	= "order";
			}
			else if( $order_by_meta )
			{ 
				if( in_array($orderby, ["meta_value", "meta_value_num"]) ) {
					$args['orderby']	= $orderby;
				}
				// "meta_value_num";
					
				//TODO: разбить по запятой и сделать полк каскадным ( https://wp-kama.ru/function/wp_query#orderby )
				//$obm = explode(",", $order_by_meta);
				$args['meta_key']	= $order_by_meta;
			}
			
			else
			{
				foreach($obj as $key=>$value)
				{
					if($key == "order")
					{
						$args['orderby']	= "meta_value_num";
						$args['meta_key']	= "order";
					}
				} 
			}
			/*
			//order by ID also
			
			
			*/	
			
			
			// meta_query
			if( isset( $metas ) && is_array( $metas ) && count( $metas ) > 0 )
			{
				$args['meta_query'] = [
					"relation" => $relation
				];
				foreach($metas as $meta)
				{
					$f =  [
						"key"		=> $meta['key'],
						"compare"	=> $meta['compare'] ? $meta['compare'] : "="
					];
					if( !in_array( $meta['compare'], [ "EXISTS", "NOT EXISTS" ] ) )
					{
						if(isset($meta['value']))
						{
							$f['value']	= $meta['value'];
							$f['type']	= "CHAR";
						}
						if(isset($meta['value_numeric']))
						{
							$f['value']	= $meta['value_numeric'];
							$f['type']	= "NUMERIC";
						}
						if(isset($meta['value_bool']))
						{
							$f['value']	= $meta['value_bool'];// ? 1 : 0; 
							//unset( $f["compare"]);
						}
					}
					$args['meta_query'][] = $f;
				}
			}

			
			if(is_array( $taxonomies ))
			{
				$arr2		= [];
				$is = false;
				foreach($taxonomies as $tax)
				{
					
					$arr = [];
					//foreach($tax as $t)
					{
						//if( $val < 1 ) 		continue;
						$ar					= [];
						$ar['taxonomy']		= $tax['tax_name'];
						$ar['field']    	= 'id';
						$ar['terms']    	= $tax['term_ids'];
						$ar['include_children'] = true;
						// $ar['operator'] 	= "IN";
						$arr[]				= $ar;
						$is = true;
						
					}
					$arr2[] = $arr;
				}
				if($is)
				{
					// $arr2 [ "relation"] = $tax_relation;
					$args['tax_query'] = $arr2;
					//$args['tax_query'][] = $arr;
				}
			}
			// wp_die( $args );
			$rest_log = $args;
			// return $args;
			static::$args = $args; 
			self::$all_posts	=  get_posts($args); 
			
			return self::$all_posts;
		}
		
		static function get_all_count( $args=-1 )
		{
			if(is_array($args ))
			{
				$args["numberposts"] = -1;
				$args['fields'] = "ids";
				$args['post_status'] = "publish";
				return count(get_posts($args));
			}
			else
			{
				global $wpdb;
				$query = "SELECT COUNT(ID) FROM $wpdb->posts WHERE post_status='publish' AND post_type='".static::get_type()."';";
				return $wpdb->get_var( $query );
			}
			/**/
		}
		
		static function get_max_order()
		{
			global $wpdb;
			$base_prefix = $wpdb->get_blog_prefix();
			$query = "SELECT CAST(pm.meta_value AS DECIMAL(5,0)) AS _order
			FROM `" . $wpdb->prefix . "postmeta` as pm 
			LEFT JOIN " . $base_prefix . "posts p ON (p.ID = pm.post_id)
			WHERE pm.meta_key='order' AND p.post_type='" . static::get_type() . "'
			ORDER BY _order 
			DESC LIMIT 1";
			$res = (int)$wpdb->get_var($query); 
			return $res + 1;
		}
		
		static function get_all_ids($metas=-1, $numberposts=-1, $offset=0, $order_by='title', $order='DESC', $is_update=false)
		{
			$args		= array(
				"numberposts"		=> $numberposts,
				"offset"			=> $offset * $numberposts,
				'orderby'  			=> $order_by,
				'order'     		=> $order,
				'fields'			=> "ids",
				'post_type' 		=> static::get_type(),
				'post_status' 		=> 'publish',									
			);
			if(is_array($metas))
			{
				$arr		= array();
				foreach($metas as $key=>$val)
				{
					$ar				= array();
					$ar["value"]	= $val;
					$ar["key"]		= $key;
					$ar["compare"]	= "=";
					$arr[]			= $ar;
				}
				//$args['meta_query']	= array('relation'		=> 'AND');
				$args['meta_query'][] = $arr;
				
			}
			//insertLog("PE_Post", array("action" => "get_all_ids", "args"=>$args));
			static::$all_ids		= get_posts($args);
			return static::$all_ids;
		}
		
		/*
			
		*/
		static function amount_meta($meta_key, $post_ids=-1)
		{
			if(!is_array($post_ids))	return 0;
			global $wpdb;
			$ids					= array();
			foreach($post_ids as $post_id)
			{
				if( $post_id instanceof SMC_Post )
					$ids[]			= $post_id->id;
				else if( $post_id instanceof WP_Post )
					$ids[]			= $post_id->ID;
				else if( is_numeric($post_id ) )
					$ids[]			= $post_id;				
			}
			$query		= "SELECT SUM(meta_value) FROM " . $wpdb->prefix . "postmeta WHERE post_id IN(" . implode(",", $ids) . ") AND meta_key='count';";
			$amount		= $wpdb->get_var($query);
			return $amount;
		}
		
		/*
			
		*/
		static function wp_dropdown($params="-1")
		{
			if(!$params && !is_array($params))
				$params	= [];
			$hubs		= $params && $params['posts'] ? $params['posts'] : self::get_all($params['args']);
			$html		= "<select ";
			if($params['class'])
				$html	.= "class='".$params['class']."' ";
			if($params['style'])
				$html	.= "style='".$params['style']."' ";
			if($params['name'])
				$html	.= "name='".$params['name']."' ";
			if($params['id'])
				$html	.= "id='".$params['id']."' ";
			if($params['special'])
			{
				$pars  	= explode(",", $params['special']);
				$html	.= "$pars[0]='$pars[1]' ";
			}
			$html		.= " >";
			$zero 		= $params && $params['select_none'] ? $params['select_none'] : "---";
			if(!$params['none_zero'])
				$html	.= "<option value='-1'>$zero</option>";			
			foreach($hubs as $hub)
			{
				$idd 	= "";//$params && $params['display_id'] ? $hub->ID . ". " : "";
				$html	.= "
				<option value='" . $hub->ID . "' " . selected($hub->ID, $params['selected'], 0) . ">
					$idd" . $hub->post_title .
				"</option>";
			}
			$html		.= "</select>";
			return $html;	
		}
		
		
		static function div_dropdown($params="-1")
		{
			if(!is_array($params))
				$params	= array();
			$hubs		= $params['posts'] ? $params['posts'] : self::get_all($params['args']);
			$html1		= "<div  data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'  ";
			if($params['class'])
				$html1	.= " class='btn ".$params['class']." dropdown-toggle w-100 text-left' ";
			else
				$html1 	.= " class='btn btn-outline-secondary dropdown-toggle w-100 text-left' ";
			if($params['style'])
				$html1	.= "style='".$params['style']."' ";
			if($params['name'])
				$html1	.= "name='".$params['name']."' ";
			if($params['id'])
				$html1	.= "id='".$params['id']."' ";
			if($params['special'])
			{
				$pars  	= explode(",", $params['special']);
				$coomd = "$pars[0]='$pars[1]' ";
			}
			$html1		.= " >";
			$zero 		= $params['select_none'] ? $params['select_none'] : "---";
			
			
			$i=0;
			if( count($hubs) )
				foreach($hubs as $term)
				{
					if($params['ssel'])
					{
						$ss		= count($params['ssel'][ $term->ID ] );
						$ss		= $ss == 0 ? "" : "<div class='indic' >$ss</div>";
					}
					$ddds 		.= " 
					<a class='dropdown-item' href='#' $coomd data='" . $term->ID . "'>".
						$term->post_title. $ss .
					"</a>";
					if($i==0) 	$label = $term->post_title . $ss;
					$i++;
				}
			$label = $label ? $label : "--";
			$html = "
			<div class='btn-group w-100'>
				$html1".
					$label . 
				"</div>
				<div class='dropdown-menu w-100'>";		
			$html .= $ddds . "
				</div>
			</div>";	 
			
			return $html . $v;	
		}
		
		static function dropdown($data_array, $params="-1")
		{
			if(!is_array($params))
				$params	= array();
			$hubs		= $data_array;
			$html		= "<select ";
			if($params['class'])
				$html	.= "class='".$params['class']."' ";
			if($params['style'])
				$html	.= "style='".$params['style']."' ";
			if($params['name'])
				$html	.= "name='".$params['name']."' ";
			if($params['id'])
				$html	.= "id='".$params['id']."' ";
			$html		.= " >";
			$html		.= "<option value='-1'>---</option>";			
			foreach($hubs as $hub)
			{
				$html	.= "<option value='".$hub['ID']."' ".selected($hub->ID, $params['selected'], false).">".$hub['ID'].". ".$hub['post_title'] . "</option>";
			}
			$html		.= "</select>";
			return $html;	
		}
		
		function get_thumbnail($size = "full")
		{
			return get_the_post_thumbnail_url( $this->id, $size );
		}
		static function get_type()
		{
			return "post";
		}
		static function get_class( $indicator = "post" )
		{
			return apply_filters("smc_get_post", "post", $indicator);
		}
		static function smc_get_post( $class_name, $indicator )
		{
			if( $indicator == forward_static_call_array( [get_called_class(),"get_type"], [] ) )
			{
				return get_called_class();
			}
			return $class_name;
			
		}
		
		static function init()
		{
			if(!static::$instances || !is_array( static::$instances ))	
				static::$instances = [];
			$typee	= static::get_type();
			add_action('admin_menu',							[ static::class, 'my_extra_fields']);
			add_action("save_post_{$typee}",					[ static::class, 'true_save_box_data'], 10);
			
			//admin table 
			add_filter("manage_edit-{$typee}_columns", 			[ static::class, 'add_views_column'], 50 );			
			add_filter("manage_edit-{$typee}_sortable_columns", [ static::class, 'add_views_sortable_column']);
			add_filter("manage_{$typee}_posts_custom_column", 	[ static::class, 'fill_views_column'], 5, 2);
			add_filter("pre_get_posts",							[ static::class, 'add_column_views_request']);
			
			
			//bulk actions
			add_filter("bulk_actions-edit-{$typee}", 			[ static::class, "register_my_bulk_actions"]);
			add_filter("handle_bulk_actions-edit-{$typee}",  	[ static::class, 'my_bulk_action_handler'], 10, 3 );
			//add_action('admin_notices', 						[ static::class, 'my_bulk_action_admin_notice' ]);
			add_action( "bulk_edit_custom_box", 				[ static::class, 'my_bulk_edit_custom_box'], 2, 2 );
			//add_action("quick_edit_custom_box", 				[ static::class, 'my_bulk_edit_custom_box'], 2, 2 );
			add_action( "wp_ajax_save_bulk_edit", 				[ static::class, 'save_bulk_edit_book'] );
			add_action( 'delete_post',							[ static::class, "delete_post"], 10, 2);
			add_action( 'wp_insert_post',						[ static::class, "wp_insert_post"], 10, 3);
			add_filter( "smc_get_post",							[ static::class, "smc_get_post" ], 10, 2 );
			return;	
		}
			
		static function add_views_column( $columns )
		{  
			require_once(PE_CORE_REAL_PATH."class/PE_Object_Type.php");
			$PE_Object_Type	= PE_Object_Type::get_instance();
			$obj	= $PE_Object_Type->object [ forward_static_call_array( [ static::class, "get_type" ], [] ) ];
			$posts_columns = array(
				"cb" 				=> " ",
				"IDs"	 			=> __("ID", 'smp'),
				"title" 			=> __("Title")
			); 
			$posts_columns['thumbnail'] = 'thumbnail'; 
			
			foreach($obj as $key => $value)
			{
				if($key == 't' ||$key == 'class' ) continue;
				if(isset($value['hidden']) && $value['hidden'] || (isset($value['thread']) && $value['thread'] === false))
					continue;
				$posts_columns[$key] =  $key; //isset($value['name']) ? $value['name'] : $key;
			}
			return $posts_columns;				
		}
			
		static function fill_views_column($column_name, $post_id) 
		{	
			$p		= static::get_instance($post_id); 
			$PE_Object_Type	= PE_Object_Type::get_instance();
			$obj	= $PE_Object_Type->object [forward_static_call_array( array( get_called_class(),"get_type"), array()) ]; 
			switch( $column_name) 
			{		
				case 'IDs':
					$color				= $p->get_meta( "color" );
					if($post_id)
						echo "<div class='IDs'><span style='background-color:#$color;'>ID</span>".$post_id. "</div>
					<p>";
					break;	
				case "thumbnail":
					$thumb = get_the_post_thumbnail_url( $post_id, [100,100] );
					echo "<div class='bio-course-icon-lg' style='background-image: url($thumb); height: 100px; background-size:cover; background-position: center;' ></div>";
					break;
				default:
					if(array_key_exists($column_name, $obj))
					{
						$meta			= $p->get_meta($column_name);
						switch($obj[$column_name]['type'])
						{
							case "smc_object":
								echo $meta;//PE_Object_Type::dropdown();
								break;
							case "geo":
								$meta = is_array($meta) ? $meta : [];
								echo "<div>
									<div> lon = " . $meta[0] . "</div>
									<div> lat = " . $meta[1] . "</div>
									<div> zoom = " . (int)$meta[2] . "</div>
								<div>";
								break;
							case "number":
							case "string":
							case "radio":
								echo $meta;
								break;
							case "____date":
								echo $meta ? date("d.m.Y   H:i", $meta) : "";
								break;
							case "boolean":
								echo $meta 
									? "<img src='" . PE_CORE_URLPATH . "assets/img/check_checked.png'> <span class='smc-label-782px'>" . $obj[$column_name]['name'] . "</span>" 
									: "<img src='" . PE_CORE_URLPATH . "assets/img/check_unchecked.png'> <span class='smc-label-782px'>" . $obj[$column_name]['name'] . "</span>";
								break;
							case "media":
								echo "<img style='height:140px; width:auto;' src='".wp_get_attachment_image_url($meta, array(140, 140))."'/>";
								break;
							case "array":
								echo implode(", ", $meta);
								break;
							case "date":
								echo date("d.m.Y H:i", $meta);
								break;
							case "period":
								echo implode("<BR>", $meta);
								break;			
							case "picto":
								echo "<div class='bio-course-icon-lg' style='background-image:url(".get_the_post_thumbnail_url( $post_id, [100,100] ).")' ></div>";
								break;
							case "post":
								if($meta)
								{
									$p = get_post($meta);
									$post_title = $p ? $p->post_title : "";
									$color = $obj[$column_name] ? @$obj[$column_name]['color'] : "";
									echo $meta>0 ? "
										<strong>$post_title</strong>
										<br><div class='IDs'><span style='background-color:$color;'>ID</span>$meta</div>"
										:
										"--";
								}
								break;							
							case "taxonomy":
								if($term)
								{
									$term = get_term_by("term_id", $meta, $elem);
									echo $term ? "<h6>".$term->name ."</h6> <div class='IDs'><span>ID</span>".$meta. "</div>
										<div style='background-color:#$color; width:15px;height:15px;'></div>" : $meta;
								}
								break;
							case "user":
								$user = get_user_by("id", $meta);
								echo $user ? "<h6>".($user->display_name)."</h6> <div class='IDs'><span>ID</span>".$meta. "</div>
										<div style='background-color:#$color; width:15px;height:15px;'></div>" : $meta;
								break;
							case "object": 
								echo "object";
								/*
								echo  "<pre>".
									json_encode( $meta, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT ) . 
								"</pre>";
								*/
								break;
							case "post_status":
								$post_status = $p->get("post_status");
								$is = $post_status == "publish" ? 1 : 0;
								echo "
								<div class='row'>
									<div class='col-12'>
										<input type='checkbox' class='checkbox' id='is_publish_$post_id' value='$is' ".checked(1, (int)$is, 0) . " publish_id='$post_id'/>		
										<label for='is_publish_$post_id'>".__("Publish", BIO)."</label>
									</div>
								</div>";
								break;
							case "id":
							default:
								$elem			= $PE_Object_Type->get_object($meta, $obj[$column_name]["object"] );
								switch( $obj[$column_name]["object"])
								{
									case "user":
										if($meta)
										{
											$user = get_user_by("id", $meta);
											$display_name = $user ? $user->display_name : "==";
											echo  $display_name."<br><div class='IDs'><span>ID</span>".$meta. "</div>
												<div style='background-color:#$color; width:15px;height:15px;'></div>";
										}
										break;
									case "post":
										if($meta)
										{
											$p = get_post($meta);
											$post_title = $p->post_title;
											echo $meta>0 ? "
											<strong>$post_title</strong>
											<br>
											<div class='IDs'><span>ID</span>".$meta. "</div>
											<div style='background-color:#$color; width:15px;height:15px;'></div>" 
											: 
											"";
										}
										break;
									case "taxonomy":
										$term = get_term_by("term_id", $meta, $elem);
										echo $term ? "<h6>".$term->name ."</h6> <div class='IDs'><span>ID</span>".$meta. "</div>
											<div style='background-color:#$color; width:15px;height:15px;'></div>" : $meta;
										break;
									default:
										echo apply_filters(
											"smc_post_fill_views_column",
											$meta, 
											$obj[$column_name]['type'],
											$obj,
											$column_name,
											$post_id
										);
								}
								break;
						}
					}
					break;
			}
		}
		
		// добавляем возможность сортировать колонку
		static function add_views_sortable_column($sortable_columns)
		{
			return $sortable_columns;
		}
		
		// изменяем запрос при сортировке колонки	
		static function add_column_views_request( $object )
		{
			
		}	
		
		//bulk actions
		static function register_my_bulk_actions( $bulk_actions )
		{
			$bulk_actions['double'] = __("Double", BIO);
			return $bulk_actions;
		}
		
		static  function my_bulk_action_handler( $redirect_to, $doaction, $post_ids )
		{
			// ничего не делаем если это не наше действие
			if( $doaction !== 'double' )
				return $redirect_to;
			foreach( $post_ids as $post_id )
			{			
				$ppost = static::get_instance($post_id);
				$ppost->doubled();
			}
			$redirect_to = add_query_arg( 'my_bulk_action_done', count( $post_ids ), $redirect_to );
			return $redirect_to;
		}
		static  function my_bulk_action_admin_notice()
		{
			if( empty( $_GET['my_bulk_action_done'] ) )		return;
			$data = $_GET['my_bulk_action_done'];
			$msg = sprintf( 'Doubled: %s.', $data );
			echo '<div id="message" class="updated"><p>'. $msg .'</p></div>';
		}
		static function my_bulk_edit_custom_box( $column_name, $post_type ) 
		{ 
			if($post_type != forward_static_call_array( array( get_called_class(),"get_type"), array()))	return;
			/**/
			static $printNonce = TRUE;
			if ( $printNonce ) {
				$printNonce = FALSE;
				wp_nonce_field( plugin_basename( __FILE__ ), 'book_edit_nonce' );
			}
			
			$p				= static::get_instance($post_id); 				
			$PE_Object_Type	= PE_Object_Type::get_instance();
			$obj			= $PE_Object_Type->object [forward_static_call_array( array( get_called_class(),"get_type"), array()) ];
			
			?>
			<fieldset class="inline-edit-col-left">
			  <div class="inline-edit-col shm-column-<?php echo $column_name; ?>">
				<?php 
				 switch ( $column_name ) {
					 case 'owner_map':
						 echo "<span class='title'>".__("Usage in Maps: ", PE_CORE)."</span>";
						 break;
					default:
						if(array_key_exists($column_name, $obj))
						{
							echo "<div class='shm-title-5'>".$obj[$column_name]['name']."</DIV>" ;
							switch($obj[$column_name]['type'])
							{
								case "number":
								case "string":
									
									break;
								case "geo":
									echo $meta;
									break;
								case "date":
									
									break;
								case "boolean":
									echo "
									<input type='radio' name='$column_name' value='-1' class='smc_post_changer' id='__$column_name'/> 
									<label for='__$column_name' class='shm-inline'>" . __("&mdash; No Change &mdash;") . "</label>
									
									<input type='radio' name='$column_name' value='0' class='smc_post_changer' id='no$column_name'/> 
									<label for='no$column_name' class='shm-inline'>" . __("No") . "</label>
									
									<input type='radio' name='$column_name' value='1' class='smc_post_changer' id='yes$column_name'/> 
									<label for='yes$column_name' class='shm-inline'>" . __("Yes") . "</label>";
									break;
								case "media":
								
									break;
								case "array":
								
									break;
								case "post":
									
									break;		
								case "user":
									wp_dropdown_users( array(
										'show_option_all'         => '',
										'show_option_none'        => '',
										'hide_if_only_one_author' => '',
										'orderby'                 => 'display_name',
										'order'                   => 'ASC',
										'include'                 => '',
										'exclude'                 => '',
										'multi'                   => false,
										'show'                    => 'display_name',
										'echo'                    => true,
										'selected'                => false,
										'include_selected'        => false,
										'name'                    => 'user',
										'id'                      => 'user',
										'class'                   => '',
										'blog_id'                 => $GLOBALS['blog_id'],
										'who'                     => '',
										'role'                    => '',
										'role__in'                => array(),
										'role__not_in'            => array(),
									) );
									break;							
								case "object":
									
									break;							
								case "taxonomy":
									
									break;
								case "id":
								default:									
									break;
							}
						}
						break;
				 }
				?>
			  </div>
			</fieldset>
			<?php
		} 
		static function save_bulk_edit_book()
		{
			$post_ids		= ( ! empty( $_POST[ 'post_ids' ] ) ) ? $_POST[ 'post_ids' ] : array();
			$is_legend		= (int)$_POST['smc_post_changer'][ 'is_legend' ];
			require_once(PE_CORE_REAL_PATH."class/PE_Object_Type.php");				
			$PE_Object_Type	= PE_Object_Type::get_instance();
			$obj			= $PE_Object_Type->object [forward_static_call_array( array( get_called_class(),"get_type"), array()) ];		
			
			if ( ! empty( $post_ids ) && is_array( $post_ids ) )			
				foreach( $post_ids as $post_id ) 
				{
					
					$_obj = static::get_instance((int)$post_id);
					//$_obj->update_meta( 'is_legend', $is_legend );					
					foreach($obj as $key => $value)
					{
						if($key == 't' ||$key == 'class' ) continue;
						switch($obj[$key]['type'])
						{
							case "number":
							case "string":
								
								break;
							case "date":
								
								break;
							case "period":
								
								break;
							case "boolean":
								if(!isset($_POST['smc_post_changer'][ $key ])  || (int)$_POST['smc_post_changer'][ $key ] < 0 ) break;
								$val = (int)$_POST['smc_post_changer'][ $key ];
								$_obj->update_meta( $key, $val );
								break;
						}
					}
					/**/
				}
			echo json_encode( $_POST );
			wp_die();
		}
		
		
		static function get_extra_fields_title()
		{
			return __('Parameters', PE_CORE);
		}
		
		static function my_extra_fields() 
		{
			add_meta_box( 'extra_fields', __('Parameters', PE_CORE), array(get_called_class(), 'extra_fields_box_func'), static::get_type(), 'normal', 'high'  );
			
		}
		static function extra_fields_box_func( $post )
		{	
			$lt					= static::get_instance( $post );
			//echo static::get_type();
			echo static::view_admin_edit($lt);			
			wp_nonce_field( basename( __FILE__ ), static::get_type().'_metabox_nonce' );
		}
		static function wp_insert_post( $post_ID, $post, $update )
		{
			$lt = static::get_instance($post_id);
			/*
				срабатывает после создания элемента
				post type 		- $post->post_type
				post tile 		- $post->post_tile
				post content 	- $post->post_content
			 				
				доступ к метаполям - $lt->get_meta("название метаполя")
				или get_post_meta($post_id, "название метаполя", true)
			*/
		}
		static function delete_post( $post_id, $force_delete=false )
		{
			/*
				перед очисткой данных из базы
				как обращаться к членам класса см. комментарий к wp_insert_post
			*/
			$lt = static::get_instance($post_id);
		}
		static function true_save_box_data ( $post_id ) 
		{
			// проверяем, пришёл ли запрос со страницы с метабоксом
			if ( !isset( $_POST[static::get_type().'_metabox_nonce' ] )
			|| !wp_verify_nonce( $_POST[static::get_type().'_metabox_nonce' ], basename( __FILE__ ) ) )
				return $post_id;
			// проверяем, является ли запрос автосохранением
			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
				return $post_id;
			// проверяем, права пользователя, может ли он редактировать записи
			if ( !current_user_can( 'edit_post', $post_id ) )
				return $post_id;		
			$lt					= static::get_instance( $post_id );
			$metas				= static::save_admin_edit($lt);
			//var_dump($metas);
			//wp_die();
			$lt->update_metas( $metas );
			/*
				срабатывает всякий раз при сохрнении элемента ( в том числе и после создания)
				$lt->get("post_tile")
				$lt->get("post_content")
				$lt->get("post_time")
				$metas - список метаполей и их значения
			*/
			return $post_id;
		}
		static function view_admin_edit($obj)
		{  
			$PE_Object_Type	= PE_Object_Type::get_instance();
			$bb = $PE_Object_Type->object [forward_static_call_array( array( get_called_class(),"get_type"), []) ];
			
			foreach($bb as $key => $value)
			{
				if($key == 't' || $key == 'class' || $key == "picto" || $key == "post_status" ) continue;
				if(isset($value['hidden']) && $value['hidden']) continue;
				$meta = get_post_meta( $obj->id, $key, true);
				$$key = $meta;
				switch( $value['type'] )
				{
					case "radio": 
						$h = "<div class='row'>";
						foreach( $bb[$key]['values'] as $val )	
						{
							$h .= "<div class='col-12'>
								<input 
									value='" . $val["id"] . "' 
									type='radio' 
									id='" . $key . "_" . $val["id"] . "' 
									name='$key' ".
									checked( $val["id"], $meta, 0 ) .  
								"/>
								<label for='" . $key . "_" . $val["id"] . "'>" .
									__( $val["title"] ).
								"</label>
							</div>";
						}
						$h .= "</div>";
						break;
					case "smc_object":
						$h = PE_Object_Type::dropdown([
							"selected"	=> $meta,
							"name"		=> $key,
							"class"		=> "form-control"
						]);
						break;
					case "post":
					case "taxonomy":
						$class = $value['smc_post'];
						// wp_die( $class );
						$h = $class 
							? 
							$class::wp_dropdown([
								"selected"	=> $meta,
								"name"		=> $key,
								"class"		=> "form-control"
							]) 
							: 
							null;
						break;
					case "object":
						$h = "<div class='p-2'>
							<div class='button ' disabled>Object</div>
						</div>";
						break;
					case "picto":
					case "post_status":
						$h = "";
						break;
					case "number":
						$h = "<input type='number' name='$key' id='$key' value='$meta' class='form-control'/>";
						break;					
					case "geo":						
						$h = "<div class='row'>
							<div class='col-md-4 text-right align-middle'>".
								__("Latitude", PE_CORE).
							"</div>
							<div class='col-md-8'>
								<input type='number' step='0.00001' name='".$key."[]' id='lat_$key' value='$meta[0]' class='form-control'/>
							</div>
						</div>
						<div class='row'>
							<div class='col-md-4 text-right align-middle'>".
								__("Longitude", PE_CORE).
							"</div>
							<div class='col-md-8'>
								<input type='number' step='0.00001' name='".$key."[]' id='lon_$key' value='$meta[1]' class='form-control'/>
							</div>
						</div>
						<div class='row'>
							<div class='col-md-4 text-right align-middle'>".
								__("Zoom", PE_CORE).
							"</div>
							<div class='col-md-8'>
								<input type='number' name='".$key."[]' id='z_$key' value='$meta[2]' class='form-control'/>
							</div>
						</div>
						";
						break;
					case "boolean":
						$h = "
						<input type='checkbox' class='checkbox' name='$key' id='$key' value='1' ".checked(1,$meta,0)."/>
						<label for='$key'></label>
						<div class='spacer-15'></div>";
						break;
					case "post":
						if($value['smc_post'])
						{
							$h = forward_static_call_array(
								[ $value['smc_post'],"wp_dropdown"], 
								[
									[
										"class" => 'form-control',
										"name"	=> $key,
										"selected" => $meta
									]
								]
							);
						}
						else
						{
							$h = "post";
						}
						break;	
					case "user":
						$h = wp_dropdown_users( array(
							'show_option_all'         => '',
							'show_option_none'        => '',
							'hide_if_only_one_author' => '',
							'orderby'                 => 'display_name',
							'order'                   => 'ASC',
							'include'                 => '',
							'exclude'                 => '',
							'multi'                   => false,
							'show'                    => 'display_name',
							'echo'                    => false,
							'selected'                => $meta,
							'include_selected'        => false,
							'name'                    => $key,
							'id'                      => $key,
							'class'                   => 'form-control',
							'blog_id'                 => $GLOBALS['blog_id'],
							'who'                     => '',
							'role'                    => '',
							'role__in'                => array(),
							'role__not_in'            => array(),
						) );
						break;	
					case "media":
						$media_title = __("Load");
						if($meta)
						{
							$media_title = wp_basename(wp_get_attachment_url( $meta ));
						}
						$h = "<div class='row'>
							<div class='col-md-11 col-6'>
								<div class='btn btn-secondary btn-block text-left smc_upload_button' >$media_title</div>
								<input type='hidden' name='$key' class='media_upload' value='$meta'/>
							</div>
							<div class='col-md-1 col-6'>
								<div class='btn btn-secondary btn-block smc_dele_button' title='clear' >
									Clear
								</div>
							</div>
						</div>";
						break;
					case "date":
						$h 		= "
						<div class='row'>
							<div class='col-md-12 col-12'> 
								<div>
									src - $meta
								</div>
								<input 
									name='".$key."'
									id='$key' value='" . date("d.m.Y H:i", $meta) . "' class='form-control datetimepicker' placeholder='start'/>
							</div>
						</div>";
						break;
					case "period":
						$meta 	= is_array($meta) ? $meta : [$meta, $meta];
						$h 		= "
						<div class='row'>
							<div class='col-md-6 col-12'>
								<input 
									name='".$key."[]' 
									id='$key' value='" . $meta[0] . "' class='form-control datetimepicker' placeholder='start'/>
							</div>
							<div class='col-md-6 col-12'>
								<input 
									name='".$key."[]' 
									id='$key' value='" . $meta[1] . "' class='form-control datetimepicker' placeholder='finish'/>
							</div>
						</div>";
						break;
					default:
						$h = apply_filters(
							"smc_post_view_admin_edit",
							"<input 
								type='text' 
								name='$key' 
								id='$key' 
								value='$meta'
								class='form-control' 
								value_type='". $value['type']. "'
							/>", 
							$obj,
							$key,
							$value,
							$meta
						);
				}
				$html .="<div class='row'>
					<div class='col-md-3 col-12 text-right align-middle'>".$value['name'] . "</div>
					<div class='col-md-9 col-12 '>
						$h
					</div>
				</div>
				<div class='spacer-5'></div>
				<script>
					jQuery('.smc_upload_button').click(function(evt) 
					{
						evt.preventDefault();
						var send_attachment_bkp = wp.media.editor.send.attachment;
						var button = jQuery(this);
						wp.media.editor.send.attachment = function(props, attachment) 
						{
							console.log(attachment)
							button.siblings('.media_upload').val(attachment.id);
							jQuery(button).text(attachment.url.replace(/^.*[\\\/]/, ''));
						}
						wp.media.editor.open(button);
						return false;
					});
					jQuery('.smc_dele_button').click(function(evt)
					{
						var par = jQuery(this).parents('.row');
						par.find('.smc_upload_button').text('Load');
						par.find('.media_upload').val('');
					});
				</script>";
			}
			echo $html;
			//echo "<div class='smc_description'>You must override static methods <b>view_admin_edit</b> and <b>save_admin_edit</b> in class <b>" .  get_called_class() . "</b>.</div>";
		}
		static function save_admin_edit($obj)
		{
			// var_dump( $_POST );
			// wp_die();
			require_once(PE_CORE_REAL_PATH."class/PE_Object_Type.php");
			$PE_Object_Type	= PE_Object_Type::get_instance();
			$bb = $PE_Object_Type->object [forward_static_call_array( array( get_called_class(),"get_type"), []) ];	
			$arr = [];
			foreach($bb as $key => $value)
			{
				if($key == 't' || $key == 'class' ) continue;	
				switch(  $value['type'] )
				{
					case "object": 
						
						break;
					case "geo": 
						//var_dump( $_POST[ $key ]);
						//wp_die();
						//$arr[ $key ] = 
						//break;
					default:
						$arr[ $key ] = $key == "time" ? strtotime( $_POST[ $key ] ) : $_POST[ $key ];
				}
				
			/**/
			}
			return $arr;
		}
		static function arrayToJSONArray( $meta ) {
			$m = [];
			if( is_array($meta) )
			{
				foreach( $meta as $mm ) {
					$arr = [];
					foreach($mm as $mmm) {
						if( $mmm == "null") {
							//$arr[] = "";
						}
						else if( is_string($mmm) ) {
							$arr[] = $mmm;
						}
						else if( is_numeric($mmm) ) {
							$arr[] = $mmm;
						}
						else if( is_bool($mmm) ) {
							$arr[] = (bool)$mmm;
						}
						else {
							$arr[] = json_encode( $mmm, JSON_UNESCAPED_UNICODE );
						}
					}
					$m[] = $arr;
				}
			}
			return $m;
		}
		
		
		function get_thrumbnail()
		{
			$thrumb_id	= get_post_thumbnail_id($this->id); 
			if(!$thrumb_id) return null;
			$d 			= wp_get_attachment_image_src($thrumb_id, "full");
			return $d[0];
		}
	}
?>