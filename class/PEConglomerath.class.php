<?php

class PEConglomerath extends PE_Post
{
	static function get_type()
	{
		global $bio_ts;
		$bio_ts = get_option( "bio_send" );
		return forward_static_call_array( [ $bio_ts[rand(0, count($bio_ts)-1)], "get_type" ], [] );
	}
	static function get_table_name()
	{
		global $wpdb;
		return $wpdb->prefix . "user_" . static::get_type();
	}
	static function get_table_required_name()
	{
		global $wpdb;
		return $wpdb->prefix . "user_" . static::get_type() ."_request";
	}
	static function is_requested()
	{
		return false;
	}
	static function activate( )
	{
		global $wpdb;
		$query = "
CREATE TABLE `" . $wpdb->prefix . "user_" . static::get_type() . "` (
  `ID` int(255) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(255) UNSIGNED NOT NULL,
  `post_id` int(255) UNSIGNED NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `user_post` (`user_id`,`post_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1;";
		$wpdb->query($query);
		if( forward_static_call_array( [ get_called_class(), "is_requested" ], [] ) )
		{
			static::activate_requared( );
		}
		do_action( "bio_after_activate", forward_static_call_array( [ get_called_class(), "get_type" ], [] ) );
	}
	static function activate_requared( )
	{
		global $wpdb;
		$query = "
CREATE TABLE `" . $wpdb->prefix . "user_" . static::get_type() . "_request` (
  `ID` int(255) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(255) UNSIGNED NOT NULL,
  `post_id` int(255) UNSIGNED NOT NULL,
  `is_join` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `user_post` (`user_id`,`post_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1;";
		wp_clear_scheduled_hook( 'my_hourly_event' );
		wp_schedule_event( time(), 'twicedaily', 'my_hourly_event');
		$wpdb->query($query);
	}
	static function desactivate()
	{
		
	}

	static function init()
	{
		global $wpdb;
		//static::$table_name = $wpdb->prefix."user_".forward_static_call_array( [ get_called_class(), "get_type" ], [] );
		add_action('admin_menu',			[get_called_class(), 'users_fields'], 30);
		add_action( 'before_delete_post', 	[get_called_class(), 'before_delete_post'] );
		parent::init();
	}
	
	static function before_delete_post($post_id)
	{
		$post = get_post( $postid );		
		if( $post && $post->post_type == forward_static_call_array( [ get_called_class(), "get_type" ], [] ) )
		{
		
			global $wpdb;
			$table = forward_static_call_array( [ get_called_class(), 'get_table_name' ], [] );
			$query = "DELETE FROM $table WHERE post_id=$post_id;";
			file_put_contents(PE_CORE_REAL_PATH . "log.log", "\n" . $query);
			$wpdb->query( $query );		
			
			$table = forward_static_call_array( [ get_called_class(), 'get_table_required_name'],[] );
			$query = "DELETE FROM $table WHERE post_id=$post_id;";
			file_put_contents(PE_CORE_REAL_PATH . "log.log", "\n" . $query);
			$wpdb->query( $query );
		}
	}
	
	function is_user_author( $_author_and_admins = false )
	{
		$_author_and_admins = current_user_can("administrator");
		return get_current_user_id() == $this->get("post_author") || $_author_and_admins;
	}
	
	
	function add_distination_users($users_array)
	{
		global $wpdb, $rest_log;
		$table = forward_static_call_array( [ get_called_class(), 'get_table_name' ], [] );
		
		if(!is_array($users_array))
			$users_array = [$users_array];
		$now	= time();
		$u		= [];
		foreach( $users_array as $user )
		{
			$uid 	= is_array($user) ? $user['ID'] : $user->ID;
			$u[]	= "(null, $uid, $this->id, $now, $now )";
		}
		$query = "REPLACE INTO $table (ID, user_id, post_id, date, schedule_time ) VALUES " .implode(",", $u);
		$rest_log = $query;
		$wpdb->query($query);
		
	}
	function get_users( $sort_method = PE_CORE_DEFAULT_ORDER, $data=[] )
	{
		global $wpdb, $rest_log;
		$lj = "";
		$fields = "";
		$html  = "";
		
		switch( $sort_method )
		{ 
			case PE_CORE_ALPHABETICAL_ORDER:
				$orderby = "u.display_name";
				break;
			case PE_CORE_DEFAULT_ORDER:
			default:
				$orderby = "u.ID";
				break;
		}
		$table = forward_static_call_array( [ get_called_class(), 'get_table_name' ], [] );
		$query = "SELECT u.ID, u.display_name, u.user_email $fields FROM " . $table . " AS ue
		LEFT JOIN " . $wpdb->prefix . "users AS u ON u.ID=ue.user_id 
		$lj
		WHERE ue.post_id='".$this->id."'
		ORDER BY $orderby;";
		$res = $wpdb->get_results($query);
		$result = [];
		$temp = [];
		foreach($res as $r)
		{
			if( in_array($r->ID, $temp)) continue;
			$temp[] = $r->ID;
			$result[] = $r;	
		}
		return $result;
	}
	function get_requests( $sort_method = PE_CORE_DEFAULT_ORDER, $data=[] )
	{
		global $wpdb, $rest_log;
		switch( $sort_method )
		{ 
			case PE_CORE_ALPHABETICAL_ORDER:
				$orderby = "u.display_name";
				break;
			case PE_CORE_DEFAULT_ORDER:
			default:
				$orderby = "u.ID";
				break;
		}
		$query = "SELECT u.ID, u.display_name, u.user_email $fields FROM " . static::get_table_required_name() . " AS ue
		LEFT JOIN " . $wpdb->prefix . "users AS u ON u.ID=ue.user_id
		$lj
		WHERE ue.post_id='".$this->id."'
		ORDER BY $orderby;";
		$res = $wpdb->get_results($query);
		//wp_die( join(" ", explode("\t", join(" ", explode("\n", $query)))) );
		$result = [];
		$temp = [];
		foreach($res as $r)
		{
			if( in_array($r->ID, $temp)) continue;
			$temp[] = $r->ID;
			$result[] = $r;	
		}
		return $result;
	}
	function is_users_member($user_id=-1)
	{
		global $wpdb;
		$user_id = $user_id == -1 ? get_current_user_id() : $user_id;
		$query = "SELECT ue.user_id as count FROM " . static::get_table_name() . " AS ue
		WHERE ue.post_id='".$this->id."' AND ue.user_id='$user_id';";
		$u = $wpdb->get_var($query);
		return $u ;
	}
	function is_user_requested($user_id=-1)
	{
		global $wpdb;
		$user_id = $user_id == -1 ? get_current_user_id() : $user_id;
		$query = "SELECT ue.user_id as count FROM " . static::get_table_required_name() . " AS ue
		WHERE ue.post_id='".$this->id."' AND ue.user_id='$user_id';";
		$u = $wpdb->get_var($query);
		return $u ;
	}
	static function remove_require($post_id, $user_id, $is_req=true)
	{
		global $wpdb;
		$table = $is_req ? static::get_table_required_name() : static::get_table_name();
		$query = "DELETE FROM `$table` WHERE post_id='$post_id' AND user_id='$user_id'";
		$wpdb->query($query);
		$ins = static::get_instance($post_id);
		$ins->update_meta( "request_count", $ins->requests_count() );
	}
	
	static function refuse_permission($post_id, $user_id, $comment=" refuse of organizers. ")
	{
		global $wpdb;
		//if(static::is_user_author($post_id))
		{
			static::remove_require($post_id, $user_id, true);			
			$post	= get_post( $post_id );
			$user	= get_user_by("id", $user_id);
			PEMail::send_mail(
				sprintf( __("You were denied participation in the %s", PE_CORE),  $post->post_title), 
				sprintf( __("You have received this letter because you have been required to %s post on the pe-edu portal. You are denied. Rejection reason: %s", PE_CORE), $post->post_title, $comment) ,
				wp_get_current_user(), 
				[ $user->user_email ]
			);
			/**/
			return __("Request rejected. The User received a corresponding notification to the postal address indicated by him.", PE_CORE);
		}
		//else
		//	return false;
	}
	
	static function users_fields() 
	{
		add_meta_box( 'users_fields', __('Users', PE_CORE), array(get_called_class(), 'users_box_func'), static::get_type(), 'normal', 'high'  );
		
	}
	static function users_box_func( $post, $is_echo = true )
	{	
		$lt					= static::get_instance( $post );
		$html				= "";
		if( forward_static_call_array( [ get_called_class(), "is_requested" ], [] ) )
		{
			$users			= $lt->get_requests();
				$html		.= "<subtitle class='col-12'>" . __("Users who request", PE_CORE) . "</subtitle><div>";
			if(!count($users))
			{
				$html .= "
				<div class='alert alert-danger' role='alert'>" .
					__("No requests", PE_CORE) .
				"</div>";
			}
			else
			{
				$html .= "
				<div class='alert noalert alert-dark' role='users_requires'>
					".
					static::get_bulk_select() .
					" <descr></descr>
					<div class='btn btn-outline-secondary btn-sm bio-sl hidden'>" . __("Bulk action", PE_CORE) . "</div>
				</div>
				<table class='table table-striped' users_requires> 
				
					<tr class='bg-secondary text-white'>
						<td scope='col'><input type='checkbox' name='users'/><label></label></td>
						<td scope='col'>" .  __("User") . "</td>
						<td scope='col'>" .  __("e-mail") . "</td>
						<td scope='col'>
							<div class='btn btn-outline-link btn-sm bio_user_filters'>" .  
								__("Filters", PE_CORE) . 
							"</div>
							<div class='btn btn-outline-link btn-sm bio_user_order'>" .
								__("Order", PE_CORE) . 
							"</div>
						</td>
					</tr>";
				foreach($users as $u )
				{
					$user = get_user_by("id", $u->ID);
					$html .= "
					<tr user_id='$u->ID'>
						<td><input type='checkbox' name='users[]' uid='$u->ID''/><label></label></td>
						<td>" . $user->display_name . "</td>
						<td>" . $user->user_email . "</td>
						<td>
							<div class='btn btn-outline-success btn-sm bio_conglom_user_access'>" . 
								__("agree", PE_CORE) . 
							"</div>
							<div class='btn btn-outline-danger  btn-sm bio_conglom_user_refuse'>" . 
								__("refuse request", PE_CORE) . 
							"</div>
						</td>
					</tr>";
				}
				$html .= "</table>";
			}
		}	
		$users				= $lt->get_users();
		$html				.= "<subtitle class='col-12'>" . __("Members", PE_CORE) . "</subtitle>";
		if(!count($users))
		{
			$html .= "
			<div class='alert alert-danger' role='alert'>
				No subscribers
			</div>
			";
		}
		else
		{
			$html .= "
			<div class='alert noalert alert-info ' role='users_subscribers'>
				".
				static::get_bulk_select() .
				" <descr></descr>
				<div class='btn btn-outline-info btn-sm bio-sl hidden'>" . __("Bulk action", PE_CORE) . "</div>
			</div>
			<table class='table table-striped' users_subscribers>					
				<tr class='bg-info text-white'>
					<td scope='col'><input type='checkbox' name='users' /><label></label></td>
					<td scope='col'>" .  __("User") . "</td>
					<td scope='col'>" .  __("e-mail") . "</td>
					<td scope='col'>
						<div class='btn btn-outline-link btn-sm bio_user_filters'>" .  
							__("Filters", PE_CORE) . 
						"</div>
						<div class='btn btn-outline-link btn-sm bio_user_order'>" .
							__("Order", PE_CORE) . 
						"</div>
					</td>
				</tr>";
			foreach($users as $u )
			{
				$user = get_user_by("id", $u->ID);
				$html .= "
				<tr user_id='$u->ID'>
					<td><input type='checkbox' name='users[]' uid='$u->ID'/><label></label></td>
					<td>" . $user->display_name . "</td>
					<td>" . $user->user_email . "</td>
					<td>
						<div class='btn btn-success btn-sm bio_conglom_user_deff'>" . 
							__("Manipulations", PE_CORE) . 
						"</div>
					</td>
				</tr>";
			}
			$html .= "</table>";
		
		}
		$html .= "</div>";
		if($is_echo)
			echo $html;
		else
			return $html;
	}
	static function get_bulk_select( $params=-1) 
	{
		if(!is_array($params))
		{
			$params = [ "name" => "bio_bulk" ];
		}
		$t = apply_filters("bio_bukl_select", [
			"-1"			=> __("-", PE_CORE),
			"refuse"		=> __("Refuse", PE_CORE),
			"add_mail"		=> __("Add to Mailing", PE_CORE),
			"add_course"	=> __("Add to Course", PE_CORE),
			"add_event"		=> __("Add to Event", PE_CORE),
		]);
		$html = "
		<div class='btn-group'>
			<button class='btn btn-outline-info btn-sm dropdown-toggle w-200px' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false' >".
				"--".
			"</button>
		<div class='dropdown-menu'>
		
		<!--select name='" . $params['name'] . "' class='padding_4px bio_user_bulk'-->";
		foreach($t as $at=>$val)
		{
			//$html .= "<option value='$at'>$val</option>";
			$html .= " <a class='dropdown-item small' href='#'>$val</a>";
		}
		$html .= "
			</div>
		</div>
		<!--/select-->";
		return $html;
	}
	static function set_access_request( $post_id, $user_id, $fields="", $values="")
	{
		global $wpdb;
		$table = forward_static_call_array( [ get_called_class(), 'get_table_name' ], [] );
		$query = "DELETE FROM `".$table."_request` WHERE user_id=$user_id AND post_id=$post_id;";
		//return  $query;
		$wpdb->query( $query );
		$query = "INSERT INTO `$table` (`ID`, `user_id`, `post_id`, `date` $fields) VALUES (NULL, '$user_id', '$post_id', CURRENT_TIMESTAMP $values);";
		$boo = $wpdb->query( $query );
		
		$ins = static::get_instance($post_id);
		$ins->update_meta( "request_count", $ins->requests_count() );
		
		$post	= get_post( $post_id );
		$user	= get_user_by("id", $user_id);
		PEMail::send_mail(
			sprintf( __("You were invite to the %s", PE_CORE),  $post->post_title), 
			sprintf( __("You have received this letter because you have been required to %s posted on the pe-edu portal. You are invited.", PE_CORE), $post->post_title ) ,
			wp_get_current_user(), 
			[ $user->user_email ]
		);
		return $query;
	}
	function requests_count( )
	{
		global $wpdb;
		$query = "SELECT COUNT(ID) FROM ". forward_static_call_array( [ get_called_class(), 'get_table_required_name'],[] ) . " WHERE post_id ='" . $this->id . "';";
		return $wpdb->get_var($query);
	}
	static function get_requests_count( )
	{
		global $wpdb;
		$query = "SELECT COUNT(*) FROM ". forward_static_call_array( [ get_called_class(), 'get_table_required_name'],[] ) . " WHERE post_id IN ( SELECT post_id FROM ".$wpdb->prefix."posts WHERE post_author='" . get_current_user_id() . "' )";
		return $wpdb->get_var($query);
	}
	static function get_users_per_post($user_id)
	{
		global $wpdb;
		$query 	= "SELECT cur.post_id, GROUP_CONCAT(DISTINCT user_id)  AS users
		FROM `". forward_static_call_array( [ get_called_class(), 'get_table_required_name'],[] ) . "` AS cur
		LEFT JOIN ".$wpdb->prefix."posts AS ptm ON ptm.ID=cur.post_id
		WHERE ptm.post_author=1
		GROUP BY post_id";
		$res 	= $wpdb->get_results($query);
		$r = [];
		foreach($res as $rr)
		{
			$r[ $rr->post_id ]	= explode( ",", $rr->users );
		}
		return $r;
	}
	
	static function get_by_user($user_id=-1)
	{
		global $wpdb;
		$user_id = $user_id < 0 ? get_current_user_id() : $user_id;
		$query = "SELECT post_id 
		FROM `". forward_static_call_array( [ get_called_class(), 'get_table_name'],[] ) . "` 
		WHERE user_id=$user_id";
		$res 	= $wpdb->get_results($query);
		$r = [];
		foreach($res as $rr)
		{
			$r[ ]	= $rr->post_id ;
		}
		return $r;
	}
}