<?php

  
use GraphQL\Error\ClientAware;
use GraphQL\Utils\BuildSchema;
use GraphQL\Utils\SchemaPrinter;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ScalarType;

require_once( PE_CORE_REAL_PATH."class/graphql/media.php" );
GQLMedia::init();

class PECoreGraphQL
{
	static function getType ($type) {
		switch($type) {
			case "ID":
				return Type::id();
			default:
				return Type::string();
		}
	}
	static function init()
	{ 
		add_action("pe_graphql_make_schema", [__CLASS__, "auth"], 9);
		add_action("pe_graphql_make_schema", [__CLASS__, "graphql_register_input_types"], 10);
		add_action("pe_graphql_make_schema", [__CLASS__, "exec_init"], 10); 
		add_action("pe_graphql_make_schema", [__CLASS__, "exec_options"], 10);		
		add_action("pe_graphql_make_schema", [__CLASS__, "exec_comments"], 7); 
		add_action("pe_graphql_make_schema", [__CLASS__, "register_smc_posts"], 60);
		add_action("pe_graphql_make_schema", [__CLASS__, "pe_graphql_make_schema"], 99); 
		add_action("pe_graphql_make_schema", [__CLASS__, "mailme"], 90.12); 
		add_action("pe_graphql_make_schema", [__CLASS__, "logs"], 90.167); 
		add_filter( 'send_password_change_email', '__return_false' ); 
		//wp_set_password("zaq12ws", 1);
	}
	/*******************************************
		Получает ленту элементов определенного типа данных
		Возвращает массив GQL-объектов
		$args: 			данные для создания/изменения элемента (именованный массив)
		$class_name: 	GQL-класс типа данных (строка)
		$post_type: 	сторовое имя типа данных (строка)
		$val: 			данные матрицы типа данных (именованный массив. Берётся из PE_Object_Type по ключу $post_type)
	********************************************/
	static function get_feed( $args, $class_name, $post_type, $val ) {
		do_action(
			"before_gql_group_resolve",
			[ 
				"args" 			=> $args, 
				"class_name"	=> $class_name, 
				"post_type"		=> $post_type, 
				"value"			=> $val 
			]
		);
		if( $args["land_id"] ) { 
			switch_to_blog( (int)$args["land_id"] );
		}
		$matrix = [];
		if( $val['t']['type'] == "post" )
		{
			if ( in_array( $class_name, ['Post', "Page"] ) )
			{
				$class_name = "PE_" . $class_name;
			}
			$taxonomies = [];
			if ( isset( $args["paging"]['taxonomies'] ) )
			{		
				foreach( $args[ "paging" ][ 'taxonomies' ] as $tax => $val )
				{
					//tax_name, term_ids
					if(!isset( $taxonomies[$val['tax_name']]) )
					{
						$taxonomies[$val['tax_name']] = [];										
					}
					$taxonomies[$val['tax_name']] =  array_merge($taxonomies[$val['tax_name']], $val['term_ids']);
				}
			}
			
			$search = $args["paging"]["search"];
			if( !!isset($search) && !!$search && $search != -1 ) {
				
			}
			$matrixes = $class_name::get_all_matrixes( $args["paging"] );  
		}
		else if( $val['t']['type'] == "taxonomy" )
		{ 
			$matrixes = $class_name::get_all_matrixes($args["paging"] );
			// wp_die( $matrixes ); 
		}
		
		do_action(
			"after_gql_group_resolve",
			[ 
				"args" 			=> $args, 
				"class_name"	=> $class_name, 
				"post_type"		=> $post_type, 
				"value"			=> $val 
			]
		);
		/**/
		return $matrixes;
	}
	
	/***********************************************
		Создаёт/редактирует единичный элемент указанного типа данных
		Возвращает GQL-объект
		$args: 			данные для создания/изменения элемента (именованный массив)
		$class_name: 	GQL-класс типа данных (строка)
		$post_type: 	сторовое имя типа данных (строка)
		$val: 			данные матрицы типа данных (именованный массив. Берётся из PE_Object_Type по ключу $post_type)
	************************************************/
	static function changePEPost($args, $class_name, $post_type, $val) {
		
		// wp_die( [$args, $class_name, $post_type, $val] );
		
		do_action(
			"before_gql_change_single_resolve",
			[ 
				"args" 			=> $args, 
				"class_name"	=> $class_name, 
				"post_type"		=> $post_type, 
				"value"			=> $val 
			]
		); 
		if( $args["land_id"] ) {
			switch_to_blog( $args["land_id"] );
		}
		$matrix = [];
		// wp_die( $args['id'] && !$args['id'] );
		if ( !isset($args["input"][PE_POST_TITLE]) && !$args['id'] )
		{
			$args["input"][PE_POST_TITLE] = "--";
		}
		if ( !isset($args["input"][PE_POST_CONTENT]) && !$args['id'] )
		{
			$args["input"][PE_POST_CONTENT] = "--";
		}
		$return = apply_filters(
			"before_change_gq_post", 
			null,
			$root, $args, $context, $info, $class_name, $post_type, $val
		);
		if(isset($return))
			$matrix = $return;
		
		 
		if($val['t']['type'] == "post")
		{				
			if(in_array($class_name, ['Post', "Page"]))
			{
				
				if(!current_user_can("edit_pages"))
				{
					PECore::addLog( $args, [], "not_can_change_post");
					throw new \PEGraphQLNotAccess( "you not right!" );
				}
				$post_args = $args['input'];
				if( $args['id'] && $args['id'] > 0 )
				{
					// wp_die( $args['id'] . " == id");
					$post_args["ID"] = $args['id'];
					wp_update_post([
						"ID" => $args['id'],
						PE_POST_TITLE 	=> $args['input'][PE_POST_TITLE],
						PE_POST_CONTENT =>$args['input'][PE_POST_CONTENT],
					]);
					$matrix = PE_Post::get_single_matrix($args['id']);
				}
				else
				{
					$post_args[PE_POST_AUTHOR] = get_current_user_id();
					$post_args[PE_POST_TYPE] = strtolower($class_name);
					$post_args[PE_POST_STATUS]   = $args['input'][PE_POST_STATUS]
							? 
							$args['input'][PE_POST_STATUS]
							: 
							"publish";
					$post_args[PE_POST_CONTENT]   = isset($args['input'][PE_POST_CONTENT])
							? 
							$args['input'][PE_POST_CONTENT]
							: 
							"--";
					$post_args[PE_POST_TITLE]   = isset($args['input'][PE_POST_TITLE])
							? 
							$args['input'][PE_POST_TITLE]
							: 
							"--";	
					//wp_die($args['input'][PE_POST_TITLE]);
					$id = wp_insert_post($post_args);
					//wp_die($id);
					if(!is_wp_error($id))
					{
						$matrix = PE_Post::get_single_matrix($id);
					}
					else
					{
						wp_die($id);
					}
				}
			}
			
			// TODO - проверить права на изменения
			// $const = constant( strtoupper( $post_type ) . "_EDIT" );
			
			// PEUser::access_caps_gq( $const, "you not right!" );
			
			
			if( $args['id'] && $args['id'] > 0 )
			{
				$art = $class_name::update( $args['input'], $args['id'] ); 
				$class_name::clear_instance($args['id']);
				 
				$matrix = apply_filters(
					"bio_gq_change_" . $class_name, 
					$class_name::get_single_matrix( $args['id'] ),
					$art,
					$args
				);
			}
			else
			{
				//wp_die( $class_name::get_type() );
				$art = $class_name::insert($args['input']);
				 
				// wp_die($class_name::get_single_matrix($art->id));
				// wp_die( $art );
				$matrix = apply_filters(
					"bio_gq_change_" . $class_name, 
					$class_name::get_single_matrix($art->id),
					$art,
					$args
				);
			}
		}
		else if($val['t']['type'] == "taxonomy")
		{
			// $const = constant( strtoupper( $post_type ) . "_EDIT" );
			// PEUser::access_caps_gq( $const, "you not right! " );
			
			if( isset($args['id']) && $args['id'] > 0 )
			{			
				$res = $class_name::update( $args['input'], $args['id'] );
				$class_name::clear_instance($args['id']);
				
				if(is_wp_error($res))
				{
					PECore::addLog( $args, $res, "create_taxonomy");
					throw new PE_GraphQL_Exception($res->get_error_message());
				}
				else
				{
					$example = $class_name::get_instance( $res['term_id']  ); 
					$matrix = apply_filters(
						"bio_gq_change_" . $post_type,
						$example->get_single_matrix(),
						$example
					); 
				}
			}
			else
			{ 
				$res = $class_name::insert( $args['input'] ); 
				if(is_wp_error($res))
				{
					PECore::addLog( $args, $res, "change_taxonomy");
					throw new PE_GraphQL_Exception($res->get_error_message());
				}
				else
				{
					$example = $class_name::get_instance( $res["term_id"] );
					$matrix = $example->get_single_matrix();
				}
			}
		} 
		do_action(
			"after_gql_change_single_resolve",
			[ 
				"args" 			=> $args, 
				"class_name"	=> $class_name, 
				"post_type"		=> $post_type, 
				"value"			=> $val 
			]
		);
		return $matrix;
	}
	
	static function pe_graphql_make_schema()
	{ 
		
		/*
		PEGraphql::add_input_type([
			'name' => 'QRInput',
			//'description'	=> __("List of SVG-strings of QR-code", PE_QR_CODES),
			'fields' => [
				'qrs' => Type::listOf(Type::string())
			]
		]);
		PEGraphql::add_query( 
			"getZippedQRCodeList",
			[
				'description' 	=> __( "Returns URL ZIP-archive of SVG-files that upload from the client as an array of strings", PE_CORE ),
				'type' 			=> Type::string(),
				'args'         	=> [
					'input' 	=> [
						'list' 	=> Type::listOf(Type::string())
					]
				],
				'resolve' => function( $root, $args, $context, $info )
				{ 
					return "BOOO";
				}
			]
		);
		*/	
		PEGraphql::add_scalar_type([
			"name" => "Role",
			'serialize' => function ($value) { return (string)$value; },
			'parseValue' => function ($value) { return $value; },
			'parseLiteral' => function (Node $valueNode, ?array $variables = null) { return $value; },

		]);
		PEGraphql::add_scalar_type([
			"name" => "MediaURL",
			'serialize' => function ($value) { return (string)$value; },
			'parseValue' => function ($value) { return $value; },
			'parseLiteral' => function (Node $valueNode, ?array $variables = null) { return $value; },

		]);
		PEGraphql::add_scalar_type([
			"name" => "Date",
			'serialize' => function ($value) { return (string)$value; },
			'parseValue' => function ($value) { return (string)$value; },
			'parseLiteral' => function (Node $valueNode, ?array $variables = null) { return (string)$value; },

		]);
		PEGraphql::add_scalar_type([
			"name" => "URL",
			'serialize' => function ($value) { return (string)$value; },
			'parseValue' => function ($value) { return (string)$value; },
			'parseLiteral' => function (Node $valueNode, ?array $variables = null) { return (string)$value; },

		]);
		PEGraphql::add_scalar_type([
			"name" => "HTML",
			'serialize' => function ($value) { return (string)$value; },
			'parseValue' => function ($value) { return (string)$value; },
			'parseLiteral' => function (Node $valueNode, ?array $variables = null) { return (string)$value; },

		]);
			
		 
		
		PEGraphql::add_object_type([
			'name' => 'QRCodeSVG',
			'fields' => [
				"list" => Type::listOf( Type::string() )
			]
		]);
		
		PEGraphql::add_input_type([
			'name' => 'QRCodesInput',
			'fields' => [
				"list" => Type::listOf( Type::listOf( Type::string() ) )// Type::listOf( PEGraphql::object_type("QRCodeSVG" ) )
			]
		]);
		 
		PEGraphql::add_query(
			"getZippedQR",  
			[
				'type' => PEGraphql::object_type("Menu"),
				'args' => [
					'input'	=> PEGraphql::input_type( "QRCodesInput" )
				],
				'resolve' => function ($root, $args) 
				{
					$input = $args['input']['list'];
					$name = MD5(time());
					$zip = new ZipArchive();
					$zip->open( ABSPATH . "/qr/$name.zip", ZipArchive::CREATE|ZipArchive::OVERWRITE );					
					foreach($input as $value)
					{
						$search = "/~~~/i";
						$replace = '"';
						$svg = preg_replace(
							$search, 
							$replace, 
							$value[1]
						);
						
						/*
						// png convert 						
						$im = new Imagick();
						$im->readImageBlob($svg);
						$im->setImageFormat("png24");
						$im->resizeImage(300, 300, imagick::FILTER_LANCZOS, 1);
						$img = $im->getImagesBlob();	
						$zip->addFromString($value[0] . ".png", $img );	
						
						$im->clear();
						$im->destroy();						
						// end png convert
						*/
						$zip->addFromString($value[0] . ".svg", $svg );	
						
										
					}
					$zip->close();
					
					/**/
					return [
						"json" => get_bloginfo("url") . "/qr/$name.zip"
					];
				}
			]
		);
	}
	
	
	static function exec_filtered( $obj, $options, $isForInput = false )
	{ 
		$PE_Object_type = PE_Object_Type::get_instance(true);	
		
		$type = Type::string();
		$fields		= [
			"id"	=> [
				"type" => $type
			]
		] ;
		foreach($obj as $key => $value)
		{
			$resolve = null; 
			switch( $obj[$key]['type'] )
			{
				case "boolean":
					$type = Type::boolean();
					$resolve =  function(
						$root, 
						$args, 
						$context, 
						$info 
					) use( $options, $key, $PE_Object_type ) // 
					{ 
						return (bool)$options[$key];
					};
					break;
				case "date":
					$type = Type::int();
					$resolve =  function(
						$root, 
						$args, 
						$context, 
						$info 
					) use( $options, $key, $PE_Object_type ) // 
					{ 
						return $options[$key];
					};
					break;
				case "number":
					$type = Type::int();
					$resolve =  function(
						$root, 
						$args, 
						$context, 
						$info 
					) use( $options, $key, $PE_Object_type ) // 
					{ 
						return $options[$key] ? (int)$options[$key] : 0;
					};
					break;
				case "post_status":
				case "string":
				case "url":
					$type = Type::string();
					$resolve =  function(
						$root, 
						$args, 
						$context, 
						$info 
					) use( $options, $key, $PE_Object_type ) // 
					{ 
						return (string)$options[$key];
					};
					break;
				case "json":
					$type = Type::string();
					$resolve =  function(
						$root, 
						$args, 
						$context, 
						$info 
					) use( $options, $key, $PE_Object_type ) // 
					{ 
						return (string)json_encode( $options[$key], JSON_UNESCAPED_UNICODE );
					};
				case "period":
					$type = Type::listOf(Type::int());
					break;
				case "picto":
					$type = Type::string();
					$resolve =  function(
						$root, 
						$args, 
						$context, 
						$info 
					) use( $options, $key, $PE_Object_type ) // 
					{ 
						//родительский объект
						$media_id 	= $options[$key];
						$url 		= !is_wp_error($media_id) ? wp_get_attachment_url($media_id) : "";
						return $url;
					};
					break;
				case "media":
					$type = Type::string();
					$resolve =  function(
						$root, 
						$args, 
						$context, 
						$info 
					) use( $options, $key, $PE_Object_type ) // 
					{ 
						//родительский объект
						$media_id 	= $options[$key];
						$url 		= !is_wp_error($media_id) ? wp_get_attachment_url($media_id) : "";
						return $url;
					};
					break;
				case "geo":
					$type = Type::listOf(Type::string());
					$resolve =  function(
						$root, 
						$args, 
						$context, 
						$info 
					) use( $options, $key, $PE_Object_type ) // 
					{ 			
						// $geo = 
						return $key;
					};
					break;
				case "post":
				case "taxonomy":  		
					$child_type  	= $obj[ $key ][ 'object' ];
					$childs_data 	= $PE_Object_type->object[ $child_type ];
					if($childs_data)
					{
						$type = $isForInput ? Type::int() : PEGraphql::object_type($childs_data['class']['type']);
					}
					$resolve =  function(
						$root, 
						$args, 
						$context, 
						$info 
					) use( $options, $child_type, $key, $PE_Object_type, $obj ) // 
					{ 
						//родительский объект
						$instance = $options;
						
						// дочерний элемент, полученный по meta-полю 
						$classs 	= $child_type['smc_post'];
						
						//
						if($obj[$key]['type'] == "post")
						{ 
							return apply_filters(
								"bio-gq-post-meta", 
								$classs::get_single_matrix( $instance[ $key ] ),
								$instance,
								$classs,
								$key
							);
						}
						else if ($obj[$key]['type'] == "taxonomy")
						{
							$element 	= $classs::get_instance( $instance[ $key ] );							
							return  apply_filters(
								"bio-gq-term-meta", 
								$element->get_single_matrix( ),
								$instance,
								$classs,
								$key
							);
						}
					};
					break;
				/*
				case "array":	
					switch($obj[$key]['class'])
					{
						case "string":
							$list_type = Type::string();
							break;
						case "number":
							$list_type = Type::int();
							$resolve =  function(
								$root, 
								$args, 
								$context, 
								$info 
							) use( $class_name, $child_type, $key, $PE_Object_type, $obj ) // 
							{
								$instance  	= $class_name::get_instance($root['id']);
								$meta 		= $instance->get_meta( $key );
								if(!is_array($meta))
								{
									$meta = [];
								}
								return $meta;
							};
							break;
						case "special":	
							$list_type = apply_filters(
								"peql_array_special", 
								Type::string(), 
								$obj, 
								$key,
								$isForInput
							);
							//wp_die( $obj[ $key ] );
							$resolve =  function(
								$root, 
								$args, 
								$context, 
								$info 
							) use( $class_name, $child_type, $key, $PE_Object_type, $obj ) // 
							{
								return apply_filters(
									"peql_array_spesial_resolve", 
									[],
									$root, 
									$args, 
									$context, 
									$info, 
									$class_name, 
									$child_type, 
									$key, 
									$PE_Object_Type, 
									$obj 
								);
							};
							break;
						default:
							$list_type = Type::string();
							//wp_die( [$class_name, $obj[$key]['class']] );
							require_once( BIO_REAL_PATH."class/PE_Object_type.php" );
							$PE_Object_Type = PE_Object_Type::get_instance(true);
							switch( $obj[$key]['class'] )
							{
								case "taxonomy":
									$child_type = "PE_Taxonomy";
									break;
								case "post":
								default:
									$child_type = "PE_Post";
									break;
							}
							foreach( $PE_Object_type->object as $_key => $_value)
							{
								if( $PE_Object_type->object[$_key]['class']['type'] == $obj[$key]['class'] )
								{
									$list_type = $isForInput 
										? 
										Type::int() 
										: 
										PEGraphql::object_type($PE_Object_Type->object[$_key]['class']['type']);
										
									$child_type = $PE_Object_Type->object[$_key]['class']['type'];
									break;
								}
							}
							$resolve =  function( 
								$root, 
								$args, 
								$context, 
								$info 
							) use(
								$class_name, 
								$child_type, 
								$obj, 
								$key,
								$PE_Object_Type
							) 
							{
								if (!class_exists($child_type) || !method_exists($child_type, "get_type"))
								{
									return;
								}
								$cl 			= $obj[$key]['class'];	
								$order 			= $obj[$key]['order'];	
								$orderby		= $obj[$key]['orderby'];	
								$element_type 	= $PE_Object_type->object[ $cl::get_type() ]['t']['type'];
								$parent_type 	= $PE_Object_type->object[ $class_name::get_type() ]['t']['type'];
								$results = [];
								if( $element_type == "post" )
								{
									$args = [
										"post_type"		=> $child_type::get_type(),
										"count"			=> 1000,
										"author"		=> -1,
										"post_status"	=> "publish"
											
									];
									switch($parent_type)
									{
										case "post":
											$args["relation"] 	= "AND"; 
											$args["fields"] 	= "all"; 
											$args["order"] 		= "DESC"; 
											$args["order_by"] 	= "title"; 
											$args["post_status"]= "publish"; 
											$args["offset"] 	= 0; 
											$args["metas"] 		= [ 
												[
													"key" 			=> $class_name::get_type(), 
													"value_numeric"	=> (int)$root["id"],
													"compare"		=> "="
												] 
											]; 
											break;
										case "taxonomy":
										default:
											$args['tax_query'] 	= [
												[	
													'taxonomy' 	=> $class_name::get_type(),
													'field'    	=> 'id',
													'terms'    	=> $root["id"],
												]
											];
											$args['taxonomies'] = [
												[
													"tax_name"	=> $class_name::get_type(),
													"term_ids"	=> $root["id"]
												]
											];
											
									}
									if( isset($orderby) )
									{
										switch($orderby)
										{
											case "id":
											case PE_POST_TITLE:
											case PE_POST_AUTHOR:
												break;
											default:
												$args["orderby"] = "meta_value_num";
												$args["meta_key"]= $orderby;
												break;
										}
									}
									if( isset($order) )
									{
										$args['order'] = $order;
									} 
									$results 	= $child_type::get_all_matrixes($args);
									//wp_die( [ $args, $results ] );
								}
								else if($element_type == "taxonomy")
								{
									$args = [
										"number"		=> 10000,
										'taxonomy'      => [ $cl::get_type() ],
										'hide_empty' 	=> false,
										"meta_query"	=> [
											'relation' 	=> 'AND',
											[
												"key" 	=> $class_name::get_type(),
												"value"	=> $root["id"],
												"compare" => "=",
												//'type'    => 'NUMERIC'
											]
										]
									];
									$subobjects = get_terms( $args );
									// wp_die( $args );
									foreach($subobjects as $cc)
									{
										$course 	= Bio_Course::get_instance($cc->term_id);
										$results[] 	= $course->get_single_matrix();
										
									}
									//wp_die( $results );
								}
								
								return $results;
							};
					}
					if(!$isForInput)
					{
						$type = Type::listOf( $list_type );
					}
					break;
				*/
				case "id":
					$type = Type::int();
					break;
				case "user":
					$type = $isForInput ? Type::int() : PEGraphql::object_type("User");
					$resolve =  function(
						$root, 
						$args, 
						$context, 
						$info 
					) use( $class_name, $child_type, $key, $PE_Object_Type, $obj ) // 
					{
						//родительский объект
						$instance = $options;	
						//wp_die( $meta );
						$user = PEUser::get_user( $instance[$key] );
						return $user;
					};
					break;					
				default: 
					$type = Type::string();
			}
			if( $type )
			{
				$fields[ $key ] = [ 
					'type' 			=> $type, 	
					'description' 	=> __( $obj[$key]['name'], PE_CORE )  . " " . __( $obj[$key]['description'], PE_CORE ),
					"resolve" 		=> $resolve,
				];
			}
		}
		
		return $fields;
	}
	static function get_exec_filtered( $isForInput = false )
	{ 
		$PE_Object_Type 	= PE_Object_Type::get_instance( true);	
		return static::exec_filtered( $PE_Object_Type->options, PECore::$options, $isForInput ); 	  
		
	}
	
	static function exec_init()
	{
		PEGraphql::add_object_type([
			'name' => 'Menu',
			'fields' => [
				'json' => Type::string()
			]
		]);	
		PEGraphql::add_query(
			"getMenu",  
			[
				'type' => PEGraphql::object_type("Menu"),
				'args' => [ ],
				'resolve' => function ($root, $args) 
				{
					$menu = get_option("bio_menu");
					return [
						"json" => $menu
					];
				}
			]
		);
		PEGraphql::add_input_type([
			'name' => 'MenuInput',
			'fields' => [
				'json' => Type::string()
			]
		]);
		PEGraphql::add_mutation( 
			"changeMenu",
			[
				'description' 	=> __( "Change Menu", PE_CORE ),
				'type' 			=> Type::boolean(),
				'args'         	=> [
					'input' 	=> [
						'type' 	=> PEGraphql::input_type('MenuInput')
					]
				],
				'resolve' => function( $root, $args, $context, $info )
				{
					// wp_die($args['input']['json']);
					update_option("bio_menu", $args['input']['json']);
					return true;
				}
			]
		);
		
		PEGraphql::add_object_type([
			'name' => 'RouteFolder',
			'description' 	=> __( "Folder in Editor Cabinet. UX-group of route's page  ", PE_CORE ),
			'fields' => [
				'json' => Type::string()
			]
		]);	
		PEGraphql::add_query(
			"getRouteFolder",  
			[
				'type' => PEGraphql::object_type("Menu"),
				'args' => [ ],
				'resolve' => function ($root, $args) 
				{
					$menu = get_option("pe_route_folder");
					return [
						"json" => $menu
					];
				}
			]
		);
		PEGraphql::add_input_type([
			'name' => 'RouteFolderInput',
			'fields' => [
				'json' => Type::string()
			]
		]);
		PEGraphql::add_mutation( 
			"changeRouteFolder",
			[
				'description' 	=> __( "Change Routes Folder", PE_CORE ),
				'type' 			=> Type::boolean(),
				'args'         	=> [
					'input' 	=> [
						'type' 	=> PEGraphql::input_type('RouteFolderInput')
					]
				],
				'resolve' => function( $root, $args, $context, $info )
				{
					// wp_die($args['input']['json']);
					update_option("pe_route_folder", $args['input']['json']);
					return true;
				}
			]
		);
		
		// Template options
		PEGraphql::add_object_type([
			'name' => 'Template',
			'fields' => [
				'json' => Type::string()
			]
		]);	
		PEGraphql::add_query(
			"getTemplate",  
			[
				'type' => PEGraphql::object_type("Template"),
				'args' => [ ],
				'resolve' => function ($root, $args) 
				{
					$Template = get_option("bio_template");
					if(!$Template) $Template = '';

					
					//!
					$PE_Object_Type	= PE_Object_Type::get_instance(true);	
					foreach( $PE_Object_Type->object as $post_type => $val )
					{
						$obj	= $PE_Object_Type->object[ $post_type ]; 
						//var_dump( json_encode( $obj );
						extract(static::getFields( $obj, $post_type, $val ));
						//$class_name				= $obj['class']['type'];
						if($class_name = "PEQRCode")
							$txt = json_encode($fields) . ' !----! ';
					}
					//!
					
					
					return [
						"json" => $txt //$Template
					];
				}
			]
		);
		PEGraphql::add_input_type([
			'name' => 'TemplateInput',
			'fields' => [
				'json' => Type::string()
			]
		]);
		PEGraphql::add_mutation( 
			"changeTemplate",
			[
				'description' 	=> __( "Change client's Template", PE_CORE ),
				'type' 			=> Type::boolean(),
				'args'         	=> [
					'input' 	=> [
						'type' 	=> PEGraphql::input_type('TemplateInput')
					]
				],
				'resolve' => function( $root, $args, $context, $info )
				{
					// wp_die($args['input']['json']);
					update_option("bio_template", $args['input']['json']);
					return true;
				}
			]
		);
		
		//Special options
		PEGraphql::add_object_type([
			'name' => 'Special',
			'fields' => [
				'is_load_menus' 	=> Type::boolean(),
				'is_load_settings' 	=> Type::boolean(),
				'is_load_data_type' => Type::boolean()
			]
		]);	
		PEGraphql::add_query(
			"getSpecial",  
			[
				'type' => PEGraphql::object_type("Special"),
				'args' => [ ],
				'resolve' => function ($root, $args) 
				{
					$Special = get_option("bio_special_options");
					if(!is_array($Special)) $Special = []; 
					return [
						"is_load_menus" 	=> $Special['is_load_menus'],
						"is_load_settings" 	=> $Special['is_load_settings'],
						"is_load_data_type" => $Special['is_load_data_type'],
					];
				}
			]
		);
		PEGraphql::add_input_type([
			'name' => 'SpecialInput',
			'fields' => [
				'is_load_menus' 	=> Type::boolean(),
				'is_load_settings' 	=> Type::boolean(),
				'is_load_data_type' => Type::boolean()
			]
		]);
		PEGraphql::add_mutation( 
			"changeSpecial",
			[
				'description' 	=> __( "Change Special client options", PE_CORE ),
				'type' 			=> Type::boolean(),
				'args'         	=> [
					'input' 	=> [
						'type' 	=> PEGraphql::input_type('SpecialInput')
					]
				],
				'resolve' => function( $root, $args, $context, $info )
				{
					$Special = [];
					$fields = [ 'is_load_menus','is_load_settings','is_load_data_type' ];
					foreach($fields as $field)
					{
						if(isset($args['input'][$field]))
						{
							$Special[$field] = $args['input'][$field];
						}
					}
					update_option("bio_special_options", $Special );
					return true;
				}
			]
		);
		
		
		// WidgetArea 
		PEGraphql::add_object_type([
			'name' => 'WidgetArea',
			'fields' => [
				'json' => Type::string()
			]
		]);
		PEGraphql::add_query(
			"getWidgetArea",  
			[
				'type' => PEGraphql::object_type("WidgetArea"),
				'args' => [ ],
				'resolve' => function ($root, $args) 
				{
					$widgets = get_option("bio_widgets");
					return [
						"json" => $widgets
					];
				}
			]
		);
		PEGraphql::add_input_type([
			'name' => 'WidgetAreaInput',
			'fields' => [
				'json' => Type::string()
			]
		]);
		PEGraphql::add_mutation( 
			"changeWidgetArea",
			[
				'description' 	=> __( "Change Widget Areas", PE_CORE ),
				'type' 			=> Type::boolean(),
				'args'         	=> [
					'input' 	=> [
						'type' 	=> PEGraphql::input_type('WidgetAreaInput')
					]
				],
				'resolve' => function( $root, $args, $context, $info )
				{
					// wp_die($args['input']['json']);
					update_option("bio_widgets", $args['input']['json']);
					return true;
				}
			]
		);
		
		
		PEGraphql::add_object_type([
			'name' => 'Init',
			'fields' => [
				'menu'				=> PEGraphql::object_type("Menu"),
				'folder' 			=> PEGraphql::object_type("RouteFolder"),
				'widgets' 			=> PEGraphql::object_type("WidgetArea"),
				"template"			=> PEGraphql::object_type("Template"),
				"public_options"	=> PEGraphql::object_type("PublicOptions"),
				"version"			=> Type::string()
			]
		]);		
		PEGraphql::add_query(
			"getInit",  
			[
				'type' => PEGraphql::object_type("Init"),
				'args' => [
					'version' => Type::string()
				],
				'resolve' => function ($root, $args) 
				{
					$Special = get_option("bio_special_options");
					$fields = [ 'is_load_menus','is_load_settings','is_load_data_type' ];
					
					$template =  $Special['is_load_menus'] ? get_option("bio_template") : "";
					if(!$template) 	$template = "";
					
					$menu = $Special['is_load_menus'] ? get_option("bio_menu") : '';
					//$menu = get_option("bio_menu");
					if(!$menu) 		$menu = "";
					
					$pe_route_folder =  $Special['is_load_menus'] ? get_option("pe_route_folder") : "";
					if(!$pe_route_folder) 	$pe_route_folder = "[]"; 
					
					$widgets =  $Special['is_load_widgets'] ? get_option("bio_widgets"):"";
					if(!$widgets) 	$widgets = "";
					
					$public_options = json_encode(apply_filters( "pe_get_public_gq_options", [ ], $Special ));
					
					$version = get_option("pe_layout_version");
					$version = $version ? $version : "1.01";
					
					return [
						"menu"	=> [
							"json" => $menu
						],
						"widgets"	=> [
							"json" => $widgets
						],
						"public_options" => [
							"json" => $public_options 
						],
						"template" => [
							"json" => $template 
						],
						"folder" => [
							"json" => $pe_route_folder 
						],
						"version" => $version
					];
				}
			]
		);
		
	}
	static function exec_options()
	{  
		try
		{
			static::exec__pe_options();
		}
		catch(\Exception $e)
		{
			PECore::addLog( [], $e, "exec_options");
			throw new Exception ("Error!!!!");
		}
	}
	static function exec__pe_options()
	{   
		$options = apply_filters( 
			"bio_gq_options",
			static::get_exec_filtered() 			
		); 
		PEGraphql::add_object_type( 
			[
				"name"			=> "Options",
				"description"	=> __( "main options of portal", PE_CORE ),
				"fields"		=> $options
			]
		);		
		
		PEGraphql::add_input_type( 
			[
				"name"		=> 'OptionsInput',
				'description' => __( "main options of portal", PE_CORE ),
				"fields"		=> function() {
					$arr = [
						"json"		=> Type::string()
					];
					$PE_Object_Type = PE_Object_Type::get_instance(true);
					foreach( $PE_Object_Type->options as $key => $option ) {  
						switch($option["type"]) {
							case "number":
								$arr[$key] = Type::int();
								break;
							case "boolean":
								$arr[$key] = Type::boolean();
								break;
							case "media":
							case "url":
							case "adress":
							case "email":
							case "string":
								$arr[$key] = Type::string();
								break;							
						}
					}
					return $arr;
				}
			]
		);
		/**/
		
		PEGraphql::add_query( 
			'getOptions', 
			[
				'description' => __( 'Get portal settings', PE_CORE ),
				'type' 		=> PEGraphql::object_type("Options"),
				'args'     	=> [ 
					"id" => Type::string()	
				],
				'resolve' 	=> function( $root, $args, $context, $info ) use($class_name, $post_type, $val)
				{		  
					if(!current_user_can("manage_options"))
						PECore::addLog( $args, [], "not_rights_get_options");
						throw new PEGraphQLNotAccess( "you not right!" );
					$options = apply_filters( "bio_get_gq_options", [ ] );
					$matrix = [ 
						"json" => json_encode( $options ) 
					];
					$PE_Object_Type = PE_Object_Type::get_instance(true);
					foreach( $PE_Object_Type->options as $key => $option ) {
						switch( $option["type"] ) {	
							case "boolean":
								$matrix[ $key ] = (boolean)PECore::$options[ $key ];
								break;
							case "number":
								$matrix[ $key ] = (int)PECore::$options[ $key ];
								break;
							case "media":
								$ee = PECore::$options[ $key ] === "false" 
									? 
									"" 
									: 
									wp_get_attachment_url( PECore::$options[ $key ] );
								$matrix[ $key ]	= (string)$ee;
								break;
							case "url":
							case "adress":
							case "email":
							case "string":
								$matrix[ $key ] = (string)PECore::$options[ $key ];
								break;
						}
					}
					return $matrix;
					/*
					$matrix = [];
					$PE_Object_Type 	= PE_Object_Type::get_instance(true); 		
					foreach( $PE_Object_Type->options as $key => $value )
					{
						switch($value["type"])
						{
							case "media":
								$matrix[ $key ] = wp_get_attachment_url( PECore::$options[ $key ] );
								break;
							default:
								$matrix[ $key ] = PECore::$options[ $key ];
						}
					}
					$options = apply_filters( "bio_get_gq_options", $matrix );
					return $options;
					*/
				}
			] 
		);
		
		PEGraphql::add_mutation ( 
			'changeOptions', 
			[
				'description' 	=> __( "Change portal settings for Administrator", PE_CORE ),
				'type' 			=> PEGraphql::object_type("Options"),
				'args'         	=> [
					"id"	=> Type::string(),
					'input' => [
						'type' => PEGraphql::input_type("OptionsInput"),
					]
				],
				'resolve' => function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
				{	
					if(!current_user_can("manage_options"))
						PECore::addLog( $args, [], "not_rights_change_options");
						throw new PEGraphQLNotAccess( "you not right!" );
					
					$PE_Object_Type 	= PE_Object_Type::get_instance(true);
					$output = []; 
					//if( is_array($options) || true )
					{
						foreach( $PE_Object_Type->options as $key => $value )
						{
							if( !isset( $args["input"][ $key ] ) ) continue;
							 
							switch( $value['type'] )
							{
								case "media": 
									if( $args["input"][ $key ] && substr($args["input"][ $key ],0, 4) != "http" )
									{
										$media = PE_Assistants::insert_media(
											[
												"data" => $args["input"][ $key ],
												"media_name" => $args["input"][ $key . '_name' ] 
													? 
													$args["input"][ $key . '_name' ] 
													: 
													"Options_" . $key . ".jpg"
											]
										);
										/*										
										wp_set_object_terms(
											$media['id'], 
											(int)PECore::$options['icon_media_term'],
											PE_CORE_MEDIA_TAXONOMY_TYPE 
										); 
										*/
										$output[ $key ] = $media['url'];
										PECore::$options[ $key ] = $media['id'];
									}
									else if($args[ "input" ][ $key . "_id"])
									{
										$output[ $key ] = $args[ "input" ][ $key ];
										PECore::$options[ $key ] = $args[ "input" ][ $key . "_id" ];
										PECore::$options[ $key ] = $args[ "input" ][ $key . "_id" ];
									}
									
									break;
								case "string":
								default:
									$output[ $key ] = $args[ "input" ][ $key ];
									PECore::$options[ $key ] = $args[ "input" ][ $key ];
									break;
							}
							
						}
					}
					update_option(PE_CORE, PECore::$options); 
					return $output; 
				}
			] 
		);
		
		
		
		PEGraphql::add_object_type( 
			[
				"name"			=> "PublicOptions",
				"description"	=> __( "main public options of portal", PE_CORE ),
				"fields"		=> function() {
					$arr = [
						"json"		=> Type::string()
					];
					$PE_Object_Type = PE_Object_Type::get_instance(true);
					foreach( $PE_Object_Type->options as $key => $option ) {  
						switch($option["type"]) {
							case "number":
								$arr[$key] = Type::int();
								break;
							case "boolean":
								$arr[$key] = Type::boolean();
								break; 
							case "media": 
							case "url":
							case "adress":
							case "email":
							case "string":
								$arr[$key] = Type::string();
								break;							
						}
					}
					return $arr;
				}
			]
		);
		PEGraphql::add_query( 
			'getPublicOptions', 
			[
				'description' => __( 'Get portal public settings', PE_CORE ),
				'type' 		=> PEGraphql::object_type("PublicOptions"),
				'args'     	=> [
					"id"	=> Type::string()	
				],
				'resolve' 	=> function( $root, $args, $context, $info ) use($class_name, $post_type, $val)
				{		
					//wp_die((apply_filters( "pe_get_public_gq_options", [ ] )));
					$options = apply_filters( "pe_get_public_gq_options", [ ] );
					$arr = [ 
						"json" => json_encode( $options ) 
					];
					$PE_Object_Type = PE_Object_Type::get_instance(true);
					foreach( $PE_Object_Type->options as $key => $option ) {
						switch( $option["type"] ) {	
							case "boolean":
								$arr[ $key ] = (boolean)PECore::$options[ $key ];
								break;
							case "number":
								$arr[ $key ] = (int)PECore::$options[ $key ];
								break;
							case "media":
								$ee = PECore::$options[ $key ] === "false" 
									? 
									"" 
									: 
									(string)wp_get_attachment_url( PECore::$options[ $key ] );
								$arr[ $key ] 	= $ee;
								break;
							case "url":
							case "adress":
							case "email":
							case "string":
								$arr[ $key ] = (string)PECore::$options[ $key ];
								break;
						}
					}
					return $arr;
				}
			] 
		);
	}  
	
	static function exec_comments()
	{
		
		PEGraphql::add_object_type( 
			[
				"name"			=> "Comment",
				"description"	=> __( "single message in hierarhical post's discussion", PE_CORE ),
				"fields"		=> apply_filters( "bio_gq_comment", [ 
					"id" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'Uniq identificator', PE_CORE )
					], 
					"discussion_id" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( "Post's disscussion id", PE_CORE )
					], 
					"discussion_type" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( "post or taxonomy", PE_CORE ), 
						'name' => 'discussion_type' 
					], 
					"discussion"  			=> [
						'type' => PEGraphql::object_type("Post"), 	
						'description' 	=> __( "post or taxonomy element", PE_CORE ),
						"resolve"		=> function( $root, $args, $context, $info )
						{
							if( $root["discussion_type"] == "post" )
							{
								$matrix = get_post($root["discussion_id"]);
								return [
									"id"			=> $matrix->ID,
									PE_POST_TITLE	=> $matrix->post_title,
									PE_POST_NAME	=> $matrix->post_name,
									PE_POST_TYPE	=> $matrix->post_type,
								];
							}
							else if( $root["discussion_type"] == "term" )
							{											
								$matrix = get_term($root["discussion_id"]);
								//wp_die( [$root["discussion_id"], $matrix] );
								return [
									"id"			=> $matrix->term_id,
									PE_POST_TITLE	=> $matrix->name,
									PE_POST_NAME	=> $matrix->slug,
									PE_POST_TYPE	=> $matrix->type,
								];
							}
							else
							{
								PECore::addLog( $args, $root, "unknown_discussion_type");
								throw new \PEGraphQLNotAccess( "unknown discussion type" );
							}
						}						
					],
					"content" 				=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'text of comment', PE_CORE )
					], 
					"parent_id" 		=> [ 
						'type' => Type::string(),		
						'description' 	=> __( "parent comment's id", PE_CORE )
						
					], 
					"parent" 		=> [ 
						'type' => PEGraphql::object_type("Comment"),		
						'description' 	=> __( 'parent', PE_CORE ),
						"resolve"	=> function( $root, $args, $context, $info )
						{
							
							return PEComment::get_single_matrix( $root["parent_id"] );
						}
					], 
					"author"		=> [ 
						'type' => PEGraphql::object_type("User"), 			
						'description' 	=> __( 'authorize author of comment', PE_CORE ),
						"resolve"	=> function( $root, $args, $context, $info )
						{
							// wp_die($root);
							if( isset($root["user_id"]) &&  $root["user_id"]> 0)
							{
								$user = PEUser::get_user( $root['user_id'] );
							}
							else
							{
								$user = apply_filters(
									"bio_get_user",
									[
										"id"				=> -1,
										"ID"				=> -1,
										"display_name"		=> $root['comment_author'],
										"first_name"		=> $root['comment_author'],
										"last_name"			=> $root['comment_author'],
										"roles"				=> [],
										"caps"				=> [],
										"caps1"				=> [],			
										"user_email"		=> $root['comment_author_email'],
										"account_activated"	=> false,
										"is_blocked"		=> true,
										"__typename" 		=> "User"
									],
									new StdClass
								);
							}
							return $user;
						}
						
					], 
					"date" 		=> [ 
						'type' => Type::int(), 		
						'description' 	=> __( 'UNIXtime int. Date of creation.', PE_CORE )
					], 
					"is_approved"		=> [ 
						'type' => Type::boolean(), 		
						'description' 	=> __( 'Approve', PE_CORE )
					]
				] )
			]
		);
		PEGraphql::add_input_type( 
			[
				"name"		=> 'CommentPaging',
				'description' => __( "single message in hierarhical post's discussion", PE_CORE ),
				'fields' 		=> apply_filters( "bio_gq_commentpaging_input", [ 
					"id" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'Uniq identificator', PE_CORE ), 
						'name' => 'id' 
					],
					"offset" => [
						'type' => Type::int(), 	
						'description' 	=> __( 'Pagination offset', PE_CORE ), 
					],
					"count" => [
						'type' => Type::int(), 	
						'description' 	=> __( "Pagination page's count of elements", PE_CORE ),
						"default"		=> 10						
					],
					"parent" => [
						'type' => Type::string(), 	
						'description' 	=> __( 'Parent Comment by this.', PE_CORE ), 
					],
					"discussion_id" => [
						'type' => Type::string(), 	
						'description' 	=> __( 'Uniq identificator', PE_CORE ), 
					],
					"discussion_type" => [
						'type' => Type::string(), 	
						'description' 	=> __( 'Uniq identificator', PE_CORE ), 
					],
					'author'	=> [
						'type' 	=> Type::string(),
						'description' => __( 'Author of comment', PE_CORE ) 
					],
					'is_admin'	=> [
						'type' 	=> Type::boolean(),
						'description' => __( 'For admin panel', PE_CORE ),
						"defaultValue" => false
					]
				])
			]
		);
		PEGraphql::add_input_type( 
			[
				"name"		=> 'CommentInput',
				'description' => __( "single message in hierarhical post's discussion", PE_CORE ),
				'fields' 		=> apply_filters( "bio_gq_comment_input", [ 
					"id" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'Uniq identificator', PE_CORE ), 
						'name' => 'id' 
					], 
					"author" 		=> [ 
						'type' => Type::int(), 	
						'description' 	=> __( "post or taxonomy", PE_CORE ), 
						'name' => 'author' 
					],
					"discussion_id" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( "Post's disscussion id", PE_CORE ), 
						'name' => 'discussion_id' 
					], 
					"discussion"  			=> [
						'type' => Type::int(), 	
						'description' 	=> __( "post or taxonomy element id", PE_CORE )
					],
					"discussion_type" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( "post or taxonomy", PE_CORE ), 
						'name' => 'discussion_type' 
					], 
					"content" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'text of comment', PE_CORE )
						
					], 
					"parent" 		=> [ 
						'type' => Type::int(),		
						'description' 	=> __( 'parent', PE_CORE )
						
					], 
					"author" 		=> [ 
						'type' => Type::int(), 			
						'description' 	=> __( 'authorize author of comment', PE_CORE )
						
					], 
					"date"		=> [ 
						'type' => Type::int(), 		
						'description' 	=> __( 'UNIXtime int. Date of creation.', PE_CORE ) 
						
					], 
					"is_approved"		=> [ 
						'type' => Type::boolean(), 		
						'description' 	=> __( 'Approve', PE_CORE ) 
						
					], 
					"is_admin"		=> [ 
						'type' => Type::boolean(), 		
						'description' 	=> __( 'For admin panel', PE_CORE ) 
						
					]
				]),
			]
		);
		
		PEGraphql::add_query( 
			'getComments', 
			[
				'description' 		=> __( 'Get comments array', PE_CORE ),
				'type' 				=> Type::listOf( PEGraphql::object_type("Comment") ),
				'args'     			=> [  
					'paging'	=> [
						"type" 	=> PEGraphql::input_type("CommentPaging") 
					]
				],
				'resolve' 			=> function( $root, $args, $context, $info )
				{	
					//wp_die($args['paging']);
					return PEComment::get_matrixes($args['paging']);
				}
			] 
		);
		PEGraphql::add_query( 
			'getCommentCount', 
			[
				'description' 		=> __( 'Get comments count', PE_CORE ),
				'type' 				=> Type::int(),
				'args'     			=> [  
					'paging'	=> [
						"type" 	=> PEGraphql::input_type("CommentPaging") 
					]
				],
				'resolve' 			=> function( $root, $args, $context, $info )
				{	
					return count(PEComment::get_matrixes($args['paging']));
				}
			] 
		);
		PEGraphql::add_query( 
			'getComment', 
			[
				'description' => __( 'Get single comment', PE_CORE ),
				'type' 		=> PEGraphql::object_type("Comment"),
				'args'     	=> [ 
					"id"	=> [
						"type" => Type::string()
					]
				],
				'resolve' 	=> function( $root, $args, $context, $info ) use($class_name, $post_type, $val)
				{		
					return PEComment::get_single_matrix( $args["id"] );
				}
			] 
		);
		
		PEGraphql::add_mutation ( 
			'changeComment', 
			[
				'description' 	=> __( "Change single comment", PE_CORE ),
				'type' 			=> Type::listOf( PEGraphql::object_type("Comment")),
				'args'         	=> [
					"id"	=> [ "type" => Type::string() ],
					'input'	=> [ 'type' => PEGraphql::input_type("CommentInput") ]
				],
				'resolve' => function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
				{ 
					if(!isset($args['id']))
					{
						$user = current_user_can("manage_option") 
							? 
							get_user_by("id", $args['input']['author'])
							:
							wp_get_current_user(); 
						
						if( !isset( $args['input']['discussion_id'] ) )
						{
							if( $args['input']['parent'] > 1)
							{
								$comment = PEComment::get_single_matrix($args['input']['parent']);
								$args['input']['discussion_id'] 	= $comment['discussion_id'];
								$args['input']['discussion_type'] 	= $comment['discussion_type'];
							}
							else
							{
								return;
							}
						}						
						
						$new_comment = wp_insert_comment( 
							apply_filters(
								"bio_gq_change_comment", [
									'comment_post_ID'	=> $args['input']['discussion_id'],
									'discussion_type'	=> $args['input']['discussion_type'],
									"comment_approved"	=> true,
									"user_id"			=> $user->ID,
									"comment_author"	=> $user->display_name,
									"comment_author_email" => $user->user_email,
									"comment_content"	=>  $args['input']['content'],
									"comment_parent"	=>  $args['input']['parent'],
									"comment_date"	 	=>  null
								],
								null
							)
						);
						if( $new_comment && $args['input']['discussion_type'] == "term" )
						{
							global $wpdb;
							$wpdb->query("UPDATE " . $wpdb->prefix . "comments SET discussion_type='term' ");
							//wp_die( [ $new_comment, $boo, $args['input']['discussion_type'] ] );
							
							$_comment = get_comment( $new_comment );
							$_comment->discussion_type = 'term';
							wp_cache_set( $_comment->comment_ID, $_comment, 'comment' );
						}
						//return Bio_Comment::get_single_matrix($new_comment);
						return PEComment::get_matrixes([
							"post_id" => $args['input']['discussion_id'] ,
							"status" => "approve" 
						]);
					}
					else
					{
						$user = current_user_can("manage_option") 
							? 
							get_user_by("id", $args['input']['author'])
							:
							get_current_user_id(); 
						$new_comment = wp_update_comment( 
							apply_filters(
								"bio_gq_change_comment", [
									'comment_post_ID'	=> $args['input']['discussion_id'],
									"comment_approved"	=> true,
									"user_id"			=> $user->ID,
									"comment_author"	=> $user->display_name,
									"comment_author_email" => $user->user_email,
									"comment_content"	=>  $args['input']['content'],
									"comment_parent"	=>  $args['input']['parent'],
									"comment_date"	 	=>  null, //$args['input']['date'], 
								],
								$args['id']
							)
						);
						return  PEComment::get_single_matrix($new_comment);
					}
					return false;
				}
			] 
		);
		
		
		PEGraphql::add_mutation( 
			'deleteComment', 
			[
				'description' 	=> __( "Delete single comment", PE_CORE ),
				'type' 			=> Type::boolean(),
				'args'         	=> [
					"id"	=> [
						'type' => Type::string(),
						'description' => __( 'Unique identificator', PE_CORE ),
					]
				],
				'resolve' => function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
				{
					return wp_delete_comment( $args['id'], true );
				}
			] 
		);
	}

	static function exec_graphql()
	{
		try
		{
			static::register_smc_posts( );
		}
		catch(PEGraphQLNotAccess $ew)
		{
			throw new PE_GraphQL_Exception ("Not Access");
		}
		catch(PEGraphQLNotLogged $ew)
		{
			throw new PE_GraphQL_Exception ("Not Logged");
		}
		catch(PEGraphQLNotAdmin $ew)
		{
			throw new PE_GraphQL_Exception ("Not Admin");
		}
		catch(PE_GraphQL_Exception $ew)
		{
			throw new PE_GraphQL_Exception ("Error");
		}
	}
	
	static function getFields( $obj, $post_type, $val = "", $isForInput = false )
	{
		$class_name				= $obj['class']['type'];
		$class_type 			= $obj['t']['type'];
		$arr[] 					= $class_name;
		$fields 				= [] ;	
		if(!$isForInput)
		{
			$fields["id"] 		= [ 
				'type' => Type::string(), 	
				'description' 	=> __( 'Uniq identificator', PE_CORE ), 'name' => 'id' 
			];
		}
		$fields[PE_POST_TITLE] 	= [
			'type' => Type::string(), 	
			'description' 	=> __( 'title', PE_CORE ) 
		];		
		$fields[PE_POST_NAME] 	= [
			'type' => Type::string(), 	
			'description' 	=> __( 'slug', PE_CORE ) 
		];			
		$fields[PE_POST_CONTENT]	= [ 
			'type' => Type::string(), 	
			'description' 	=> __( 'content. May be empty.', PE_CORE )
		];
		$fields[PE_POST_STATUS]	= [ 
			'type' => Type::string(), 	
			'description' 	=> __( '"publish" or "draft"', PE_CORE ) 
		];
		$fields[PE_POST_DATE] 	= [ 
			'type' => Type::int(), 		
			'description' 	=> __( 'UNIXtime int. Date of creation.', PE_CORE ) 
		];
		$fields["post_type"] 	= [ 
			'type' => Type::string(), 		
			'description' 	=> __( 'Post type', PE_CORE ) ,
			'resolve' 		=> function( $root, $args, $context, $info )use( $class_name, $class_type, $post_type ) {
				return $class_name;
			}
		];
		$fields["thumbnail"] 	= [ 
			'type' => Type::string(), 	
			'description' 	=> __( 'URL of media', PE_CORE ) 
		];
		$fields["thumbnail_id"] = [ 
			'type' => Type::string(), 		
			'description' 	=> __( 'ID of thumbnail. Need for uploading thumbnail from media-library', PE_CORE ) 
			
		];
		$fields["thumbnail_name"] = [ 
			'type' => Type::string(), 		
			'description' 	=> __( 'file name of thumbnail. Need for uploading new thumbnail', PE_CORE ) 
			
		];
		$fields["count"] = [ 
			'type' => Type::int(), 		
			'description' 	=> __( 'Only for group selection. Count of elements in group', PE_CORE ) 
			
		];
		$fields["offset"] = [ 
			'type' => Type::int(), 		
			'description' 	=> __( 'Only for group selection. Offset in group by zero.', PE_CORE ) 
			
		];
		$fields["parent"]		 = [
			'type' 			=> $isForInput ? Type::int() : PEGraphql::object_type($class_name),		
			'description' 	=> __( 'Parent element', PE_CORE )  ,
			'resolve' 		=> function( $root, $args, $context, $info )use( $class_name, $class_type, $post_type )
			{
				if($class_type == "post")
				{
					return null;
				}
				else if($class_type == "taxonomy")
				{
					$term = get_term($root['id'], $post_type);
					if( $root['parent'] )
					{
						$term = $class_name::get_instance( $root['parent'] );
						return $term->get_single_matrix();
					}
					else
					{
						return null;
					}
				}
				else
				{
					return null;
				}
			}
		];
		$fields["main_parent"]		 = [
			'type' 			=> $isForInput ? Type::int() : PEGraphql::object_type($class_name),		
			'description' 	=> __( 'Top-level parent element', PE_CORE )  ,
			'resolve' 		=> function( $root, $args, $context, $info )use( $class_name, $class_type, $post_type )
			{
				if($class_type == "post")
				{
					return null;
				}
				else if($class_type == "taxonomy")
				{
					if( $root['parent'] )
					{
						$parent = get_term_top_most_parent($root['id'], $post_type);
						$term = $class_name::get_instance( $parent );
						return $term->get_single_matrix();
					}
					else
					{
						return null;
					}
				}
				else
				{
					return null;
				}
			}
		];
		$fields[PE_POST_AUTHOR] 	 = [ 
			'type' => $isForInput ? Type::int() : PEGraphql::object_type("User"), 	
			'description' 	=> __( 'Author of this element', PE_CORE ) ,
			'resolve' 		=> function( $root, $args, $context, $info )
			{
				//wp_die($root["post_author"]);
				if(!$root["post_author"]) return null;  
				$user = PEUser::get_user( $root['post_author'] );
				
				//wp_die($user);
				return $user['id'] ? $user : null;
			}
		];
		$fields["children"] = [ 
			'type' => Type::listOf($isForInput ? Type::int() : PEGraphql::object_type($class_name)), 	
			'description' 	=> __( '', PE_CORE ),
			'resolve' 		=> function( $root, $args, $context, $info )use( $class_name, $class_type, $post_type )
			{
				$children = [];
				if($class_type == "post")
				{
					return $children;
				}
				else if($class_type == "taxonomy")
				{
					$childs = $class_name::get_all( [ "parent" => $root[ "id" ], "order_by_meta" => "order" ] );
					foreach($childs as $child)
					{
						$ch = $class_name::get_instance( $child );
						$children[] = $ch->get_single_matrix( [ 'children' => $ch->id ] );
					}
					return $children;
				}
				else
				{
					return null;
				}
			}
		];
		
		$type = Type::string(); 
		foreach($obj as $key => $value)
		{ 
			$resolve = null;
			if($key == 't' ||$key == 'class' ) continue;
			
			switch( $obj[$key]['type'] )
			{
				case "boolean":
					$type = Type::boolean();
					break;
				case "date":
					$type = Type::int();
					$resolve =  function(
						$root, 
						$args, 
						$context, 
						$info 
					) use( $class_name, $child_type, $key, $PE_Object_Type ) // 
					{
						//wp_die( [ $key ] );						
						$inst = $class_name::get_instance($root['id']);
						return (int)$inst->get_meta($key);
					};
					break;
				case "number":
					$type = Type::int();
					
					$resolve =  function(
						$root, 
						$args, 
						$context, 
						$info 
					) use( $class_name, $child_type, $key, $PE_Object_Type ) // 
					{
						//wp_die( [ $key, $root['id'] ] );	 
						$inst = $class_name::get_instance($root['id']);
						$num = $inst->get_meta($key);
						if(!$num) $num = 0;
						return (int)$num;
					};
					/**/
					break;
				case "post_status":
				case "string": 
					$type = Type::string();
					break;
				case "period":
					$type = Type::listOf(Type::int());
					break;
				case "picto":
				case "media":
					$type = Type::string();
					$resolve =  function(
						$root, 
						$args, 
						$context, 
						$info 
					) use( $class_name, $child_type, $key, $PE_Object_Type ) // 
					{
						//родительский объект
						$instance 	= $class_name::get_instance( $root['id'] );
						$media_id 	= $instance->get_meta( $key );
						$url 		= !is_wp_error($media_id) ? wp_get_attachment_url($media_id) : "";
						return $url;
					};
					break;
				case "post":
				case "taxonomy": 
					$PE_Object_Type = PE_Object_Type::get_instance(true);		
					$child_type  	= $obj[ $key ][ 'object' ];
					$childs_data 	= $PE_Object_Type->object[ $child_type ];
					$child_class	= $childs_data["class"]["type"];
					if($childs_data)
					{ 
						if( (boolean)$obj[ $key ]["is_single"] ){ 
							$type = (boolean)$isForInput ? Type::int() : PEGraphql::object_type( $child_class );
						}
						else {							
							$type =(boolean)$isForInput ? Type::listOf( Type::int() ) : Type::listOf( PEGraphql::object_type( $child_class ) );
						}
					}
					
					$resolve =  function(
						$root, 
						$args, 
						$context, 
						$info
					) use( $class_name, $child_type, $key, $PE_Object_Type, $obj, $child_class, $type ) // 
						{ 
							/*if($child_class === "PEFestTrack") {
								wp_die([
									(boolean)$isForInput,
									$child_class,
									$type,
									$obj[ $key ]["is_single"]
								]);
							}*/
							if ( !class_exists( $child_class ) || !method_exists( $child_class, "get_type" ) )
							{
								return null;
							} 
							// родительский объект
							$instance = $class_name::get_instance( $root['id'] );
							// дочерний элемент, полученный по meta-полю
							//$classs 	= $PE_Object_Type->get_class_by_name( $child_class );
							 
							// 
							if($obj[$key]['type'] == "post") {
								return apply_filters(
									"bio-gq-post-meta", 
									$child_class::get_single_matrix( $instance->get_meta( $key ) ),
									$instance,
									$child_class,
									$key
								);
							}
							else if ($obj[$key]['type'] == "taxonomy") {
								if( $obj["t"]["type"] === "post" ) {
									$terms = wp_get_object_terms( $root['id'], $child_type );
								}
								else if( $obj["t"]["type"] === "taxonomy" ){
									$terms = $obj[ $key ]["is_single"]
										?
										get_term_meta($root['id'], $key, true)
										:
										get_term_meta($root['id'], $key, true);
								}
								if( $obj[ $key ]["is_single"] ) {
									if( count( $terms ) > 0 ) {
										$element = $child_class::get_instance( $terms[0] ); 
										// wp_die( $element->get_single_matrix() );
										return  apply_filters(
											"bio-gq-single-taxonomy", 
											$element->get_single_matrix(),
											$instance,
											$child_class,
											$key
										);
									}
								}
								else {
									$elements = [];
									foreach( $terms as $term ) {
										$element = $child_class::get_instance( $term );
										$elements[] = apply_filters(
											"bio-gq-taxonomy", 
											$element->get_single_matrix(),
											$instance,
											$child_class,
											$key
										);
									}
									return $elements;
								}
							}
						};
					break;
				case "array":
					switch($obj[$key]['class'])
					{
						case "string":
							
							$list_type = Type::string();// Type::listOf(Type::string());
							$resolve =  function(
								$root, 
								$args, 
								$context, 
								$info 
							) use( $class_name, $child_type, $key, $PE_Object_Type, $obj ) // 
							{
								
								// родительский объект
								$instance = $class_name::get_instance( $root['id'] );
								$meta = $instance->get_meta($key);
								if(!$meta) {
									//wp_die( $obj[$key]["default"]);
									$meta = $obj[$key]["default"]
										?
										$obj[$key]["default"]
										:
										[];
								} 
								return apply_filters(
									"peql_array_spesial_resolve", 
									$meta,
									$root, 
									$args, 
									$context, 
									$info, 
									$class_name, 
									$child_type, 
									$key, 
									$PE_Object_Type, 
									$obj 
								);
							};
							break;
						case "object":
							$list_type = Type::listOf(Type::string());
							$resolve =  function(
								$root, 
								$args, 
								$context, 
								$info 
							) use( $class_name, $child_type, $key, $PE_Object_Type, $obj ) // 
							{
								// родительский объект
								$instance = $class_name::get_instance( $root['id'] );
								$meta = $instance->get_meta($key); 
								
								//wp_die( $meta);
								return apply_filters(
									"peql_array_spesial_resolve", 
									PE_Post::arrayToJSONArray( $meta ), 
									$root, 
									$args, 
									$context, 
									$info, 
									$class_name, 
									$child_type, 
									$key, 
									$PE_Object_Type, 
									$obj 
								);
							};
							break;
						case "number":
							$list_type = Type::string();
							$resolve =  function(
								$root, 
								$args, 
								$context, 
								$info 
							) use( $class_name, $child_type, $key, $PE_Object_Type, $obj ) // 
							{
								$instance  	= $class_name::get_instance($root['id']);
								$meta 		= $instance->get_meta( $key );
								if(!is_array($meta))
								{
									$meta = [];
								}
								return $meta;
							};
							break;
						case "user":
							$list_type = $isForInput ? Type::int() : PEGraphql::object_type( "User" );
							$resolve =  function(
								$root, 
								$args, 
								$context, 
								$info 
							) use( $class_name, $key, $PE_Object_Type, $obj ) // 
							{
								return apply_filters(
									"peql_array_users_resolve", 
									[ ], 
									$root, 
									$args, 
									$context, 
									$info, 
									$class_name,  
									$key, 
									$PE_Object_Type, 
									$obj 
								);
								// $meta = [ ["display_name" => "AAAA"], ["display_name" => "BBBB"] ];
								// return $meta;
							};
							break; 
						case "special":	
							$list_type = apply_filters(
								"peql_array_special", 
								Type::string(), 
								$obj, 
								$key,
								$isForInput
							);
							// wp_die( $obj[ $key ] );
							$resolve =  function(
								$root, 
								$args, 
								$context, 
								$info 
							) use( $class_name, $child_type, $key, $PE_Object_Type, $obj ) // 
							{
								// wp_die( $obj[ $key ] );
								return apply_filters(
									"peql_array_spesial_resolve", 
									[],
									$root, 
									$args, 
									$context, 
									$info, 
									$class_name, 
									$child_type, 
									$key, 
									$PE_Object_Type, 
									$obj 
								);
							};
							break;
						default:
							$list_type = Type::string();
							//wp_die( [$class_name, $obj[$key]['class']] );
							require_once( PE_CORE_REAL_PATH."class/PE_Object_Type.php" );
							$PE_Object_Type = PE_Object_Type::get_instance(true);
							switch( $obj[$key]['class'] )
							{
								case "taxonomy":
									$child_type = "SMC_Taxonomy";
									break;
								case "post":
								default:
									$child_type = "SMC_Post";
									break;
							}
							foreach( $PE_Object_Type->object as $_key => $_value)
							{
								if( $PE_Object_Type->object[$_key]['class']['type'] == $obj[$key]['class'] )
								{
									$list_type = $isForInput 
										? 
										Type::int() 
										: 
										PEGraphql::object_type($PE_Object_Type->object[$_key]['class']['type']);
										
									$child_type = $PE_Object_Type->object[$_key]['class']['type'];
									break;
								}
							}
							$resolve =  function( 
								$root, 
								$args, 
								$context, 
								$info 
							) use(
								$class_name, 
								$child_type, 
								$obj, 
								$key,
								$PE_Object_Type
							) 
							{
								
								if (!class_exists($child_type) || !method_exists($child_type, "get_type"))
								{
									return;
								}
								$cl 			= $obj[$key]['class'];	
								$order 			= $obj[$key]['order'];	
								$orderby		= $obj[$key]['orderby'];	
								$element_type 	= $PE_Object_Type->object[ $cl::get_type() ]['t']['type'];
								$parent_type 	= $PE_Object_Type->object[ $class_name::get_type() ]['t']['type'];
								$results = [];
								if( $element_type == "post" )
								{
									$args = [
										"post_type"		=> $child_type::get_type(),
										"count"			=> 1000,
										"author"		=> -1,
										"post_status"	=> "publish"
											
									];
									switch($parent_type)
									{
										case "post":
											$args["relation"] 	= "AND"; 
											$args["fields"] 	= "all"; 
											$args["order"] 		= "DESC"; 
											$args["order_by"] 	= "title"; 
											$args["post_status"]= "publish"; 
											$args["offset"] 	= 0; 
											$args["metas"] 		= [ 
												[
													"key" 			=> $class_name::get_type(), 
													"value_numeric"	=> (int)$root["id"],
													"compare"		=> "="
												] 
											]; 
											break;
										case "taxonomy":
										default:
											$args['tax_query'] 	= [
												[	
													'taxonomy' 	=> $class_name::get_type(),
													'field'    	=> 'id',
													'terms'    	=> $root["id"],
												]
											];
											$args['taxonomies'] = [
												[
													"tax_name"	=> $class_name::get_type(),
													"term_ids"	=> $root["id"]
												]
											];
											
									}
									if( isset($orderby) )
									{
										switch($orderby)
										{
											case "id":
											case PE_POST_TITLE:
											case PE_POST_AUTHOR:
												break;
											default:
												$args["orderby"] = "meta_value_num";
												$args["meta_key"]= $orderby;
												break;
										}
									}
									if( isset($order) )
									{
										$args['order'] = $order;
									}
									
									/*
									$subobjects = get_posts( $args );
									//wp_die($args);
									
									foreach($subobjects as $subobject)
									{
										$results[] = $child_type::get_single_matrix( $subobject );
									}
									*///
									$results 	= $child_type::get_all_matrixes($args);
									//wp_die( [ $args, $results ] );
								}
								else if($element_type == "taxonomy")
								{
									$args = [
										"number"		=> 10000,
										'taxonomy'      => [ $cl::get_type() ],
										'hide_empty' 	=> false,
										"meta_query"	=> [
											'relation' 	=> 'AND',
											[
												"key" 	=> $class_name::get_type(),
												"value"	=> $root["id"],
												"compare" => "=",
												//'type'    => 'NUMERIC'
											]
										]
									];
									$subobjects = get_terms( $args );
									// wp_die( $args );
									foreach($subobjects as $cc)
									{
										$course 	= PE_Taxonomy::get_instance($cc->term_id);
										$results[] 	= $course->get_single_matrix();
									}
									//wp_die( $results );
								}
								
								return $results;
							};
					}
					//if(!$isForInput)
					{
						$type = Type::listOf( $list_type );
					}
					break;
				case "id":
					$type = Type::int();
					break;
				case "user":
					$type = $isForInput ? Type::int() : PEGraphql::object_type("User");
					$resolve =  function(
						$root, 
						$args, 
						$context, 
						$info 
					) use( $class_name, $child_type, $key, $PE_Object_Type, $obj ) // 
					{ 
						// подмена: имя мета-поля не совпадает с именем GQL-члена?
						if( isset( $obj[$key]["origin"] ) ) {
							$_okey = $obj[$key]["origin"];
						}
						else {
							$_okey = $key;
						}
						//родительский объект
						$instance = $class_name::get_instance( $root['id'] );
						
						// wp_die( $instance );
						if($obj[$key]['type'] == "post")
						{ 
							$meta = $instance->get_meta( $_okey );
						}
						else
						{
							$meta = $instance->get_meta( $_okey );
						}
						$user = PEUser::get_user( $meta );
						return $user;
					};
					break;
				case "geo":
					$type = Type::listOf(Type::float());
					$resolve =  function(
						$root, 
						$args, 
						$context, 
						$info 
					) use( $class_name, $child_type, $key, $PE_Object_Type, $obj ) // 
					{ 
						$geo = [];
						foreach($root[$key] as $value)
						{
							$geo[] = (float)$value;
						}
						return $geo;
					};
					break;
				case "object":
					$type = Type::string(); 
					$resolve =  function(
						$root, 
						$args, 
						$context, 
						$info 
					) use( $class_name, $child_type, $key, $PE_Object_Type, $obj ) // 
					{
						// return $root[$key];
						// wp_die( $key );
						return apply_filters(
							"peql_object_resolve", 
							json_encode( $root[$key], JSON_UNESCAPED_UNICODE ),
							$root, 
							$args, 
							$context, 
							$info, 
							$class_name, 
							$child_type, 
							$key
						);
					};
					break;
				case "special": 
					$type = apply_filters( 
						"peql_special", 
						$isForInput 
							? 
							PEGraphql::input_type( $obj[$key]['gql']."Input" ) 
							: 
							PEGraphql::object_type( $obj[$key]['gql'] ),
						$obj, 
						$key,
						$isForInput,
						$root,
						$class_name, 
						$child_type, 						
					); 
						
					$resolve =  function(
						$root, 
						$args, 
						$context, 
						$info 
					) use( $class_name, $child_type, $key, $PE_Object_Type, $obj, $isForInput ) // 
					{ 
						$item = apply_filters(
							"peql_spesial_resolve", 
							"", //json_encode( $root[$key], JSON_UNESCAPED_UNICODE ),
							$root, 
							$args, 
							$context, 
							$info, 
							$class_name, 
							$child_type, 
							$key,
							$isForInput
						);
						return $item;
					};
					break;
				default: 
					$type = Type::string();
			}
			
			if($type)
			{
				$fields[ $key ] = [ 
					'type' 			=> $type, 	
					'description' 	=> __( $obj[$key]['name'], PE_CORE )  . " " . __( $obj[$key]['description'], PE_CORE ),
					"resolve" 		=> $resolve,
				];
			}
		}
		$fields['comments'] =  [
			'type' 			=> Type::listOf($isForInput ? Type::int() : PEGraphql::object_type("Comment")), 	
			'description' 	=> __( "Comments list", PE_CORE ),
			"resolve" =>  function( $root, $args, $context, $info ) use($class_type)
			{
				//wp_die( $class_type );
				return PEComment::get_matrixes([ "discussion_id" => $root['id'], "discussion_type" => $class_type ]);
			}		
		];
		
		// add post's taxonomy terms
		if($obj['t']['type'] == "post")
		{
			$taxonomy_names = get_object_taxonomies( $post_type );			
			foreach($taxonomy_names as $tax)
			{
				require_once( PE_CORE_REAL_PATH."class/PE_Object_Type.php" );
				$PE_Object_Type = PE_Object_Type::get_instance(true);		
				$childs_data 	= $PE_Object_Type->object[ $tax ];
				if($childs_data)
				{
					$class_named = $childs_data['class']['type'];
					$fields[ $tax ] = [ 
						'type' 			=>  Type::listOf($isForInput ? Type::int() : PEGraphql::object_type($class_named)),
						'description' 	=> "taxonomy",
						"resolve" =>  function( $root, $args, $context, $info ) use($class_name, $class_named, $post_type) // 
						{
							
							if (!class_exists($class_named) || !method_exists($class_named, "get_type"))
							{
								return;
							}
							
							$results = [];
							$terms	= get_the_terms($root["id"], $class_named::get_type());
							
							
							if($terms)
							{
								foreach($terms as $term)
								{
									// if($class_named === "PEFestSchool") wp_die( $term );	
									$__term 	= $class_named::get_instance($term );
									$results[] = $__term->get_single_matrix();
								}
							}
							else {
								$results = [];
							}
							
							return $results;
						}
					];
				}
			}
		}
		
		/*
		// get taxonomy's post_types objects
		if($obj['t']['type'] == "taxonomy") {
			$postTypes = get_post_types([], "names");
			$fields[ "BOOBOO" ] = [ 
				'type' 			=>  Type::string(),
				'description' 	=> "posts",
				"resolve" =>  function( $root, $args, $context, $info ) use($class_name, $class_named, $post_type) // 
				{
					return json_encode( $postTypes );
				}
			];
			 
			foreach($postTypes as $tax)
			{
				require_once( PE_CORE_REAL_PATH."class/PE_Object_Type.php" );
				$PE_Object_Type = PE_Object_Type::get_instance(true);		
				$childs_data 	= $PE_Object_Type->object[ $tax ];
				if($childs_data)
				{
					$class_named = $childs_data['class']['type'];
					$fields[ $tax ] = [ 
						'type' 			=>  Type::listOf($isForInput ? Type::int() : PEGraphql::object_type($class_named)),
						'description' 	=> "posts",
						"resolve" =>  function( $root, $args, $context, $info ) use($class_name, $class_named, $post_type) // 
						{
							
							if (!class_exists($class_named) || !method_exists($class_named, "get_type"))
							{
								return;
							}
							
							$results = [];
							
							$terms	= get_the_terms($root["id"], $class_named::get_type());
							if($terms)
							{
								foreach($terms as $term)
								{
									$__term 	= $class_named::get_instance($term );									
									$results[] = $__term->get_single_matrix();
								}
							}
							return $results;
						}
					];
				}
			}
		}
		*/
			 		
		return [
			"class_name" 	=> $class_name, 
			"fields" 		=> apply_filters("bio_gq_get_fields", $fields, $class_name, $post_type, $isForInput ), 
			"type" 			=> $type 
		];
	}
	
	static function register_smc_posts( )
	{	 
		PEGraphql::add_object_type( 
			[
				"name"			=> "Label",
				"description"	=> __( "Unediteble text", PE_CORE ),
				"fields"		=> [
					"post_content"	=> [
						"type"	=> Type::string()
					]
				]
			]
		);
		$PE_Object_Type	= PE_Object_Type::get_instance( true );		
		$arr = []; 
		PEGraphql::add_query( 
			'getTest', 
			[
				'description' => __( 'Get test', PE_CORE ),
				'type' 		=> Type::string(),
				'args'     	=> [
					"id"	=> Type::string(),
					"land_id" => Type::id()
				],
				'resolve' 	=> function( $root, $args, $context, $info )
				{		
					return "Test text";
				}
			] 
		); 
		foreach( $PE_Object_Type->object as $post_type => $val )
		{
			$obj	= $PE_Object_Type->object[ $post_type ]; 
			
			// get object data 
			extract(static::getFields( $obj, $post_type, $val )); 		
			/*
			*	@class_name - string 
			*	@fields		- array
			*	@type		- instance Type
			*/ 
			
			PEGraphql::add_object_type( 
				[
					"name"			=> $class_name,
					"description"	=> "modified post type",
					"fields"		=> $fields
				]
			); 
			PEGraphql::add_query( 
				'get' . $class_name, 
				[
					'description' => __( 'Get single post', PE_CORE ),
					'type' 		=> PEGraphql::object_type($class_name),
					'args'     	=> [ 
						"id" 		=> Type::string(),
						"land_id" 	=> Type::id(),
						
					],
					'resolve' 	=> function( $root, $args, $context, $info ) use( $class_name, $post_type, $val )
					{	
						do_action(
							"before_gql_single_resolve",
							[ 
								"args" 			=> $args, 
								"class_name"	=> $class_name, 
								"post_type"		=> $post_type, 
								"value"			=> $val 
							]
						);
						if( $args["land_id"] ) { 
							switch_to_blog( $args["land_id"] );
						}
						$matrix = []; 
						if (in_array($class_name, ['Post', "Page"]))
						{
							$class_name = "PE_" . $class_name;
						}
						if($val['t']['type'] == "taxonomy")
						{
							$term = $class_name::get_instance( $args['id'] ); 
							$matrix = $term->get_single_matrix();  
						}
						else
						{
							$post = $class_name::get_single_matrix($args['id']);
							$matrix = $post;
							//return [ "id" => $args['id'], "post_content" => $class_name ];
						} 
						do_action(
							"after_gql_single_resolve",
							[ 
								"args" 			=> $args, 
								"class_name"	=> $class_name, 
								"post_type"		=> $post_type, 
								"value"			=> $val 
							]
						);
						//wp_die( $matrix );
						return $matrix;						
					}
				] 
			);
			
			PEGraphql::add_query( 
				'get'. $class_name . "s", 
				[
					'description' 	=> __( 'Get post type list', PE_CORE ),
					'type' 			=> Type::listOf(PEGraphql::object_type($class_name)),
					'args'     		=> [ 
						"paging" 	=> [ "type" => PEGraphql::input_type("Paging") ],
						"land_id" 	=> Type::id()					
					],
					'resolve' 	=> function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
					{
						return static::get_feed( $args, $class_name, $post_type, $val );
					}
				] 
			);
			PEGraphql::add_query( 
				'get'. $class_name . "Count", 
				[
					'description' 	=> __( 'Get post type count', PE_CORE ),
					'type' 			=> Type::int(),
					'args'     		=> [ 
						"paging" 	=> [ "type" => PEGraphql::input_type("Paging") ],
						"land_id" 	=> Type::id(),
						
					],
					'resolve' 	=> function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
					{
						unset ($args["paging"]["count"]);
						unset ($args["paging"]["offset"]);
						do_action(
							"before_gql_group_count_resolve",
							[ 
								"args" 			=> $args, 
								"class_name"	=> $class_name, 
								"post_type"		=> $post_type, 
								"value"			=> $val 
							]
						);
						if( $args["land_id"] ) { 
							switch_to_blog( $args["land_id"] );
						}
						$matrix = [];
						if($val['t']['type']=="post")
						{
							if (in_array($class_name, ['Post', "Page"]))
							{
								$class_name = "PE_" . $class_name;
							}
							$taxonomies = [];
							if (isset($args["paging"]['taxonomies']))
							{								
								foreach($args["paging"]['taxonomies'] as $tax => $val )
								{
									//tax_name, term_ids
									if(!isset( $taxonomies[$val['tax_name']]) )
									{
										$taxonomies[$val['tax_name']] = [];										
									}
									$taxonomies[$val['tax_name']] =  array_merge($taxonomies[$val['tax_name']], $val['term_ids']);
								}
							} 
							//wp_die( $args["paging"] );
							$articles = $class_name::get_all_matrixes( $args["paging"] ); 
							$matrix = count($articles);
						}
						else if($val['t']['type']=="taxonomy")
						{
							unset ($args["paging"]["meta_relation"]);
							unset ($args["paging"]["tax_relation"]);
							unset ($args["paging"]["taxonomies"]);
							unset ($args["paging"]["metas"]);
							unset ($args["paging"]["post_status"]);
							unset ($args["paging"]["is_admin"]);
							unset ($args["paging"]["order"]);
							//wp_die([$args["paging"], $class_name]);
							$matrix = count($class_name::get_all_matrixes($args["paging"]));
						}
						do_action(
							"after_gql_group_count_resolve",
							[ 
								"args" 			=> $args, 
								"class_name"	=> $class_name, 
								"post_type"		=> $post_type, 
								"value"			=> $val 
							]
						);
						return $matrix;
					}
				] 
			);
			
			PEGraphql::add_mutation( 
				'delete'.$class_name, 
				[
					'description' 	=> __( "Delete single single", PE_CORE ),
					'type' 			=> Type::boolean(),
					'args'         	=> [
						"id"	=> [
							'type' => Type::string(),
							'description' => __( 'Unique identificator', PE_CORE ),
						],
						"land_id" => Type::id()
					],
					'resolve' => function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
					{
						do_action(
							"before_gql_delete_resolve",
							[ 
								"args" 			=> $args, 
								"class_name"	=> $class_name, 
								"post_type"		=> $post_type, 
								"value"			=> $val 
							]
						);
						if( $args["land_id"] ) { 
							switch_to_blog( $args["land_id"] );
						}
						$matrix = false;
						if( $val['t']['type'] == "post" )
						{
							if(in_array($class_name, ['Post', "Page"]))
							{
								if(!current_user_can("edit_pages"))
								{
									PECore::addLog( $args, [$class_name, $val], "not_rights_delete");
									throw new \PEGraphQLNotAccess( "you not right!" );
								}
								
							}
							else
							{
								$const = constant( strtoupper( $post_type ) . "_DELETE" );
								PEUser::access_caps_gq( $const, "you not right!" );
							}							
							$del = wp_delete_post( $args[ "id" ] );	
							if( $del === false || $del === null )
							{
								$matrix = false;
							}
							else
							{
								$matrix = true;
							}
						}
						else if( $val['t']['type'] == "taxonomy" )
						{
							$del	= $class_name::delete($args["id"]);
							if($del	=== true)
								$matrix = true;
							else
								$matrix = false;
						} 
						do_action(
							"after_gql_delete_resolve",
							[ 
								"args" 			=> $args, 
								"class_name"	=> $class_name, 
								"post_type"		=> $post_type, 
								"value"			=> $val 
							]
						);
						return $matrix;
					}
				] 
			);
			
			PEGraphql::add_mutation( 
				'deleteBulk'.$class_name, 
				[
					'description' 	=> __( "Delete package of element by single type ", PE_CORE ),
					//'type' 			=> Type::listOf( Type::id() ),
					'type' 			=> Type::listOf(PEGraphql::object_type($class_name)),
					'args'         	=> [
						"id"		=> [
							'type' 			=> Type::listOf(Type::id()), 
							'description' 	=> __( 'Unique identificators list', PE_CORE ),
						],
						"land_id" 	=> Type::id()
					],
					'resolve' => function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
					{
						do_action(
							"before_gql_delete_bulk_resolve",
							[ 
								"args" 			=> $args, 
								"class_name"	=> $class_name, 
								"post_type"		=> $post_type, 
								"value"			=> $val 
							]
						);
						if( $args["land_id"] ) { 
							switch_to_blog( $args["land_id"] );
						}
						$resolve = [];
						if( $val['t']['type'] == "post" )
						{
							// TODO:: Rights check	
							foreach($args[ "id" ] as $id) {
								$preDelObj = $class_name::get_single_matrix($id);
								$del = wp_delete_post( $id, FALSE );
								
								if( $del === false || $del === null )
								{
									//$resolve[] = $id;
								}
								else
								{
									$resolve[] = $preDelObj;
								} 
							}
						}
						else if( $val['t']['type'] == "taxonomy" )
						{
							// TODO:: Rights check	
							foreach($args[ "id" ] as $id) {
								$instance = $class_name::get_instance( $id );
								$preDelObj = $instance->get_single_matrix($id); 
								$del	= $class_name::delete($id);
								if($del	=== true)
									$resolve[] = $preDelObj; 
							}
						} 
						do_action(
							"after_gql_delete_resolve",
							[ 
								"args" 			=> $args, 
								"class_name"	=> $class_name, 
								"post_type"		=> $post_type, 
								"value"			=> $val 
							]
						);
						return $resolve;
					}
				] 
			);
			PEGraphql::add_mutation( 
				'delete'.$class_name, 
				[
					'description' 	=> __( "Delete single single", PE_CORE ),
					'type' 			=> Type::boolean(),
					'args'         	=> [
						"id"	=> [
							'type' 	=> Type::string(),
							'description' => __( 'Unique identificator', PE_CORE ),
						],
						"land_id" 	=> Type::id()
					],
					'resolve' => function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
					{
						if( $args["land_id"] ) { 
							switch_to_blog( $args["land_id"] );
						}
						if( $val['t']['type'] == "post" )
						{
							if(in_array($class_name, ['Post', "Page"]))
							{
								if(!current_user_can("edit_pages"))
								{
									PECore::addLog( $args, [$class_name, $val], "not_rights_delete_post");
									throw new \PEGraphQLNotAccess( "you not right!" );
								}
								
							}
							else
							{
								$const = constant( strtoupper( $post_type ) . "_DELETE" );
								PEUser::access_caps_gq( $const, "you not right!" );
							}							
							$del = wp_delete_post( $args[ "id" ] );	
							if( $del === false || $del === null )
							{
								return false;
							}
							else
							{
								return true;
							}
						}
						else if( $val['t']['type'] == "taxonomy" )
						{
							$del	= $class_name::delete($args["id"]);
							if($del	=== true)
								return true;
							else
								return false;
						}
					}
				] 
			);
			// get input data
			extract(static::getFields( $obj, $post_type, $val, true ));
			/*
			*	@class_name - string 
			*	@fields		- array
			*	@type		- instance Type
			*/
			
			PEGraphql::add_input_type( 
				[
					"name"		=> $class_name.'Input',
					'description' => __( "post type", PE_CORE ),
					'fields' 		=> $fields,
				]
			);
			
			
			PEGraphql::add_mutation(
				"change$class_name", 
				[
					'description' 	=> __( "Change parameter of single post type", PE_CORE ),
					'type' 			=> PEGraphql::object_type($class_name),
					'args'         	=> [
						"id"	=> [
							'type' 	=> Type::string(),
							'description' => __( 'Unique identificator', PE_CORE ),
						],
						'input' => [
							'type' => PEGraphql::input_type($class_name.'Input'),
							'description' => __( "Post types's new params", PE_CORE ),
						],
						"land_id" => Type::id()
					],
					'resolve' => function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
					{ 
						return static::changePEPost($args, $class_name, $post_type, $val);
					}
				] 
			);
			PEGraphql::add_mutation(
				"changeBulk$class_name", 
				[
					'description' 	=> __( "Change parameter of group of posts by single type", PE_CORE ),
					'type' 			=> Type::listOf(PEGraphql::object_type($class_name)),
					'args'         	=> [
						"id"	=> [
							'type' 	=> Type::listOf(Type::id()),
							'description' => __( 'Unique identificator', PE_CORE ),
						],
						'input' => [
							'type' => PEGraphql::input_type($class_name.'Input'),
							'description' => __( "New params for all posts in group", PE_CORE ),
						],
						"land_id" => Type::id()
					],
					'resolve' => function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
					{
						do_action(
							"before_gql_change_bulk_resolve",
							[ 
								"args" 			=> $args, 
								"class_name"	=> $class_name, 
								"post_type"		=> $post_type, 
								"value"			=> $val 
							]
						); 
						if( $args["land_id"] ) { 
							switch_to_blog( $args["land_id"] );
						}
						//wp_die( $args['id']  );
						if ( !isset($args["input"][PE_POST_TITLE]) && !$args['id'] )
						{
							$args["input"][PE_POST_TITLE] = "--";
						}
						if ( !isset($args["input"][PE_POST_CONTENT]) && !$args['id'] )
						{
							$args["input"][PE_POST_CONTENT] = "--";
						}
						$resolves = [];
						if($val['t']['type'] == "post")
						{						
							if(in_array($class_name, ['Post', "Page"]))
							{ 
						
							}
							
							// TODO - проверить права на изменения
							// $const = constant( strtoupper( $post_type ) . "_EDIT" );
							
							// PEUser::access_caps_gq( $const, "you not right!" );
							
							foreach($args["id"] as $id) {
								if( isset($id) && $id > 0 )
								{ 
									$class_name::update( $args['input'], $id );
									// TODO - вставить все поля (только $args['input')
									$class_name::clear_instance($id);
									 
									$resolves[] =  apply_filters(
										"bio_gq_change_" . $class_name, 
										$class_name::get_single_matrix( $id ),
										$art,
										$args
									);
								}
								else
								{
									//wp_die( $args['input'] );	
									$art = $class_name::insert( $args['input'] );
									// wp_die( $art );
									$resolves[] = apply_filters(
										"bio_gq_change_" . $class_name, 
										$class_name::get_single_matrix($art->id),
										$art,
										$args
									);
								}
							}
						}
						else if($val['t']['type'] == "taxonomy")
						{
							// $const = constant( strtoupper( $post_type ) . "_EDIT" );
							// PEUser::access_caps_gq( $const, "you not right! " );
							foreach($args["id"] as $id) {
								if( isset($id) && $id > 0 )
								{			
									$res = $class_name::update( $args['input'], $id );
									
									if(is_wp_error($res))
									{
										PECore::addLog( $args, $res, "create_bulk");
										throw new PE_GraphQL_Exception($res->get_error_message());
									}
									else
									{
										$examle = $class_name::get_instance( $res['term_id']  );
										$resolves[] = apply_filters(
											"bio_gq_change_" . $post_type,
											$examle->get_single_matrix(),
											$examle
										);
									}
								}
								else
								{
									$res = $class_name::insert( $args['input'] );
									if(is_wp_error($res))
									{
										PECore::addLog( $args, $res, "change_bulk");
										throw new PE_GraphQL_Exception($res->get_error_message());
									}
									else
									{
										$examle = $class_name::get_instance( $res["term_id"] );
										$resolves[] = $examle->get_single_matrix();
									}
								}
							}
						}
						do_action(
							"after_gql_change_bulk_resolve",
							[ 
								"args" 			=> $args, 
								"class_name"	=> $class_name, 
								"post_type"		=> $post_type, 
								"value"			=> $val 
							]
						); 
						return $resolves;
					} 
				] 
			);
			PEGraphql::add_mutation(
				"duplicateBulk$class_name", 
				[
					'description' 	=> __( "Copy of group of posts by single type", PE_CORE ),
					'type' 			=> Type::boolean(),
					'args'         	=> [
						"id"	=> [
							'type' 	=> Type::nonNull( Type::listOf( Type::nonNull(Type::id()) ) ),
							'description' => __( 'Unique identificator', PE_CORE ),
						],
						"data_type" => Type::string(),
						"land_id" => Type::id()
					],
					'resolve' => function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
					{ 
						if( $args["land_id"] ) { 
							switch_to_blog( $args["land_id"] );
						} 
						$resolves = [];
						/**/
						if($val['t']['type'] == "post") {
							foreach($args["id"] as $id) {
								$instance = $class_name::get_instance( $id );
								$post = $instance->doubled();
								$resolves[] = $post;
							}
						}
						else if($val['t']['type'] == "taxonomy") {
							foreach($args["id"] as $id) {
								$instance = $class_name::get_instance( $id );
								$post = $instance->doubled();
								$resolves[] = $post;
							}
						} 
						
						return true;
					} 
				] 
			);
			
		}
		return $arr;
		
	}
	
	static function graphql_register_input_types()
	{ 
		PEGraphql::add_input_type(
			[
				"name"		=> 'MetaFilter',
				'description' => __( "Filter by meta Field", PE_CORE ),
				"fields"	=> [
					'key'	=> [
						'type' 	=> Type::string(),
						'description' => __( 'name of filter term', PE_CORE )
					],
					'value'	=> [
						'type' 	=> Type::string() ,
						'description' => __( 'sting value', PE_CORE )
					],
					'value_numeric'	=> [
						'type' 	=> Type::int() ,
						'description' => __( 'numeric value', PE_CORE )
					],
					'value_bool'	=> [
						'type' 	=> Type::boolean() ,
						'description' => __( 'boolean value', PE_CORE )
					],
					'value_array'	=> [
						'type' 	=> Type::string(),
						'description' => __( 'String list value', PE_CORE )
					],
					'compare'	=> [
						'type' 	=> Type::string() ,
						'description' => '=, !=, >, <, >=, <=, EXISTS, NOT EXISTS, IN, NOT IN, BETWEEN, NOT BETWEEN'
					]
				]
			]
		);
		PEGraphql::add_input_type(
			[
				"name"		=> 'TaxFilter',
				'description' => __( "Filter by tax Field", PE_CORE ),
				"fields"	=> [
					'tax_name'	=> [
							'type' 	=> Type::string(),
							'description' => __( 'name of filter term', PE_CORE )
						],
					'term_ids'	=> [
							'type' 	=> Type::listOf(Type::int()),
							'description' => __( 'array of ID', PE_CORE )
						],
				]
			]
		);		
		
		PEGraphql::add_enum_type( 
			[
				"name"			=> "TIME_PERIOD",
				'description' 	=> 'periods for filter',
				"values"		=> [
					"HALF_HOUR"		=> [
						"value"		=> "HALF_HOUR", 
					],
					"HOUR"			=> [
						"value"		=> "HOUR", 
					],
					"TWO_HOURS"		=> [
						"value"		=> "TWO_HOURS", 
					],
					"THREE_HOURS"	=> [
						"value"		=> "THREE_HOURS", 
					],
					"SIX_HOURS"		=> [
						"value"		=> "SIX_HOURS", 
					],
					"TWELVE_HOURS"	=> [
						"value"		=> "TWELVE_HOURS",
					],
					"DAY"			=> [
						"value"		=> "DAY",
					],
					"TWO_DAYS"		=> [
						"value"		=> "TWO_DAYS",
					],
					"THREE_DAYS"			=> [
						"value"		=> "THREE_DAYS",
					], 
					"WEEK"			=> [
						"value"		=> "WEEK",
					],
					"TWO_WEEKS"		=> [
						"value"		=> "TWO_WEEKS",
					],
					"MONTH"			=> [
						"value"		=> "MONTH",
					],
					"TWO_MONTHS"	=> [
						"value"		=> "TWO_MONTHS",
					],
					"THREE_MONTHS"	=> [
						"value"		=> "THREE_MONTHS",
					],
					"SIX_MONTHS"	=> [
						"value"		=> "SIX_MONTHS",
					],
					"YEAR"			=> [
						"value"		=> "YEAR",
					],
				]
			]
		);
		
		PEGraphql::add_enum_type( 
			[
				"name"			=> "CARD_SORT_TYPE",
				'description' 	=> 'Sorting lists of elements',
				"values"		=> [
					"NACS"			=> [
						"value"		=> "NACS",
						"description" => "By order meta"
					],
					"NDECS"			=> [
						"value"		=> "NDECS",
						"description" => "By order meta reverse"
					],
					"ACS"			=> [
						"value"		=> "ACS",
						"description" => "By alphabet"
					],
					"DECS"			=> [
						"value"		=> "DECS",
						"description" => "By alphabet reverse"
					],
					"ID"			=> [
						"value"		=> "ID",
						"description" => "By ID"
					],
					"RID"			=> [
						"value"		=> "RID",
						"description" => "By ID reverse"
					],
					"MACS"			=> [
						"value"		=> "MACS",
						"description" => "By meta field"
					],
					"MDECS"			=> [
						"value"		=> "MDECS",
						"description" => "By meta field reverse"
					],
				]
			]
		);
		
		PEGraphql::add_input_type( 
			[
				"name"		=> 'Sort',
				'description' => __( "Sorting list of elements", PE_CORE ),
				'fields' 		=> [
					'type' 	=> [
						'type' 	=> PEGraphql::enum_type("CARD_SORT_TYPE"),
						'description' => __( 'Type of sorting', PE_CORE ),
						"defaultValue" => "NACS"
					],
					'meta' 	=> [
						'type' 	=> Type::string(),
						'description' => __( 'Type of sorting', PE_CORE ),
						"defaultValue" => "order"
					],
				]	
			]
		);
		
		PEGraphql::add_input_type( 
			[
				"name"		=> 'Paging',
				'description' => __( "Pagination of all Types", PE_CORE ),
				'fields' 		=> [
					'count' 	=> [
						'type' 	=> Type::int(),
						'description' => __( 'Count of collection', PE_CORE ),
						"defaultValue" => 10000
					],
					'offset' 	=> [
						'type' 	=> Type::int(),
						'description' => __( 'offset by start', PE_CORE ),
						"defaultValue" => 0
					],
					"meta_relation" => [
						'type' 	=> Type::string(),
						'description' => __( 'Compare of different meta filters. Values "OR" or "AND". Default - "OR".', PE_CORE ),
						"defaultValue" => "OR"
					],
					"tax_relation" => [
						'type' 	=> Type::string(),
						'description' => __( 'Compare of different taxonomies filters. Values "OR" or "AND". Default - "OR".', PE_CORE ),
						"defaultValue" => "OR"
					],
					"order" => [
						'type' 	=> Type::string(),
						'description' => __( 'Values "DESC" or "ASC". Default "DESC".', PE_CORE ),
						"defaultValue" => 'date'
					],
					
					'parent'			=> [
						'type' 	=> Type::string(),
						'description' => __( 'Parent of elements', PE_CORE )
					],
					'order_by_meta'	=> [
						'type' 	=> Type::string(),
						'description' => __( '', PE_CORE )
					],
					"relation"		=> [
						"type" 	=> Type::string(),
						"description" => __( '', PE_CORE ),
					],
					'taxonomies'	=> [
						'type' 	=> Type::listOf(PEGraphql::input_type("TaxFilter")),//,
						'description' => __( 'Current page. Default is 1', PE_CORE ),
						"defaultValue" => []
					],
					'metas'	=> [
						'type' 	=> Type::listOf(PEGraphql::input_type("MetaFilter")),//,
						'description' => __( ' ', PE_CORE ),
						"defaultValue" => []
					],
					'post_status'	=> [
						'type' 	=> Type::string(),
						'description' => __( 'Current page. Default is 1', PE_CORE ),
						"defaultValue" => 'publish'
					],
					'post_author'	=> [
						'type' 	=> Type::string(),
						'description' => __( 'By author ID', PE_CORE )
					],
					'is_admin'	=> [
						'type' 	=> Type::boolean(),
						'description' => __( 'For admin panel', PE_CORE ),
						"defaultValue" => false
					],
					"search" => [
						'type' 	=> Type::string(),
						'description' => __( 'search in titles by substring".', PE_CORE )
					],
				],
			]
		);
		
		//
		PEGraphql::add_object_type( 
			[
				"name"		=> 'ServiceOptions',
				'description' => __( "Multisite service options", PE_CORE ),
				'fields' 		=> [
					'roles' 	=> [
						'type' 	=> Type::listOf( PEGraphql::scalar_type("Role") ),
						'description' => __( 'Type of sorting', PE_CORE ),
						"defaultValue" => []
					], 
				]	
			]
		);
		PEGraphql::add_input_type( 
			[
				"name"		=> 'ServiceOptionsInput',
				'description' => __( "Multisite service options", PE_CORE ),
				'fields' 		=> [
					'roles' 	=> [
						'type' 	=> Type::listOf( Type::string() ),
						'description' => __( 'Type of sorting', PE_CORE ),
						"defaultValue" => []
					], 
				]	
			]
		);
		PEGraphql::add_query( 
			"getServiceOptions", 
			[
				'description' 	=> __( 'Get Multisite service options', PE_CORE ),
				'type' 			=> PEGraphql::object_type("ServiceOptions"),
				'args'     		=> [ ],
				'resolve' 	=> function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
				{ 
					$roles = get_site_meta(get_main_site_id(), "roles", true);
					return [
						"roles" => $roles ? $roles : []
					];
				}
			]
		);
		PEGraphql::add_mutation( 
			'changeServiceOptions', 
			[
				'description' 	=> __( "Change Multisite service options", PE_CORE ),
				'type' 			=> PEGraphql::object_type("ServiceOptions"),
				'args'         	=> [
					'input' => [
						'type' => PEGraphql::input_type('ServiceOptionsInput'),
						'description' => __( "Service Options.", PE_CORE ),
					]
				],
				'resolve' => function( $root, $args, $context, $info )
				{
					switch_to_blog(get_main_site_id());
					$user = wp_get_current_user();
					if( !in_array( "administrator", $user->roles ) ) {
						PECore::addLog( $args, $root, "change_service_options_not_rights");
						throw new PEGraphQLNotAccess( "you not right!" );
					}
					if($args["input"]["roles"] && is_array( $args["input"]["roles"] )) {
						$args["input"]["roles"][]= "administrator"; 
						$args["input"]["roles"][]= "editor"; 
						array_unique( $args["input"]["roles"]); 
						update_site_meta(get_main_site_id(), "roles", $args["input"]["roles"]);
					}
					$roles = get_site_meta(get_main_site_id(), "roles", true);
					return [
						"roles" => $roles ? $roles : []
					];
				}
			]
		);
			
		// Dictionary
		PEGraphql::add_input_type( 
			[
				"name"		=> 'DictionaryInput',
				'description' => __( "UI-client dictionary extension", PE_CORE ),
				'fields' 		=> [
					'dictionary' 	=> [
						'type' 	=> Type::string(),
						'description' => __( 'JSON dictionary: lst "phase: transfer"', PE_CORE ),
						"defaultValue" => "{}"
					],
					'language' 	=> [
						'type' 	=> Type::string(),
						"defaultValue" => "ru-RU"
					],
				],
			]
		);
		//
		PEGraphql::add_object_type( 
			[
				"name"		=> "Dictionary",
				'description' => __( "UI-client dictionary extension", PE_CORE ),
				'fields' 		=> [
					'dictionary' 	=> [
						'type' 	=> Type::string(),
						'description' => __( 'JSON dictionary: lst "phase: transfer"', PE_CORE ),
						"defaultValue" => "{}"
					],
					'language' 	=> [
						'type' 	=> Type::string(),
						"defaultValue" => "ru-RU"
					],
				],
			]
		); 
		PEGraphql::add_query( 
			"getDictionary", 
			[ 
				'type' 			=> PEGraphql::object_type("Dictionary"),
				'args'     		=> [ 			
					"language"	=> [
						"type" => Type::string() 
					]
				],
				'resolve' 	=> function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
				{ 
					 switch_to_blog(get_main_site_id());
					 $dictionary = get_option("pe_dictionary_" . $args["language"]);
					 // wp_die( $args["language"] );
					 return [
						"dictionary" 	=> ($dictionary)  ? json_encode($dictionary) : "[]",
						"language"		=> $args["language"]
					];
				}
			]
		);
		PEGraphql::add_mutation( 
			'changeDictionary', 
			[ 
				'type' 			=> PEGraphql::object_type("Dictionary"),
				'args'         	=> [
					'input' => [
						'type' => PEGraphql::input_type('DictionaryInput'), 
					]
				],
				'resolve' => function( $root, $args, $context, $info )
				{
					 switch_to_blog( get_main_site_id() ); 
					 if( $args["input"]["language"] && $args["input"]["dictionary"]) {
						$json = json_decode( $args["input"]["dictionary"] );  
						update_option("pe_dictionary_" . $args["input"]["language"], $json);
						$dictionary = get_option("pe_dictionary_" . $args["input"]["language"]); 
						return [
							"dictionary" 	=> ($dictionary)  ? json_encode($dictionary) : "[]",
							"language"		=> $args["input"]["language"]
						];
					}
					else {
						$dictionary = get_option("pe_dictionary_ru_RU");
						return [
							"dictionary" 	=> ($dictionary)  ? json_encode($dictionary) : "[]",
							"language"		=> $args["language"]
						];
					}
				}
			]
		);
		
		
		PEGraphql::add_object_type( 
			[
				"name"		=> "RutubeUser",
				'description' => __( "Rutube user", PE_CORE ),
				'fields' 		=> [
					'id' 	=> [
						'type' 	=> Type::string(), 
					],
					'name' 	=> [
						'type' 	=> Type::string(), 
					],
					'avatar_url' 	=> [
						'type' 	=> Type::string(), 
					],
					'site_url' 	=> [
						'type' 	=> Type::string(), 
					],
				],
			]
		); 
		PEGraphql::add_object_type( 
			[
				"name"		=> "RutubeVideoData",
				'description' => __( "Rutube video data", PE_CORE ),
				'fields' 		=> [
					'title' 	=> [
						'type' 	=> Type::string(), 
					],
					'description' 	=> [
						'type' 	=> Type::string(), 
					],
					'thumbnail_url' 	=> [
						'type' 	=> Type::string(), 
					],
					'is_audio' 	=> [
						'type' 	=> Type::boolean(), 
					],
					'video_url' 	=> [
						'type' 	=> Type::string(), 
					],
					'track_id' 	=> [
						'type' 	=> Type::string(), 
					],
					'duration' 	=> [
						'type' 	=> Type::int(), 
					],
					'preview_url' 	=> [
						'type' 	=> Type::string(), 
					],
					'html' 	=> [
						'type' 	=> Type::string(), 
					],
					'author' 	=> [
						'type' 	=> PEGraphql::object_type("RutubeUser"), 
					],
				],
			]
		); 
		PEGraphql::add_query( 
			"getRutubeData", 
			[ 
				'type' 			=> PEGraphql::object_type("RutubeVideoData"),
				'args'     		=> [ 
					"id"	=> [ "type" => Type::string() ]
				],
				'resolve' 	=> function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
				{ 
					$url = "http://rutube.ru/cgi-bin/xmlapi.cgi?rt_mode=movie&rt_movie_id=" . $args["id"] . "&utf=1";
					$xml = simplexml_load_file($url); 
					// wp_die( $xml ); 
					return $xml;
				}
			]
		);
	}

	static function auth()
	{
		//register
		PEGraphql::add_mutation( 
			'registerUser', 
			[
				'description' 	=> __( "Register new User's account", PE_CORE ),
				'type' 			=> Type::boolean(),
				'args'         	=> [
					'input' => [
						'type' => PEGraphql::input_type('UserInput'),
						'description' => __( "User's new params", PE_CORE ),
					]
				],
				'resolve' => function( $root, $args, $context, $info )
				{
					do_action("bio_graphql_register_user", $args['input']);
					PECore::addLog( $args, $res, "try_register_user");
					if(!$args['input']['password'])
					{
						PECore::addLog( $args, $res, "register_user");
						throw new PE_GraphQL_Exception( __("Password not be empty!", PE_CORE) );
					}
					$answ = PEUser::insert($args['input']);
					if(is_wp_error($answ))
					{
						PECore::addLog( $args, $res, "register_user");
						throw new PE_GraphQL_Exception( $answ->get_error_message() );
					}
					else
					{
						$login		= $args['input']['user_email'];
						$passw		= $args['input']['password'];
						do_action("graphql_after_register_user", $answ, $args['input']);
						$user = get_user_by('id', $answ);
						
						if( $user )
						{
							if(PECore::$options['user_verify_account'])
							{
								$user_id	= $user->ID;							
								$code 		= md5(time());
								update_user_meta($user_id, 'activation_code', $code);
								update_user_meta($user_id, 'account_activated', 0);
								
								PEMail::send_mail(								
									sprintf( __("You are create account on %s", PE_CORE),  get_bloginfo("title") ),
									sprintf( 
										__("To ensure full functionality, please confirm your email address. To do this, follow <a href='%s'>this link</a>", PE_CORE),  
										Bio::$options['web_client_url'] . "/verify/$user_id/$code" 
									),
									1,
									[ $args['input']['user_email'] ]
								);
							}
							else
							{
								$user_id	= $user->ID;
								update_user_meta($user_id, 'account_activated', 1);
							}
						}
						else
						{
							PECore::addLog( $args, $res, "success_register_user");
							throw new PE_GraphQL_Exception(__( "User insert success. Logg in please.", PE_CORE) );
						}
						return true;
					}
				}
			] 
		);
		
		//register
		PEGraphql::add_mutation( 
			'verifyUser', 
			[
				'description' 	=> __( "Verified User after create account", PE_CORE ),
				'type' 			=> Type::boolean(),
				'args'         	=> [
					'id' 	=> [
						'type' 	=> Type::string(),
						'description' => __( 'User unique identificator', PE_CORE )
					],
					'code' => [
						'type' => Type::string(),
						'description' => __( "Unique cache that user have in registration e-mail.", PE_CORE ),
					]
				],
				'resolve' => function( $root, $args, $context, $info )
				{
					$code				= $args['code'];
					$activation_code	= get_user_meta((int)$args['id'], 'activation_code', $true);
					if($activation_code[0] == $code)
					{
						update_user_meta((int)$args['id'], 'account_activated', 1);
						return true;
					}
					else
					{
						return false;
					}
					
				}
			]
		);
		
		//register
		PEGraphql::add_mutation( 
			'restorePass', 
			[
				'description' 	=> __( "Restore password by unlogged User with send e-mail.", PE_CORE ),
				'type' 			=> Type::boolean(),
				'args'         	=> [
					'email' 	=> [
						'type' 	=> Type::string(),
						'description' => __( 'User\'s e-mail box', PE_CORE )
					]
				],
				'resolve' => function( $root, $args, $context, $info )
				{
					$user = get_user_by( "email",  $args['email'] );
					//wp_die( [ $args, $user ] );
					if( $user )
					{
						$user_id	= $user->ID;								
						$code 		= md5(time());
						update_user_meta($user_id, 'restored_code', $code);						
						$isSend = PEMail::send_mail(								
							sprintf( __("You are called resore your password in %s", PE_CORE),  get_bloginfo("title") ),
							sprintf( 
								__("Restore may your password. To do this, follow <a href='%s'>this link</a>", PE_CORE),  
								PECore::$options['web_client_url'] . "/restore/$user_id/$code" 
							),
							1,
							[ $args['email'] ]
						);
						/*
						return wp_mail(
						return wp_mail(
							"genglaz@gmail.com",
							"test theme",
							"test message"
						);*/
						// wp_die([$user_id, $code ]);
						return $isSend;
					}
					return false;
				}
			]
		);
		//restored password
		PEGraphql::add_mutation( 
				'compareRestore', 
				[
					'description' 	=> __( "Final restored User password", PE_CORE ),
					'type' 			=> Type::boolean(),
					'args'         	=> [
						'id' 	=> [
							'type' 	=> Type::string(),
							'description' => __( 'User unique identificator', PE_CORE )
						],
						'code' => [
							'type' => Type::string(),
							'description' => __( "Unique cache that user have in e-mail.", PE_CORE ),
						]
					],
					'resolve' => function( $root, $args, $context, $info )
					{
						$code				= $args['code'];
						$restored_code	= get_user_meta((int)$args['id'], 'restored_code', $true);
						if($restored_code[0] == $code)
						{
							//update_user_meta((int)$args['id'], 'restored_code', 1);
							return true;
						}
						else
						{
							return false;
						}
					}
				]
		);
		//restored password
		PEGraphql::add_mutation( 
			'saveNewPassword', 
			[
				'description' 	=> __( "Change User's password", PE_CORE ),
				'type' 			=> Type::boolean(),
				'args'         	=> [
					'id' 	=> [
						'type' 	=> Type::string(),
						'description' => __( 'User unique identificator', PE_CORE )
					],
					'password' => [
						'type' => Type::string(),
						'description' => __( "New password", PE_CORE ),
					],
					'code' => [
						'type' => Type::string(),
						'description' => __( "Unique cache that user have in e-mail.", PE_CORE ),
					]
				],
				'resolve' => function( $root, $args, $context, $info )
				{
					$code				= $args['code'];
						$restored_code	= get_user_meta((int)$args['id'], 'restored_code', $true);
						if(isset($restored_code) && $restored_code[0] == $code )
						{
							delete_user_meta((int)$args['id'], 'restored_code' );
							$userdata = [
								'ID'              => (int)$args['id'],
								'user_pass'       => $args['password']
							];
							$user = wp_update_user(  $userdata );
							if(is_wp_error($user))
							{
								PECore::addLog( $args, $user, "save_new_user");
								throw new PE_GraphQL_Exception( __("Unknown error. Repeat please!", PE_CORE) );
							}
							else
								return true;
						}
						else
						{
							return false;
						}
					
				}
			]
		);	
	}
	
	static function mailme() {
		//restored password
		PEGraphql::add_mutation( 
			'send_mail', 
			[
				'description' 	=> __( "Send e-mail from Master", PE_CORE ),
				'type' 			=> Type::boolean(),
				'args'         	=> [
					'adressees' 	=> [
						'type' 	=> Type::listOf(Type::string()),
						'description' => __( 'List of adressees', PE_CORE )
					],
					'title' => [
						'type' => Type::string(),
						'description' => __( "Title", PE_CORE ),
					], 
					'message' => [
						'type' => Type::string(),
						'description' => __( "HTML mail text.", PE_CORE ),
					], 
					"land_id"	=> [ "type" => Type::id() ]	
				],
				'resolve' => function( $root, $args, $context, $info )
				{ 	 
					if(!current_user_can("edit_options"))
					{ 
						PECore::addLog( $args, $res, "not_rights_send_mail");
						throw new \PEGraphQLNotAccess( "you not right!" );
					}
					
					return PEMail::send_mail(
						$args["title"],
						$args["message"],
						wp_get_current_user(),
						$args["adressees"],
					);
					
				}
			]
		);	
	}
	static function logs() {
		PEGraphql::add_object_type( 
			[
				"name"		=> "LogFile",
				'description' => __( "Rutube video data", PE_CORE ),
				'fields' 		=> [
					"file_name"	=> [ "type" => Type::string() ],
					"text"		=> [ "type" => Type::string() ],
				],
			]
		); 
		PEGraphql::add_query( 
			"getLogFiles", 
			[ 
				'type' 			=> Type::listOf(PEGraphql::object_type("LogFile")),
				'args'     		=> [ 
					"chapters"			=> [ "type" => Type::listOf(Type::string()) ],
					"ignore_land_id"	=> [ "type" => Type::boolean() ],
					"period" 			=> [ "type" => PEGraphql::enum_type("TIME_PERIOD") ], 
					"land_id"			=> [ "type" => Type::id() ] 
				],
				'resolve' 	=> function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
				{
					if(!current_user_can("edit_options"))
					{ 
						PECore::addLog( $args, $res, "get_log_files");
						throw new \PEGraphQLNotAccess( "you not right!" );
					}
					$dir = ABSPATH  . '/log/';
					$log_files = [];
					
					if ($dh = opendir($dir)) { 
						$period = $args["period"] ? get_time_period( $args["period"] ) : time() - 24*60*60;
						while (( $file = readdir($dh) ) !== false) {
							$text = file_get_contents( $dir . $file);
							$fileData = explode(".", $file);
							if( !$file || !$text ) 	continue;
							
							//chapters
							if( is_array( $args["chapters"] ) && !in_array( $fileData[1], $args["chapters"] ) ) { 
								continue;
							}
							//period
							$time = strtotime($fileData[2]); 
							if($time < $period )	continue;
							
							//land
							if( ( !!$args["land_id"] && $fileData[0] == $args["land_id"] ) || !!$args["ignore_land_id"] || !$args["land_id"] ){
								$log_files[] = [
									"file_name"		=> $file,
									"text"			=> $text
								];
							}
						}
						closedir($dh);
					}
					return $log_files;
				}
			]
		); 
		PEGraphql::add_mutation( 
			"deleteLogFile", 
			[ 
				'type' 			=> Type::boolean(),
				'args'     		=> [ 
					"file_name"	=> [ "type" => Type::string() ] 
				],
				'resolve' 	=> function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
				{
					if(!current_user_can("edit_options"))
					{ 
						PECore::addLog( $args, $res, "delete_log_file");
						throw new \PEGraphQLNotAccess( "you not right!" );
					}
					$dir = ABSPATH  . '/log/';
					$trash = ABSPATH  . '/log/trash/';
					if (!file_exists($trash)) 
					{
						mkdir($trash, 0777, true);
					}
					
					return rename( $dir . $args["file_name"], $trash . $args["file_name"] );
				}
			]
		);
		PEGraphql::add_mutation( 
			"deleteAllLogFiles", 
			[ 
				'type' 			=> Type::boolean(),
				'args'     		=> [ ],
				'resolve' 	=> function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
				{
					if(!current_user_can("edit_options"))
					{ 
						PECore::addLog( $args, $res, "delete_log_file");
						throw new \PEGraphQLNotAccess( "you not right!" );
					}
					$dir = ABSPATH  . '/log/';
					$trash = ABSPATH  . '/log/trash/';
					if (!file_exists($trash)) 
					{
						mkdir($trash, 0777, true);
					}
					if ($dh = opendir($dir)) { 
						$bool = true;
						while (( $file = readdir($dh) ) !== false) {
							$bool = $bool && rename( $dir . $file, $trash . $file );
						}
						closedir($dh);
					}
					return $bool;
				}
			]
		);
		PEGraphql::add_mutation( 
			"restoreLogFile", 
			[ 
				'type' 			=> Type::boolean(),
				'args'     		=> [ 
					"file_name"	=> [ "type" => Type::string() ] 
				],
				'resolve' 	=> function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
				{
					if(!current_user_can("edit_options"))
					{ 
						PECore::addLog( $args, $res, "delete_log_file");
						throw new \PEGraphQLNotAccess( "you not right!" );
					}
					$dir = ABSPATH  . '/log/';
					$trash = ABSPATH  . '/log/trash/'; 
					
					return rename( $trash . $args["file_name"], $dir . $args["file_name"] );
				}
			]
		);
		PEGraphql::add_mutation( 
			"deleteTrashLogFile", 
			[ 
				'type' 			=> Type::boolean(),
				'args'     		=> [ 
					"file_name"	=> [ "type" => Type::string() ] 
				],
				'resolve' 	=> function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
				{
					if(!current_user_can("edit_options"))
					{ 
						PECore::addLog( $args, $res, "delete_trash_log_files");
						throw new \PEGraphQLNotAccess( "you not right!" );
					}
					$dir = ABSPATH  . '/log/trash/';
					return unlink( $dir.$args["file_name"] );
				}
			]
		);
		PEGraphql::add_mutation( 
			"cleanTrashLog", 
			[ 
				'type' 			=> Type::boolean(),
				'args'     		=> [ 
					"file_name"	=> [ "type" => Type::string() ] 
				],
				'resolve' 	=> function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
				{
					if(!current_user_can("edit_options"))
					{ 
						PECore::addLog( $args, $res, "clean_trash_log");
						throw new \PEGraphQLNotAccess( "you not right!" );
					}
					$dir = ABSPATH  . '/log/';
					return unlink( $dir.$args["file_name"] );
				}
			]
		);
		PEGraphql::add_query( 
			"getTrashLogFiles", 
			[ 
				'type' 			=> Type::listOf(PEGraphql::object_type("LogFile")),
				'args'     		=> [  
					"ignore_land_id"	=> [ "type" => Type::boolean() ], 
					"land_id"			=> [ "type" => Type::id() ] 
				],
				'resolve' 	=> function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
				{
					if(!current_user_can("edit_options"))
					{ 
						PECore::addLog( $args, $res, "get_trash_log_files");
						throw new \PEGraphQLNotAccess( "you not right!" );
					}
					$dir = ABSPATH  . '/log/trash/';
					$log_files = [];
					
					if ($dh = opendir($dir)) { 
						$period = $args["period"] ? get_time_period( $args["period"] ) : time() - 24*60*60;
						while (( $file = readdir($dh) ) !== false) {
							$text = file_get_contents( $dir . $file);
							$fileData = explode(".", $file);
							if( !$file || !$text ) 	continue;
							
							//land
							if( ( !!$args["land_id"] && $fileData[0] == $args["land_id"] ) || !!$args["ignore_land_id"] || !$args["land_id"] ){
								$log_files[] = [
									"file_name"		=> $file,
									"text"			=> $text
								];
							}
						}
						closedir($dh);
					}
					return $log_files;
				}
			]
		);
		PEGraphql::add_mutation( 
			"sendLog", 
			[ 
				'type' 			=> Type::boolean(),
				'args'     		=> [  
					"input"				=> [ "type" => Type::nonNull(Type::string()) ], 
					"log_type"			=> [ "type" => Type::nonNull(Type::string()) ], 
					"land_id"			=> [ "type" => Type::id() ] 
				],
				'resolve' 	=> function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
				{
					PECore::addLog( $args, [], $args["log_type"] );
					return true; 
				}
			]
		);
	}
}

if(!function_exists("get_term_top_most_parent"))
{
	function get_term_top_most_parent( $term, $taxonomy ) 
	{ 
		$parent  = get_term( $term, $taxonomy ); 
		while ( $parent->parent != '0' ) 
		{
			$term_id = $parent->parent;
			$parent  = get_term( $term_id, $taxonomy);
		}
		return $parent;
	}
}
if(!function_exists("get_time_period"))
{
	function get_time_period( $period ) 
	{  
		switch( $period ) {
			case "HALF_HOUR":
				return time() - 30*60;
			case "HOUR":
				return time() - 60*60;
			case "TWO_HOURS":
				return time() - 120*60;
			case "THREE_HOURS":
				return time() - 180*60;
			case "SIX_HOURS":
				return time() - 6*60*60;
			case "TWELVE_HOURS":
				return time() - 12*60*60;
			case "DAY":
				return time() - 24*60*60;
			case "TWO_DAYS":
				return time() - 2*24*60*60;
			case "THREE_DAYS":
				return time() - 3*24*60*60;
			case "WEEK":
				return time() - 7*24*60*60;
			case "TWO_WEEKS":
				return time() - 2*24*60*60;
			case "MONTH":
				return time() - 30*24*60*60;
			case "TWO_MONTHS":
				return time() - 2*30*24*60*60;
			case "THREE_MONTHS":
				return time() - 2*30*24*60*60;
			case "SIX_MONTHS":
				return time() - 6*30*24*60*60;
			case "YEAR":
				return time() - 365*24*60*60;
			default:
				return time();
		}
	}
} 

