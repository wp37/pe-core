<?php
class ExceptionNotLogged extends Exception
{
    public function Send()
    {
        // отсылаем содержимое ошибки на email админа
    }
}
class ExceptionNotAdmin extends Exception
{
    public function Send()
    {
        // отсылаем содержимое ошибки на email админа
    }
}
class PEAjax
{
	static $instance;
	static function get_instance()
	{
		if(!static::$instance)
			static::$instance = new static;
		return static::$instance;
	}
	function __construct()
	{
		add_action('wp_ajax_nopriv_peajax',		array(__CLASS__, 'ajax_submit') );
		add_action('wp_ajax_peajax',			array(__CLASS__, 'ajax_submit') );
		add_action('wp_ajax_peajax-admin', 		array(__CLASS__, 'ajax_submit'));		
	}
	static function ajax_submit()
	{
		try
		{
			static::peajax_submit();
		}
		catch(ExceptionNotLogged $ew)
		{
			$d	= [	
				-1,
				[
					'a_alert'	=> __("Only logged users may do this!", BIO)
				]
			];
			$d_obj	= json_encode(apply_filters("pe_ajax_data_not_owner", $d, $params));
			print $d_obj;
			exit;
		}
		catch(ExceptionNotAdmin $ew)
		{
			$d	=  [
				-2,
				[
					'a_alert'	=> __("Only Administrator may do this!", BIO)
				]
			];
			$d_obj	= json_encode(apply_filters("pe_ajax_data_not_admin", $d, $params));
			print $d_obj;
			exit;
		}
		catch(Error $ex)
		{
			$d = [	
				"Error",
				[
					'msg'	=> $ex->getMessage (),
					'log'	=> $ex->getTrace ()
				]
			];
			$d_obj		= json_encode( $d );				
			wp_die( $d_obj );
		}
		wp_die();
	}
	
	static function peajax_submit()
	{
		require_once PE_CORE_REAL_PATH.'vendor/gamegos/jws/src/JWS.php';
		global $wpdb;
		$nonce = $_POST['nonce'];
		if ( !wp_verify_nonce( $nonce, 'peajax-nonce' ) ) die ( $_POST['params'][0] );
		
		$params	= $_POST['params'];	
		$d		= [ $_POST['params'][0], array() ];				
		switch($params[0])
		{				
			case "test":	
				$d = [	
					$params[0],
					[ 
						"text"		=> 'testing',
					]
				];
				break;	
			case "pe_options":	
				$name = $params[1];
				$val  = $params[2];
				if( $name == "menu")
				{
					$vw 	= [];
					foreach($val as $k => $v)
					{						
						$f				= $v;
						$thumbnail		= wp_get_attachment_image_src( $v['icon'], "full" )[0];
						$f['icon']		= $thumbnail ? $thumbnail : PE_CORE_EMPTY_IMG;
						$vw[]			= $f;
					}
					file_put_contents( ABSPATH."static/json/menu.json", json_encode( $vw ) );
				} 
				if( $name === "jws")
				{
					$headers = array(
						'alg' => 'HS256', //alg is required
						'typ' => 'JWT'
					);

					// anything that json serializable
					$payload = array(
						'email' 	=> $val["email"],
						'name' 		=> $val["name"],
						'secret' 	=> $val["secret"],
						'iat' 		=> $val["time"]
					);
					
					$key = GRAPHQL_JWT_AUTH_SECRET_KEY;
					$jws = new \Gamegos\JWS\JWS();
					$jwsString = $jws->encode($headers, $payload, $key);
					PECore::$options["jws-token"] 	= $jwsString;
					$val["jws-token"] 				= $jwsString;
				}
				PECore::$options[$name] = $val;
				update_option(PE_CORE, PECore::$options);
				$d = array(	
					$params[0],
					array( 
						"text"		=> 'pe_options',
						$name		=> $val
					)
				);
				break;	
			case "bio_role_user_id":	
				$data = $params[1];
				$user = get_user_by("id", $data['role_user_id']);
				if( $data['is_check'] )
				{					
					$user->add_role($data['role']);
					$text = sprintf(__("Add %s role for %s", PE_CORE), "<b>".$data['role']."</b>", $user->display_name);
				}
				else
				{
					$user->remove_role($data['role']);
					$text = sprintf(__("Remove %s role for %s", PE_CORE), "<b>".$data['role']."</b>", $user->display_name);
				}
				$d = array(	
					$params[0],
					array( 
						"msg"		=> $text,
					)
				);
				break;	
			case "bio_user_force_activate":	
				$user_id = $params[1];
				update_user_meta($user_id, "account_activated", 1);
				$d = [
					$params[0],
					[ 
						"is_activate" => get_user_meta($user_id, "account_activated", true)
					]
				];
				break;
			default:
				$d = apply_filters("pe_ajax_submit", $d, $params);
				break;
		}
		$d_obj		= json_encode(apply_filters("pe_ajax_data", $d, $params));				
		print $d_obj;
		wp_die();
	}
}