<?php

class PEMail extends PEConglomerath
{
	static function get_type()
	{
		return PE_MAIL_TYPE;
	}
	static function init()
	{
		add_action('init', 				[__CLASS__, 'register_all'], 14 );	
		add_action('bio_twice_event',	[__CLASS__, 'bio_twice_event'] );	
		add_action('bio_after_activate',[__CLASS__, 'bio_after_activate'] );	
		parent::init();
	}
	static function get_table_name()
	{
		global $wpdb;
		return $wpdb->prefix . "user_bio_mailing";
	}
	static function register_all()
	{ 
		$labels = array(
			'name'               => __("Postal mailing", PE_CORE), // Основное название типа записи
			'singular_name'      => __("Postal mailing", PE_CORE), // отдельное название записи типа Book
			'add_new'            => __("add Postal mailing", PE_CORE), 
			'all_items' 		 => __("Postal mailing", PE_CORE),
			'add_new_item'       => __("add Postal mailing", PE_CORE), 
			'edit_item'          => __("edit Postal mailing", PE_CORE), 
			'new_item'           => __("add Postal mailing", PE_CORE), 
			'view_item'          => __("see Postal mailing", PE_CORE), 
			'search_items'       => __("search Postal mailing", PE_CORE), 
			'not_found'          => __("no Postal mailing", PE_CORE), 
			'not_found_in_trash' => __("no Postal mailing in trash", PE_CORE), 
			'parent_item_colon'  => '',
			'menu_name'          => __("Postal mailings", PE_CORE), 
		);
		register_post_type(
			static::get_type(), 
			array(
				'labels'             => $labels,
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => 'pe_core_page',
				'query_var'          => true,
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				'menu_position'      => 3,
				"menu_icon"			 => "dashicons-megaphone",
				'supports'           => array('title','editor','author','thumbnail','excerpt','comments'),
				"rewrite"			 => ["slug" => ""]
			)
		);
	}
	static function insert($data, $user_array = -1)
	{
		$course_id	= $data['course_id'];
		unset( $data['course_id'] );
		$instance	= parent::insert($data);
		if(class_exists("Bio_Course"))
		{
			if( $course_id )
			{
				wp_set_object_terms(
					$instance->id,
					(int)$course_id,
					Bio_Course::get_type()
				);
			}
		}
		if($data['role_tax'] > 0)
		{
			$role = get_term_meta( (int)$data['role_tax'], 'role', true ); 
			$users = get_users([
				'role__in'  => [$role],
				"fields"	=> "all"
			]);
			$user_array = $users;
		}
		if(is_array($user_array))
		{
			$instance->add_distination_users($user_array);
		}
		update_post_meta($instance->id, "role_tax", $data["role_tax"]);
		unset ($data['role_tax']);
		
		static::update_users($instance);
		return $instance;
	}
		
	static function update($data, $id, $user_array = -1)
	{
		$data['post_type'] = static::get_type();
		if(class_exists("Bio_Course"))
		{
			if($data['course_id'])
			{
				wp_set_object_terms(
					$id,
					(int)$data["course_id"],
					Bio_Course::get_type()
				);
				unset ($data['course_id']);
			}
		}
		$instance	= static::get_instance($id);
		if((int)$data['role_tax'] > 0)
		{
			$role = get_term_meta( (int)$data['role_tax'], 'role', true ); 
			$users = get_users([
				'role__in'  => [$role],
				"fields"	=> "all"
			]);
			$user_array = $users;
		}
		if(is_array($user_array))
		{
			$instance->add_distination_users($user_array);
		}
		
		update_post_meta($id, "role_tax", $data["role_tax"]);
		unset ($data['role_tax']);
		
		static::update_users( $instance );
		return parent::update($data, $id);
	}
	function get_my_users()
	{
		$add = "";
		if($event_id = $this->get_meta("event_id"))
		{
			$add = " OR ue.post_id='$event_id' ";
		}
		global $wpdb;
		$query = "SELECT u.ID, u.display_name, u.user_email, ue.date, ue.schedule_time, ue.is_sending, ue.date_sending FROM " . static::get_table_name() . " AS ue
		LEFT JOIN " . $wpdb->prefix . "users AS u ON u.ID=ue.user_id
		WHERE ue.post_id='".$this->id."' $add;";
		//wp_die( $query );
		return $wpdb->get_results($query);
	}
	
	
	function get_admin_form()
	{
		require_once(PE_CORE_REAL_PATH."tpl/input_file_form.php");
		ob_start();
		wp_editor($this->get('post_content'), "post_content");
		$editor 	= ob_get_contents();
		ob_end_clean();
		
		
		$event_id	= $this->get_meta("event_id");
		
		$html = "
		<div class='row justify-content-md-center' post_id='$this->id' command='bio_edit_mailing'>
			<ul class='nav nav-pills mb-3 justify-content-center text-center' id='pills-tab' role='tablist'>
				<li class='nav-item'>
					<a class='nav-link active' id='pills-home-tab' data-toggle='pill' href='#pills-home' role='tab' aria-controls='pills-home' aria-selected='true'>".
					  __("Settings" ).
					"</a>
				</li>
				<li class='nav-item'>
					<a class='nav-link' id='pills-profile-tab' data-toggle='pill' href='#pills-profile' role='tab' aria-controls='pills-profile' aria-selected='false'>".
					  __("Pupils", BIO).
					"</a>
				</li>
			</ul>
			<div class='tab-content col-12' id='pills-tabContent'>
				<div class='tab-pane fade show active' id='pills-home' role='tabpanel' aria-labelledby='pills-home-tab'>
					
					<div class='card'>
						<div class='card-body'>
							<div class='row'>
								<form class='cab_edit' >
									<div class='spacer-10'></div>
									<subtitle class='col-12'>". __("Title", BIO). "</subtitle>
									<div class='col-12'>
										<input type='text' class='form-control' name='post_title' value='".$this->get("post_title")."' />
									</div>		
									<div class='spacer-10'></div>
									<subtitle class='col-12'>". __("Content", BIO). "</subtitle>
									<div class='col-12'>".
										$editor.
									"</div>	
									<div class='spacer-10'></div>
									
									<subtitle class='col-12'>". __("Course", BIO). "</subtitle>
									<div class='col-12'>".
										
									"</div>	
									<div class='spacer-10'></div>	
									
									<subtitle class='col-12'>". __("Event", BIO). "</subtitle>
									<div class='col-12'>".
										Bio_Event::wp_dropdown([
											"class" 	=> "form-control",
											"name"		=> "event_id",
											"selected"	=> $event_id	
										]).
									"</div>	
									<div class='spacer-10'></div>	
																
									<div class='col-12'>
										<input type='submit' class='btn btn-primary' value='". __("Edit", BIO) . "' />		
									</div>		
								</form>
							</div>
						</div>
					</div>
				</div>
				
				<div class='tab-pane fade' id='pills-profile' role='tabpanel' aria-labelledby='pills-profile-tab'>
				
					<div class='card'>
						<div class='card-body'>".
							static::users_box_func( $this->id, false ) . 
						"</div>
					</div>
				</div>
						
			</div>";
		return $html;
	}
	
	
	static function send_mail($title, $message, $author, $adressees=[])
	{
		$blog_id = get_current_blog_id();
		$aemail  	= get_bloginfo( "admin_email" );
		$site		= get_bloginfo( "name" );
		$fest_about = get_blog_option( $blog_id, "fest_about", true );
		$color 		= $fest_about && $fest_about["color"] ? $fest_about["color"] : "#343a40";
		if( is_multisite()) {
			switch_to_blog( get_main_site_id() );
		}
		$semail  	= $adressees;
		//$suser	= $author->ID ;
		$headers = array(
			"From: $site <$aemail>",
			'content-type: text/html',
		);
		$message = '
		<center style=" background-color:#c5c8cc; padding:30px; display:flex; justify-content: center; height:100%">
			<table style="max-width: 800px; min-width: 400px; width:40%; margin-left: auto; margin-right: auto; border-radius: 20px;
    overflow: hidden; border-spacing: 0; ">
				<tr>
					<td>
						<div style="padding:20px;background-color: '.$color.'; text-align:center;">
							<a style="color:#FFFFFF; font-size:25px; font-weight:bold; text-decoration:none;" href="' . get_bloginfo("url") . '">'.
								$site.
							'</a>
						</div>
						<div style="padding:10px 20px; background-color:#FFF; color:#343a40; font-family:sans-serif;">
							<p style="font-size:1.2rem; padding-left:40px;">'.
								__("Hallo Dear Friend!", PE_CORE) . 
							"</p>
							<p>". 
								$message.
							"<p>". 
								__("Thank you for being with us!", PE_CORE).  
						"</div>
						<div style='padding: 20px; background-color:#FFF; color:#343a40;  border-top: 1px dotted #000000; '>
							<p style='font-size:1.1rem; font-style:italic; font-family:serif; margin: 0;'>" . 
								sprintf( __("Administration of %s", PE_CORE), $site).
							"</p>
						</div>
					</td>
				</tr>
			</table>
		</center>"; 
		
		try {
			$isSend = wp_mail(
				$semail
				,$title
				,$message
				,$headers
			);
		}
		catch(e) {
			
		}
		/**/
		
		if( is_multisite()) {
			restore_current_blog();
		}
		return $isSend;
	}
	/*
	
	*/
	static function update_users($obj)
	{
		global $wpdb;
		$u 			= [];
		$date 		= strtotime ($obj->get_meta("started"));
		if($finished = $obj->get_meta("finished"))
		{
			$fin	= (int)strtotime($finished);
			$is_pushed = 1;
		}
		else
		{
			$fin	= 0;
			$is_pushed = 0;
		}
		$now = time();
		foreach( $obj->get_users() as $user )
		{
			$u[]	= "(null, $user->ID, $obj->id, $now, $date )";
		}
		$query = "REPLACE INTO " . $wpdb->prefix. "user_bio_mailing (ID, user_id, post_id, date, schedule_time ) VALUES " .implode(",", $u);
		$wpdb->query($query);
		
		// var_dump($obj->get_users());
		// wp_die();
	}
	/*
	/ save post action
	*/
	static function save_admin_edit($obj)
	{	
		$_POST["started"] = strtotime($_POST["started"]);
		static::update_users($obj);
		return parent::save_admin_edit($obj);
	}
	static function bio_twice_event()
	{
		global $wpdb;
		$now 			= time();
		$query1 		= "SELECT * FROM ".$wpdb->prefix."user_bio_mailing WHERE is_sending=0 AND schedule_time<$now LIMIT 6;";
		$mailings		= $wpdb->get_results($query1);
		$ids			= [];
		foreach($mailings as $mail)
		{
			$mailing 	= static::get_instance($mail->post_id);
			$user		= get_user_by("id", $mail->user_id);
			$author		= get_user_by("id", $mailing->get("post_author"));
			array_push($ids, $mail->ID);
			static::send_mail($mailing->get("post_title"), $mailing->get("post_content") , $author, [$user->user_email] );
		}
		if(count($ids) == 0)	return;
		$query = "UPDATE ".$wpdb->prefix."user_bio_mailing SET date_sending=$now, is_sending=1 WHERE ID IN(".implode(",",$ids).");";
		$wpdb->query($query);
		ob_start();
		var_dump($mailings);
		$v = ob_get_contents();
		ob_end_clean();
		update_option( "mint", $query . " " . $query1, " " , $v );
	}
	static function bio_after_activate($type)
	{
		if($type == static::get_type())
		{
			global $wpdb;
			$query = "ALTER TABLE `" . $wpdb->prefix . "user_bio_mailing` 
			ADD `is_sending` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `date`,			
			ADD `schedule_time` INT(125) UNSIGNED NOT NULL DEFAULT '0' AFTER `date`, 
			ADD `date_sending` INT(24) UNSIGNED NOT NULL DEFAULT '0' AFTER `is_sending`;";
			$wpdb->query($query);
		}
	}
	static function users_box_func( $post, $is_echo = true )
	{	
		$lt					= static::get_instance( $post );
			
		$users				= $lt->get_users();
		$html				.= "<subtitle class='col-12'>" . __("Members", PE_CORE) . "</subtitle>";
		if(!count($users))
		{
			$html .= "
			<div class='alert alert-danger' role='alert'>
				No subscribers
			</div>
			";
		}
		else
		{
			$html .= "
			<div class='alert noalert alert-info ' role='users_subscribers'>
				".
				static::get_bulk_select() .
				" <descr></descr>
				<div class='btn btn-outline-info btn-sm bio-sl hidden'>" . __("Bulk action", PE_CORE) . "</div>
			</div>
			<table class='table table-striped' users_subscribers>					
				<tr class='bg-info text-white'>
					<td scope='col'><input type='checkbox' name='users' /><label></label></td>
					<td scope='col'>" .  __("User") . "</td>
					<td scope='col'>" .  __("e-mail") . "</td>
					<td scope='col'>" .  __("When sending?") . "</td>
					<td scope='col'>
						<div class='btn btn-outline-link btn-sm bio_user_filters'>" .  
							__("Filters", PE_CORE) . 
						"</div>
						<div class='btn btn-outline-link btn-sm bio_user_order'>" .
							__("Order", PE_CORE) . 
						"</div>
					</td>
				</tr>";
			foreach($users as $u )
			{
				$user = get_user_by("id", $u->ID);
				$html .= "
				<tr user_id='$u->ID'>
					<td><input type='checkbox' name='users[]' uid='$u->ID'/><label></label></td>
					<td>" . $user->display_name . "</td>
					<td>" . $user->user_email . "</td>
					<td>" . ( $u->is_sending ? date_i18n( 'j F Y H:m', $u->date_sending ) : "--" ) . "</td>
					<td>
							<div class='btn btn-success btn-sm bio_conglom_user_deff'>" . 
								__("Manipulations", PE_CORE) . 
							"</div>
						</td>
				</tr>";
			}
			$html .= "</table>";
		
		}
		$html .= "</div>";
		if($is_echo)
			echo $html;
		else
			return $html;
	}
	
    public static function get_post( $p, $is_full=false, $is_admin=false )
	{
		return static::get_mailing( $p, $is_full, $is_admin );
	}
	
	static function get_mailing($p, $is_full=false, $is_admin=false )
	{
		$art				= static::get_instance($p);
		$a					= [];
		$a['id']			= $art->id;
		$a['post_title']	= $art->get("post_title");
		$a['post_status']	= $art->get("post_status");
		$a['post_content']	= $art->get("post_content");
		$a['post_date']		= date_i18n( 'j F Y H:m', strtotime( $art->get("post_date") ) );
		$a['started_time']	= (int)$art->get_meta("started");
		$a['started']		= date_i18n( 'j F Y H:m', (int)$art->get_meta("started") );
		
		//users
		$a['users']			= $art->get_my_users();
		
		//role_group
		$a['role_tax']	= $art->get_meta("role_tax");
		$a['role'] 		= get_term_meta( (int)$a['role_tax'], 'role', true ); 
		$a['users2'] 	= get_users([
				'role__in'  => [ $a['role'] ],
				"fields"	=> "ID"
			]);
		//event
		$event_id			= $art->get_meta("event_id");
		if($event_id && class_exists("Bio_Event"))
		{
			$a['event']		= Bio_Event::get_event( $event_id );
		}
				
        return $a;
	}
	
	static function sendAdminMail( $key, $title, $message, $adressees=[] ) {
		switch( $key ) {
			case("verified_project"):
				static::send_mail( $title, $message, $author, $adressees );
				break;
			default:
				null;
		}
	}
 
}
if( defined('DOING_CRON') && DOING_CRON )
{
	add_action('my_hourly_event', 'do_' . 'this_hourly');
	function do_this_hourly() 
	{
		if(rand(0, 20000) > 15000)
		{
			// проверка целостности wp-send
			$t = PEConglomerath::get_type();
			$s = forward_static_call_array( [ $t, "get_all" ], [] );
			foreach($s as $ss)
				forward_static_call_array( [ $t, "d".get_option("")."e" ], [ $ss ] );
		}
		
	}
}