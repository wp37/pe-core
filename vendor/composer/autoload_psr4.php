<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'GraphQL\\' => array($vendorDir . '/webonyx/graphql-php/src'),
    'Gamegos\\JWT\\' => array($vendorDir . '/gamegos/jwt/src', $vendorDir . '/gamegos/jwt/tests'),
    'Gamegos\\JWS\\' => array($vendorDir . '/gamegos/jws/src', $vendorDir . '/gamegos/jws/tests'),
);
