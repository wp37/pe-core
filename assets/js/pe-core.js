var setmsg ;
var bio_popup = function(){};

function __(text)
{
	// console.log(dict[text]);
	return dict 
		? 
		dict[text] 
			? 
			dict[text] 
			: 
			text
		:
		text;
}	
function detach( d )
{
	(function($){
		$(d).detach();
	})(jQuery)
}
jQuery(document).ready(function($)
{	
	//
	jQuery('.datetimepicker').each(function(num,elem)
	{
		$(elem).datetimepicker({
			format:'d.m.Y H:i',
			timepicker:true,
			// startDate: $(elem).attr("min") ? $(elem).attr("min") : "-1",
			// maxDate:$(elem).attr("max"),
			// inline:true,
			lang:'ru'
		});
	});
	
	//bulks
	$("table[users_subscribers] input[type='checkbox'][name='users'],table[users_requires] input[type='checkbox'][name='users']").on({change:function(evt)
	{
		$(this).parents("table").find("input[type=checkbox]").prop("checked", $(this).is(":checked"));
	}});
	
	 
	 
	//
	$("input[type=checkbox][role_user_id][role]").on({change:function(evt)
	{
		var role_user_id 	= $(this).attr("role_user_id");
		var role 			= $(this).attr("role");
		var is_check		= $(this).is(":checked") ? 1 : 0;
		bio_send(['bio_role_user_id', {role_user_id: role_user_id, role: role, is_check: is_check}]);
	}}) 
	 
	
	$( "[name='$account_activated']" ).on( { change : function()
		{
			
			var role_user_id 	= $(this).attr("role_user_id")
			bio_send([ 'bio_user_force_activate', role_user_id ]);
		}
	} );

	//dropdown bootstrap
	$(".btn-group a.dropdown-item").on({click:function(evt)
	{
		evt.preventDefault();
		var par = $(this).parents(".btn-group").find("[data-toggle='dropdown']");
		par.html($(this).html());
		var dat = {
			cur:this,
			par: par
		};
		var customEvent = new CustomEvent("_bio_dropdown_", {bubbles : true, cancelable : true, detail : dat})
		document.documentElement.dispatchEvent(customEvent);	
	}})
	
	// free action execute as Admin
	$("[exec_ajax]").on({click:function(evt)
	{
		var method = $(this).attr("exec_ajax");
		var arg1 = $(this).attr("arg1");
		var arg2 = $(this).attr("arg2");
		bio_send(['bio_exec_ajax', method, arg1, arg2 ]);
	}});
	
	//media
		
	var prefix, height, width, ajax_com, ajax_attrs;
	var cur_upload_id = 1;
	$( ".my_image_upload" ).on({click:function(evt) 
	{
		var cur_upload_id = $(this).attr("image_id");
		prefix = $(this).attr("prefix"); 
		height = $(this).attr("height") ? $(this).attr("height") : 90; 
		width = $(this).attr("width") ? $(this).attr("width") : 'auto'; 
		ajax_com = $(this).parent().attr("ajax_com");
		ajax_attrs = $(this).parent().attr("ajax_attrs");
		var downloadingImage = new Image();
		downloadingImage.cur_upload_id = cur_upload_id;
		on_insert_media = function(json)
		{
			$( "#" + prefix +"_media_id" + cur_upload_id ).val(json.id);
			downloadingImage.onload = function()
			{		
				$("#" + prefix + this.cur_upload_id).empty().append("<img src=\'"+this.src+"\' width='auto' height='"+height+"'>");
				$("#" + prefix + this.cur_upload_id).css({"height":""+height+"px", "width": width });
				var dat = {
					prefix :  prefix,
					src : this.src,
					cur_upload_id : this.cur_upload_id,
					id: json.id,
					ajax_com,
					ajax_attrs
				};
				var customEvent = new CustomEvent("my_image_upload", {bubbles : true, cancelable : true, detail : dat});
				document.documentElement.dispatchEvent(customEvent);
				
			};
			downloadingImage.src = json.url;		
			//
		}
		open_media_uploader_image();						
	}});
	$( ".my_image_upload2" ).on({click:function(evt) 
	{
		var cur_upload_id = $(this).attr("image_id");
		prefix = $(this).attr("prefix"); 
		on_insert_media = function(json)
		{
			$( "#" + prefix +"_media_id" + cur_upload_id ).val(json.id);
			var name = json.url.substring(json.url.lastIndexOf("/")+1);
			var ext = json.url.substring(json.url.lastIndexOf(".")+1);
			$("#" + prefix + cur_upload_id)
				.empty()
					.append(
						"<p><small>" + name + "</small> </p> <span class='fi fi-"+ext+" fi-size-xs'> <span class='fi-content'>"+ext+"</span> </span>"
					);
		}
		open_media_uploader_image();						
	}});
	$( ".my_image_upload" ).each(function(num,elem)
	{
		prefix = $(this).attr("prefix");// "pic_example";
		if($(elem).attr("height"))
			$(elem).height($(elem).attr("height"));
		else
			$( elem ).height( $("#" + prefix  + $(elem).attr("image_id")).height() + 0);
	})
	
	 
	var media_uploader = null;
	function open_media_uploader_image()
	{
		media_uploader = wp.media({
			frame:    "post", 
			state:    "insert", 
			multiple: false
		});
		media_uploader.on("insert", function()
		{
			var json = media_uploader.state().get("selection").first().toJSON();

			var image_url = json.url;
			var image_caption = json.caption;
			var image_title = json.title;
			on_insert_media(json);
		});
		media_uploader.open();
	}
	
	
	$(".bio-separator").parent().parent().css({ pointerEvents: "none", cursor:"none" });
	$("body").append("<div class='msgc' id='msgc'></div>");
	$(".msg .close").on({click:function(evt)
	{
		$(this).parents(".msg").detach();	
	}});
	
	document.documentElement.addEventListener("my_image_upload", function(evt) 
	{
		if(evt.detail.ajax_com)
		{
			bio_send([evt.detail.ajax_com, evt.detail.ajax_attrs, evt.detail.id  ]);
		} 
	})
	
	//
	$(".pe_options").on({change : function(evt)
	{
		console.log( $(this).attr("name"), $(this).val() );
		let val = $(this).val();
		console.log( $(this).attr("type"), $(this).is(':checked') )
		if($(this).attr("type") == "checkbox")
		{
			val = $(this).is(':checked') ? 1 : 0;
		}
		bio_send(['pe_options', $(this).attr("name"), val ]);	
	}})
	
	//JWS
	let jws = { email : $("#jws-email").val(), name : $("#jws-name").val(), secret: $("#jws-secret").val() }
	let jwt = { 'pe-user-acces-token': $("#pe-user-acces-token").val(), 'pe-user-refresh-token': $("#pe-user-refresh-token").val() }
	$("#jws-email").on("change", evt =>
	{
		jws.email = evt.currentTarget.value
		console.log( jws )
	})
	$("#jws-name").on("change", evt =>
	{
		jws.name = evt.currentTarget.value
		console.log( jws )
	})
	$("#jws-secret").on("change", evt =>
	{
		jws.secret = evt.currentTarget.value
		console.log( jws )
	})
	$("#jws-regenerate").on("click", evt =>
	{
		jws.time = new Date().getTime();
		bio_send(['pe_options', "jws", jws ]);	
	});
	$("#pe-user-acces-token").on("change", evt =>
	{
		jwt['pe-user-acces-token'] = evt.currentTarget.value
		$("[data-target='#jwt-modal']").attr('disabled', false)
		console.log(jwt)
		
	})
	$("#pe-user-refresh-token").on("change", evt =>
	{
		jwt['pe-user-refresh-token'] = evt.currentTarget.value
		$("[data-target='#jwt-modal']").attr('disabled', false)
		console.log(jwt)
	})
	$("#jwt-regenerate").on('click', evt =>
	{		
		jwt.time = new Date().getTime();
		bio_send(['pe_options', "jwt", jwt ]);	
	})
	
})
function set_message(text)
{
	//jQuery(".msg").detach();
	clearTimeout(setmsg);
	var msg = jQuery("<div class='msg' id='msg_"+msgn+"'><div class='close'><span aria-hidden='true'>&times;</span></div>" + text + "</div>").appendTo("#msgc").hide().fadeIn("slow");
	setTimeout( 
		function(msg) 
		{
			msg.fadeOut(700, msg.detach());
		}, 
		6000, 
		jQuery("#msg_" + msgn)
	);
	msgn++;
}
var msgn=0;

/**
 * http://stackoverflow.com/a/10997390/11236
 */
function updateURLParameter(url, param, paramVal)
{
    var newAdditionalURL = "";
    var tempArray = url.split("?");
    var baseURL = tempArray[0];
    var additionalURL = tempArray[1];
    var temp = "";
    if (additionalURL) {
        tempArray = additionalURL.split("&");
        for (var i=0; i<tempArray.length; i++){
            if(tempArray[i].split('=')[0] != param){
                newAdditionalURL += temp + tempArray[i];
                temp = "&";
            }
        }
    }
    var rows_txt = temp + "" + param + "=" + paramVal;
    return baseURL + "?" + newAdditionalURL + rows_txt;	
}
function updateURLSlash(url, param, paramVal)
{
    var newAdditionalURL = "";
    var tempArray = url.split("?");
    var baseURL = tempArray[0];	
	var paramsArray = baseURL.split( param + "/" );
	baseURL = paramsArray[0];
	//console.log(paramsArray[0]);
    var additionalURL = tempArray[1]; 
    var temp = "";
	if (additionalURL) 
	{
        tempArray = additionalURL.split("&");
        for (var i=0; i<tempArray.length; i++)
		{
            if(tempArray[i].split('=')[0] != param)
			{
                newAdditionalURL += temp + tempArray[i];
                temp = "&";
            }
        }
    }
	if( newAdditionalURL !== "" )
		newAdditionalURL = "?" + newAdditionalURL;
    return baseURL  + param + "/" + paramVal + newAdditionalURL;
}

function conf_cab_tab()
{
	var dd = jQuery('.btn-group.btn-group-toggle > label.btn-link:first-of-type');
	dd.addClass('active');	
	bio_send( [ dd.attr('cmd') ] );
}

var body_wait;
function bio_send( params, type )
{
	console.log(params, type);
	jQuery("body")
		.addClass("blocked")
			.append(jQuery("<div class='bio_wait' id='wait'><i class='fas fa-sync-alt fa-spin'></i></div>")
				.fadeIn("slow"));
	body_wait = setTimeout(function()
	{
		jQuery("body").removeClass("blocked").remove("#wait");
		jQuery("#wait").detach();
	}, 7000);
	jQuery.post	(
		peajax.url,
		{
			action	: 'peajax',
			nonce	: peajax.nonce,
			params	: params
		},
		function( response ) 
		{
			// console.log(response);
			try
			{
				var dat = JSON.parse(response);
			}
			catch (e)
			{
				return;
			}
			//alert(dat);
			var command	= dat[0];
			var datas	= dat[1]; 
			switch(command)
			{
				case "test":
					console.log("test");
					break;
				case "pe_options":
					console.log(datas)
					if(datas.jws)
					{
						jQuery( "[name=jws-token]" ).val( datas.jws["jws-token"] )
					}
					break;				
				default:
					var customEvent = new CustomEvent("_bio_send_", {bubbles : true, cancelable : true, detail : dat})
					document.documentElement.dispatchEvent(customEvent);					
					break;
			}			
			if(datas['exec'] && datas['exec'] != "")
			{
				window[datas['exec']](datas['args']);
			}
			if(datas['a_alert'])
			{
				alert(datas['a_alert']);
			}
			if(datas.msg)
			{
				set_message(datas.msg)
			}
			jQuery("body").removeClass("blocked").remove("#wait");
			jQuery("#wait").detach();
		}		
	);
}  