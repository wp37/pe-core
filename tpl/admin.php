<?php
	//require_once(PE_CORE_REAL_PATH."tpl/input_file_form.php");
	//wp_die("GGGGGG");
	$int = get_option("mint");
	global $components;
	$components = [ ];
	function wp_pe_component($name, $selected)
	{
		global $components;
		$html = "<select class='form-control' name='$name'>";
		foreach( $components as $component)
		{
			$html .= "
			<option value='".$component[0]."' " . selected($component[0], $selected, 0) . ">".
				$component[1].
			"</option>";
		}
		$html .= "</select>";
		return $html;
	} 
	$fields = "";
	$groups = []; 
	/*
	echo( "<pre><code>" . json_encode(PE_Object_Type::get_instance()->options, JSON_PRETTY_PRINT) . "</code></pre>" );
	wp_die();
	*/
	foreach(PE_Object_Type::get_instance()->options as $key => $val)
	{
		if($val["hidden"]) continue;
		$group_name = $val[ "tab" ]; 
		if(!$groups[ $group_name ])
		{
			$groups[ $group_name ] = "<li class='list-group-item '>
			<div class='row mb-4 '>
				<div class='lead col-md-9 offset-md-3 col-12 '>". __( $group_name, PE_CORE )."</div>
			</div>";
		}
		$form = $val[ "type" ] . " " . $key . " " .
			"<pre><code>" . json_encode(static::$options[ $key ], JSON_PRETTY_PRINT) . "</code></pre>" ;
		/**/
		$form = PE_Assistants::admin_form( 
			$val[ "type" ], 
			static::$options[ $key ], 
			$key, 
			[
				"class" => "pe_options"
			]
		);
		
		$groups[ $group_name ] .= "
			<div class='row align-items-center  mb-2'>
				<div class='col-md-3 col-sm-12 text-right text-secondary small '>".
					__($val["name"], PE_CORE).
				"</div>
				<div class='col-md-9 col-sm-12 '>
					$form
				</div>
				<div class='small col-md-9 offset-md-3'>" .
					__($val["description"], PE_CORE).
				"</div>
			</div>";
	}
	// return;
	foreach($groups as $gr)
	{
		$fields .= $gr . "</li>";
	}
	$html = "<style>
			.nav.nav-tabs 
			{
				padding: 0;
			}
		</style>
		<section>
			<div class='container'>
				<div class='w-100 d-flex align-items-center mb-5 mt-5'>
					<div 
						class='bio-course-icon-lg mr-3' 
						style='background-image:url(" . PE_CORE_URLPATH . "assets/img/PE_logo_large.svg); width:75px; height:75px;'
					>
					</div>
					<div class='d-flex '>
						<div class='display-4'> " . 
							__("Protopia Ecosystem", PE_CORE) .
						"</div>
					</div>
				</div>
				<nav>
				  <div class='nav nav-tabs' id='nav-tab' role='tablist'>
					<a 
						class='nav-item nav-link active' 
						id='nav-home-tab' 
						data-toggle='tab' 
						href='#nav-home' 
						role='tab' 
						aria-controls='nav-home' 
						aria-selected='true'
					>".
						__("Main settings", PE_CORE).
					"</a>
					<a 
						class='nav-item nav-link' 
						id='nav-jws-tab' 
						data-toggle='tab' 
						href='#nav-jws' 
						role='tab' 
						aria-controls='nav-jws' 
						aria-selected='false'
					>".
						__("My ecosystem", PE_CORE).
					"</a>
					<!--a 
						class='nav-item nav-link' 
						id='nav-utilities-tab' 
						data-toggle='tab' 
						href='#nav-utilities' 
						role='tab' 
						aria-controls='nav-utilities' 
						aria-selected='false'
					>".
						__("Utilities", PE_CORE).
					"</a-->
					<a 
						class='nav-item nav-link' 
						id='nav-about-tab' 
						data-toggle='tab' 
						href='#nav-about' 
						role='tab' 
						aria-controls='nav-about' 
						aria-selected='false'
					>".
						__("About", PE_CORE).
					"</a>
				  </div>
				</nav>
				
				<div class='tab-content' id='nav-tabContent'>
				  <div class='tab-pane fade show active' id='nav-home' role='tabpanel' aria-labelledby='nav-home-tab'>				  
					<ul class='list-group'> 
						$fields 			
						<li class='list-group-item '>
							<div class='row mb-4'>
								<div class='lead col-md-9 offset-md-3 col-12 '>".
									__("Media taxonomies", PE_CORE).
								"</div>
							</div>
							<div class='row mb-2'>
								<div class='col-md-3 col-sm-12 text-right text-secondary small '>".
									__("Media Taxonomy for icons", PE_CORE).
								"</div>
								<div class='col-md-9 col-sm-12'>".
									wp_dropdown_categories( [
										'show_option_all'    => '',
										'show_option_none'   => '---',
										'option_none_value'  => -1,
										'orderby'            => 'ID',
										'order'              => 'ASC',
										'echo'               => 0,
										'selected'           => static::$options['icon_media_term'],
										'hierarchical'       => 0,
										'name'               => 'icon_media_term',
										'id'                 => 'icon_media_term',
										'class'              => 'form-control pe_options',
										'taxonomy'           => PE_CORE_MEDIA_TAXONOMY_TYPE,
										'hide_if_empty'      => false,
										'hide_empty'      	=> false,
									]).
								"</div>
							</div>
							<div class='row mb-2'>
								<div class='col-md-3 col-sm-12 text-right text-secondary small '>".
									__("Media Taxonomy for Users loaded", PE_CORE).
								"</div>
								<div class='col-md-9 col-sm-12'>".
									wp_dropdown_categories( [
										'show_option_all'    => '',
										'show_option_none'   => '---',
										'option_none_value'  => -1,
										'orderby'            => 'ID',
										'order'              => 'ASC',
										'echo'               => 0,
										'selected'           => static::$options['test_media_free'],
										'hierarchical'       => 0,
										'name'               => 'test_media_free',
										'id'                 => 'test_media_free',
										'class'              => 'form-control pe_options',
										'taxonomy'           => PE_CORE_MEDIA_TAXONOMY_TYPE,
										'hide_if_empty'      => false,
										'hide_empty'      	=> false,
									]).
								"</div>
							</div>
						</li>" . 						
						apply_filters("pe_core_admin", "").						
					"</ul>				  
				  </div>
				  <div class='tab-pane fade' id='nav-jws' role='tabpanel' aria-labelledby='nav-jws-tab'>				  
					<ul class='list-group'>
						<li class='list-group-item '>
							<div class='row mb-4'>
								<div class='lead col-md-9 offset-md-3 col-12 '>".
									__("Users JWT secrets", PE_CORE).
								"</div>
								<div class='small text-danger font-italic col-md-9 offset-md-3 col-12 '>".
									__("Do not share this data with anyone. They protect the data of your Users from unscrupulous hackers and ensure the safety of communication between your service and partner services. In the event of attacks on User messages, change the secret phrases.", PE_CORE) . 
								"</div>
							</div>								
							<div class='row mb-2'>
								<div class='col-md-3 col-sm-12 justify-content-end align-items-center d-flex text-secondary small  '>".
									__("First (access token) secret", PE_CORE).
								"</div>
								<div class='col-md-9 col-sm-12 input-group '> 
									<input 
										type='text' 
										class='form-control' 
										id='pe-user-acces-token' 
										value='" . PECore::$options['jwt']['pe-user-acces-token'] . "'
										placeholder='" . __("Access secret", PE_CORE) . "'
									> 
								</div>
							</div>							
								
							<div class='row mb-2'>
								<div class='col-md-3 col-sm-12 justify-content-end align-items-center d-flex text-secondary small  '>".
									__("Second (refresh token) secret", PE_CORE).
								"</div>
								<div class='col-md-9 col-sm-12 input-group '> 
									<input 
										type='text' 
										class='form-control' 
										id='pe-user-refresh-token' 
										value='" . PECore::$options['jwt']['pe-user-refresh-token'] . "'
										placeholder='" . __("Refresh secret", PE_CORE) . "'
									> 
								</div>
								<div class='col-md-9 offset-md-3 col-12'>									
									<button 
										type='button' 
										class='btn btn-secondary text-nowrap my-3'
										data-toggle='modal' 
										data-target='#jwt-modal'
										disabled 
									>" . 
										__("Update all secrets", PE_CORE). 
									"</button>
									<div class='small text-danger my-0 '>".
										__("Attention! Changing secrets will lead to a one-time reset of all logged. Everyone logged Users will have to re-login.", PE_CORE) . 
									"</div>
								</div>
							</div>
						</li>
						<li class='list-group-item '>
							<div class='row mb-2 '>
								<div class='col-md-3 col-sm-12 justify-content-center align-items-end d-flex flex-column text-secondary text-right small'>".
									__("JWS-token of this WP-client", PE_CORE).
								"
									<div class='text-secondary small my-4'>".
										__("Boo-boo-boo boo-boo boo-boo", PE_CORE) .
									"</div>
								</div>
								<div class='col-md-9 col-sm-12'> 									
									<textarea 
										class='form-control jws-token mb-2 mr-3 ' 
										name='jws-token'
										rows=12
										disabled
										value='" .static::$options['jws-token']. "' 
									>" .static::$options['jws-token']. "</textarea>
									<button 
										type='button' 
										class='btn btn-secondary re-generate-btn text-nowrap my-3'
										data-toggle='modal' 
										data-target='#jws-modal'
									>" . 
										__("re-generate JWS-token", PE_CORE). 
									"</button>
								</div>
							</div>
							
							<div class='modal fade' id='jwt-modal' tabindex='-1' aria-labelledby='jwt-modal-label' aria-hidden='true'>
							  <div class='modal-dialog modal-dialog-centered'>
								<div class='modal-content'>
								  <div class='modal-header'>
									<h5 class='modal-title' id='jwt-modal-label'>".
										__("Update all users secrets", PE_CORE). 
									"</h5>
									<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
									  <span aria-hidden='true'>&times;</span>
									</button>
								  </div> 
								  <div class='modal-footer'>
									<button type='button' class='btn btn-secondary' data-dismiss='modal'>".
										__("Close") .
									"</button>
									<button type='button' class='btn btn-primary' id='jwt-regenerate' data-dismiss='modal' >".
										__("Update", PE_CORE) .
									"</button>
								  </div>
								</div>
							  </div>
							</div>	
							
							<div class='modal fade' id='jws-modal' tabindex='-1' aria-labelledby='jws-modal-label' aria-hidden='true'>
							  <div class='modal-dialog modal-dialog-centered'>
								<div class='modal-content'>
								  <div class='modal-header'>
									<h5 class='modal-title' id='jws-modal-label'>".
										__("re-generate JWS-token", PE_CORE). 
									"</h5>
									<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
									  <span aria-hidden='true'>&times;</span>
									</button>
								  </div>
								  <div class='modal-body'>
									<div class='form-group'>
										<label for='jws-email'>" . __("Contact e-mail", PE_CORE) . "</label>
										<input 
											type='email' 
											class='form-control' 
											id='jws-email' 
											value='" . PECore::$options['jws']['email'] . "'
											placeholder='name@example.com'
										>
									</div>
									<div class='form-group'>
										<label for='jws-name'>" . __("Name in Latin literature", PE_CORE) . "</label>
										<input 
											type='text' 
											class='form-control' 
											id='jws-name' 
											value='" . PECore::$options['jws']['name'] . "'
											placeholder='" . __("Name in Latin literature", PE_CORE) . "'
										>
									</div>
									<div class='form-group'>
										<label for='jws-secret'>" . __("Secret", PE_CORE) . "</label>
										<input 
											type='text' 
											class='form-control' 
											id='jws-secret' 
											value='" . PECore::$options['jws']['secret'] . "'
											placeholder='" . __("Secret", PE_CORE) . "'
										>
									</div>
									
								  </div>
								  <div class='modal-footer'>
									<button type='button' class='btn btn-secondary' data-dismiss='modal'>".
										__("Close") .
									"</button>
									<button type='button' class='btn btn-primary' id='jws-regenerate' data-dismiss='modal' >".
										__("Start regeneration", PE_CORE) .
									"</button>
								  </div>
								</div>
							  </div>
							</div>							
						</li>					
				</div>
				<div class='tab-pane fade' id='nav-utilities' role='tabpanel' aria-labelledby='nav-utilities-tab'>			  
					<ul class='list-group'>
						<li class='list-group-item '>
							<div class='raw'>
								<div class='col-12 m-3'>".
									 
								"</div>
							</div>
						</li>
					</ul>
				</div> 
				<div class='tab-pane fade' id='nav-about' role='tabpanel' aria-labelledby='nav-about-tab'>			  
					<ul class='list-group'>
						<li class='list-group-item '>
							<div class='raw'>
								<div class='col-12 m-3'>".
									 
								"</div>
							</div>
						</li>
					</ul>
				</div> 
				 <span id='is_save' style='vertical-align:top; display:inline-block; opacity:0;'>
					<svg aria-hidden='true' viewBox='0 0 512 512' width='40' height='40' xmlns='http://www.w3.org/2000/svg'>
						<path fill='green' d='M435.848 83.466L172.804 346.51l-96.652-96.652c-4.686-4.686-12.284-4.686-16.971 0l-28.284 28.284c-4.686 4.686-4.686 12.284 0 16.971l133.421 133.421c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-28.284-28.284c-4.686-4.686-12.284-4.686-16.97 0z'></path>
					</svg>
				</span>	
			</div>
		</section>";
		echo $html;